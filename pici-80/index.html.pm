#lang pollen

◊define-meta[title]{פיצי-80: ספר ללימוד תכנות}
◊define-meta[publish-date]{2023-08-14}◊;TODO
◊define-meta[multigender]{true}

◊margin-bit-audio["bit.opus"]{
	שלום!
	לי קוראים ביט, ואני אלווה אותך בלימוד תכנות עם פיצי-80.  
	נעים להכיר ^_^◊br{}◊br{}
	◊gender["תוכלי" "תוכל"]{} להתחיל מקריאה של ה◊xref["pici-80/preface"]{הקדמה}, ומשם להמשיך לפרקים שלמטה.
	רק חלק מהפרקים כתובים בינתיים.◊br{}◊br{}
    דרך אגב אם ◊gender["תלחצי" "תלחץ"]{} עלי עם העכבר (כאן ובכל מקום) ◊gender["תוכלי" "תוכל"]{} לשמוע אותי מדברת.
}
◊sound["stardew-valley-coin.opus"]{◊pici{}} הוא ספר בעברית ללימוד תכנות שמכוון לא·נשים ◊gender["סקרניות" "סקרנים"]{} בכל הגילאים, ולא מניח רקע קודם.
הגישה שבספר רואה בתכנות לא עניין טכני יבש אלא בראש ובראשונה כלי לביטוי יצירתי, שהלימוד שלו גם בעל ערך בפני עצמו וגם מאפשר ללמוד לחשוב באופנים חדשים.
◊gender["תוכלי" "תוכל"]{} לבחור◊;
◊numbered-note{
	בחירה זה משהו שיכול להחוות כמאיים או לא ברור כשאין רקע.
	אל דאגה! נסביר הכל בהקדמה, ובכל מקרה לא מדובר בחתונה קתולית… 💍
}
בין שפות תכנות (◊L{◊wiki-he{Python}} או ◊L{◊wiki-he{Lua}}) ובין סביבות פיתוח (◊link["https://tic80.com/"]{◊L{TIC-80}} או ◊link["https://pico-8.com/"]{◊L{PICO-8}}), אבל העקרונות ודרכי החשיבה שנלמד יהיו מועילים גם לתכנות בשפות אחרות ובסביבות פיתוח אחרות.

◊;◊link[#:rel "preload" #:as "image" #:href "/pici-80/megason-easter.webp"]{}
◊`(link ((rel "preload") (as "image") (href "/pici-80/megason-easter.webp")))

◊div[#:class "center"]{
    ◊a[#:onclick "this.firstChild.play()" #:class "invisiblelink"]{◊audio[#:src "/pici-80/smb-coin.opus"]{}◊span[#:class "megason"]{}}
}

הספר נכתב בחלקים, ובשלב זה פורסמו רק ההקדמה והפרק הראשון.
אשמח לקבל משוב כדי לשפר את הקיים ואת הפרקים הבאים; דרכי יצירת קשר מופיעות ◊link["https://me.digitalwords.net"]{כאן}.


