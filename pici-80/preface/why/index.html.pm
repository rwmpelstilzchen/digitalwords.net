#lang pollen

◊define-meta[title]{👩‍💻 למה ללמוד לתכנת?}
◊define-meta[publish-date]{2019-10-26}◊;TODO
◊define-meta[hide-from-sitemap]{true}
◊define-meta[multigender]{true}
◊define-meta[toc]{true}



◊margin-bit{תכנות = ◊img[#:class "pixelart" #:style "width: 1em" #:src "/pici-80/heart.png"]{}}
לדעתי יש שלוש סיבות משלימות ללמוד לתכנת (מלבד זה שזה כיף…):
כדי לבנות דברים יפים, מעניינים ומועילים,
כדי ללמוד לחשוב באופנים חדשים,
וכדי להבין יותר טוב איך מחשבים פועלים.

תכנות הוא אחד התחומים שיכולים לחבר באופן יפה פנים שבתרבות הפופולרית נתפסים כמנוגדים: אנליטיות ◊TODO{להסביר או להשתמש במונח אחר} ויצירתיות.
אפשר לממש במחשב רעיונות מגוונים באופן אלגנטי, ובעזרת האינטרנט אפשר לחלוק את המימושים האלה ולהשתתף בקהילה מקוונת יוצרת.



◊section{🏗 לבנות דברים}

◊margin-bit{יש כיף מיוחד בלחשוב על רעיון, לתכנן ולפרק אותו ואז לבנות אותו במחשב ולראות את התוצאה של כל התהליך חיה לפנינו על המסך.}
הסיבה הראשונה היא תועלתנית, ומתמקדת בתוצר של מה שאפשר לעשות בעזרת תכנות ולא במה שנותן לנו התהליך כ◊gender["בנות" "בני"]{}־אדם.

בעזרת תכנות אנחנו ◊gender["יכולות" "יכולים"]{} להפיק ממחשבים הרבה יותר ממה שאפשר בתור ◊gender["משתמשות" "משתמשים"]{} בלבד; תכנות מאפשר לנו לקחת תפקיד פעיל ולא להיות ◊gender["תלויות" "תלויים"]{} רק במה שכבר נוצר.
אם ◊gender["את קוראת" "אתה קורא"]{} את השורות האלה, ◊gender["את משתמשת" "אתה משתמש"]{} במחשב כדי לעשות את זה: בין אם מחשב שולחני, מחשב נייד, טלפון חכם או טאבלט.
רובנו ◊gender["מתייחסות" "מתייחסים"]{} למחשב כאל „קופסה שחורה” שמאפשרת לנו לעשות כל מני דברים שאנחנו ◊gender["רוצות" "רוצים"]{}: לשמוע מוזיקה, לדבר עם חברות/ים, לכתוב טקסטים, לקרוא ספרים דיגיטליים (כמו זה!), לשחק במשחקי־מחשב, לצייר ועוד.
כל השימושים האלה נהדרים, אבל הם מוגבלים לפונקציות של המחשב שכבר נהגו ומומשו.
מה אם נרצה ליצור דבר חדש, ללמד את המחשב לעשות דברים חדשים?

כמו שנראה תוך כדי לימוד, מחשבים הם מצד אחד מכונות מאוד פשוטות אבל מצד שני מאוד גמישות.
אלא שתי תכונות שרק לכאורה נראות סותרות אחת את השניה.
למעשה, הגמישות של מחשבים מגיעה בדיוק מתוך העובדה שעקרונות הפעולה שלהם פשוטים ובסיסיים כל כך, ואפשר לרתום אותם למטרות שונות ומגוונות.
בעזרת תכנות נוכל ללמד את המחשב יכולות חדשות וליצור דברים שלא היו קיימים קודם.
במעבד תמלילים נוכל לכתוב טקסטים, וזהו; בדפדפן נוכל לגלוש באינטרנט, וזהו; בתוכנת ציור נוכל לצייר, וזהו.
לעומת זאת, כשאנחנו אלה ש◊gender["יוצרות" "יוצרים"]{} את התוכנה שהמחשב מריץ האפשרויות פתוחות לפנינו לעשות מה שנרצה ואיך שנרצה!

ב◊fici{} נלמד שפת־תכנות בשם ◊wiki-he["לואה (שפת תכנות)"]{◊L{Lua}}.
אחד היתרונות שלה הוא שהיא מאוד גמישה ומאפשרת גם תכנות של משחקים ותוכנות עצמאיים וגם יישומים שמוטמעים בתוך מערכות אחרות◊;
◊numbered-note{
	לדוגמה, בעזרת ◊L{Lua} אפשר לתכנת לגו ◊wiki-en["Lego_Mindstorms_NXT#Lua"]{◊L{NXT}}, לכתוב פילטרים לתוכנות גרפיות או לכתוב סקריפטים (תסריטים) שמשנים את האופן שבו משחקי־מחשב פועלים.
	רשימות של מערכות קיימות שמשתמשות ב־◊L{Lua} אפשר למצוא ◊wiki-en["List of applications using Lua"]{כאן}, ◊wiki-en["Category:Lua-scripted video games"]{כאן} ו◊wiki-en["Category:Lua-scriptable game engines"]{כאן}.
}.

ב◊fici{} נתמקד בשני תחומי של יצירה: הדגמות גרפיות (דמואים, ◊wiki-en["Demoscene"]{demos}) ומשחקים.
בכל פרק ניצור הדגמות ומשחקים קטנים וחמודים, ולא פרוייקטים גדולים, כשהמטרה היא ללמוד עקרונות תכנותיים בהתנסות ישירה וביצירה של משהו קומפקטי ומינימליסטי שעומד בפני עצמו.



◊section{💡 ללמוד לחשוב}

◊margin-bit{„אני מחשב, משמע אני קיימת.” — ביט}
יש הרבה דרכים לחשוב בהן.
תכנות מחשבים מחייב אותנו לתעל את המחשבות שלנו באופן מסויים שמחשבים יכולים להבין.

קודם כתבתי שמחשבים הם פשוטים.
כדי שנוכל להורות למחשב לעשות דברים מורכבים ומעניינים נצטרך ללמוד איך לעבד ו◊wiki-he["פורמליזם (מתמטיקה)"]{להצרין} את המחשבות שלנו כך שמחשב יוכל לבצע את מה שאנחנו ◊gender["רוצות" "רוצים"]{} שיעשה.
זאת יכולת שאפשר ללמוד, והלימוד הכי טוב נעשה דרך הידיים: בהתחלה נפתור בעיות פשוטות ועם הזמן נלמד להעביר רעיונות מורכבים יותר ויותר למבנה שמתאים למחשב.

ליכולת לחשוב בצורה הגיונית וסדרתית יש יתרונות חשובים מעבר לתכנות מחשבים.
היא מועילה כדרך להתבוננות מושכלת על העולם, וקשורה הדוקות לאופני המחשבה שנצרכים במתמטיקה, הנדסה וכל סוגי המדעים.



◊section{💻להבין מחשבים}

◊margin-bit{הידעת? אחת המכונות הראשונות שאפשר לקרוא להן „מחשב” היתה ◊wiki-he["מנוע אנליטי"]{המנוע האנליטי} שהגה ◊wiki-he["צ'ארלס בבג'"]{צ׳ארלס בבג׳}. ◊wiki-he{עדה לאבלייס} כתבה תוכנות למנוע האנליטי. ◊gender["כשתלמדי" "כשתלמד"]{} לתכנת תצטרפי לשושלת מכובדת של מתכנתות ומתכנתים, שהתחילה עם פורצות ופורצי דרך כמו לאבלייס ובבג׳!}
מחשבים מקיפים אותנו בכל מקום, לא רק אלה שאנחנו ◊gender["רגילות" "רגילים"]{} לחשוב עליהם כעל מחשבים, אלא גם אלה שקובעים את המסלול של המעלית, ששולטים על מה מכונת הכביסה תעשה בכל שלב, או שמעבדים את הקולות מהמיקרופון במכשירי שמיעה או באוזניות חוסמות רעשים.
רובנו ◊gender["יודעות" "יודעים"]{} להשתמש במחשבים אבל לא ◊gender["מכירות" "מכירים"]{} את עקרונות הפעולה שלהם, או שיש לנו רק מושג מעורפל לגביהם.
ברגע שהעקרונות האלה מוּכּרים לנו ויש לנו אפילו קצת נסיון בתכנות, הכל מתבהר.
אולי לא נדע איך בדיוק מחליטים המחשבים מתי להחליף את האורות ברמזורים, אבל נבין מה אוסף הכלים שבעזרתם זה קורה.◊;
◊numbered-note{
	את ההבנה של אופני הפעולה של מחשבים, גם ברמה של התכנון שלהם וגם ברמה של איך הם עובדים בפועל, נוכל לקבל מלימוד של כל שפת תכנות, גם כאלה שבנויות באופן קרוב לאיך שהמחשב עובד בפועל (כמו ◊wiki-he["שפת סף"]{שפות־סף} או ◊wiki-he["C (שפת תכנות)"]{C}) וגם כאלה שרחוקות יותר ומוסיפות שכבות של הפשטה (כמו ◊wiki-he{פייתון} או ◊wiki-he["לואה (שפת תכנות)"]{◊L{Lua}}).
	אני חושב ש־◊L{Lua} מעולה כשפה ראשונה בגלל שהיא מאזנת היטב בין קלות הלימוד ובין הגמישות שהיא מאפשרת.
}

לתחום שעוסק במחשבים ובחישוביוּת מנקודת מבט מדעית־מחקרית קוראים ◊wiki-he["מדעי המחשב"]{מדעי־המחשב}, ותכנות הוא כלי בסיסי מאוד וחסר תחליף בתחום הזה.
ב◊link["https://www.mada.org.il/"]{מוזיאון המדע בירושלים} יש תערוכה קבועה מצויינת על מדעי־המחשב, בשם ◊link["https://www.mada.org.il/captcha"]{קאפצ׳ה}.
בנוסף לתערוכה הגשמית עצמה, יש לה גם ◊link["https://www.mada.org.il/captcha"]{אתר} שמהווה מבוא נהדר לתחום ונוגע על קצה המזלג בשאלות יסוד; לדעתי כדאי לבקר באתר ובתערוכה בנוסף ללימוד ב◊fici{}.

אצל חלק מציבור הכללי עדיין קיימת התייחסות אל תכנות כאל משהו ששמור ליחידות ויחידי סגולה, כאל תחום מורכב מכדי להיות נגיש לכולנו.
כמו הרבה תפיסות, גם זו לא נכונה בכלל.
ברגע שפורטים את הכישוף המסתורי של אופן הפעולה של מחשבים ושל הדרך שבה אפשר לתכנת אותם המסתורין אולי מתפוגג (וטוב שכך), אבל הקסם נשאר ומתעוררות תחושות נוספות: תחושה של סקרנות לגבי מה שאפשר עוד לעשות בעזרת מחשבים, ותחושה של יצירתיות למימוש של הרעיונות האלה.
