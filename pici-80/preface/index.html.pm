#lang pollen

◊define-meta[title]{0️⃣ הקדמה}
◊define-meta[publish-date]{2023-08-14}◊;TODO
◊define-meta[multigender]{true}



◊margin-bit{הארנב הלבן הרכיב את משקפיו. „היכן אתחיל, אם טוב בעיני הוד־מלכותך?” שאל. „התחל בהתחלה,” אמר המלך בכובד ראש, „והמשך עד שתגיע אל הסוף, אז תחדל.”}
אישית, הרבה פעמים אני לא טורחת להתחיל מלקרוא הקדמות (בטח אם הן ארוכות), אלא מתחילה ישר מלקפוץ למים, להבין קצת במה מדובר, ואז חוזרת וקוראת את ההקדמה אם אני רוצה או אם אני רואה שיש בכך צורך.
◊gender["תרגישי חופשית" "תרגיש חופשי"]{} לדלג על חלקים מההקדמה הזאת וישר לשכשך את הרגליים במים ב◊xref["pici-80/chapters/01"]{פרק הראשון}, אם כי לדעתי יש בהקדמה תוכן מעניין.

◊; כדי שתוכלו לשכשך את הרגליים במים צריך שיהיו מים, כלומר סביבה שנוכל לתכנת בה.
◊; לאורך כל הספר נעבוד עם ◊link["https://pico-8.com/"]{◊L{PICO-8}} (פִּיקוֹ-8): סביבה קטנה, ידידותית ונעימה שמתאימה גם למתחילות/ים וגם למתקדמות/ים.
◊; את ◊L{PICO-8} מפתח אדם בשם ◊link["https://www.lexaloffle.com/info.php"]{◊L{Joseph White}} (◊link["https://twitter.com/lexaloffle"]{◊L{@lexaloffle}}).
◊; זאת לצערי תוכנה בתשלום (◊L{US$14.99}) וב◊xref-local["pici-80/preface/foss"]{קוד סגור}, אבל היא המתאימה ביותר לצרכינו ולדעתי אלה בין ה־◊L{$14.99} הטובים ביותר שאפשר להשקיע במשהו…◊;
◊; ◊numbered-note{
◊; 	לקבוצות לימוד יש ◊link["https://www.lexaloffle.com/pico-8.php?page=schools"]{הנחה משמעותית}.
◊; 	אם ◊gender["את" "אתה"]{} רוצה ללמוד לתכנת אבל יש סיבה חיצונית שמונעת ממך לקנות את פיקו-8 (סיבה כלכלית, העדר גישה לכרטיס אשראי, או כל סיבה אחרת), ◊link["https://me.digitalwords.net/"]{◊gender["דברי" "דבר"]{} איתי} ואשמח לעזור.
◊; }
◊; ◊sound["stardew-valley-give-gift.opus"]{🙂}.
◊; מדריך קצר לקניה והתקנה תוכלו למצוא ◊xref-local["setup.html"]{כאן}.
◊; 
◊; אם קנית, התקנת ועכשיו מופיעה לך שורת הפקודה של פיקו-8 על המסך◊;
◊; ◊margin-pico["PICO-8-command-line.png"]{}
◊; (כמו בצילום המסך שבצד), הכל מוכן ואפשר ◊xref["pici-80/chapters/01"]{לצאת לדרך}! ◊sound["bicycle-bell.mp3"]{🚴}

◊p[#:class "center"]{
	◊i{הספר מוקדש באהבה לאילאיל וראם} ◊sound["stardew-valley-new-special-item.opus"]{💜}
}

◊;{
@@@DUAL:
אבל יש בחירה שצריך לעשות לפני ש◊gender["תוכלי" "תוכל"]{} לשכשך את הרגליים במים:
ב◊fici{} לכל פרק יש שתי גרסאות, אחת שמותאמת למערכת בשם ◊link["https://pico-8.com/"]{◊L{PICO-8}} (◊img[#:class "pixelart" #:style "width: 1em" #:src "/pici-80/pico-8.png"]{}) ואחת שמותאמת למערכת בשם ◊link["https://tic.computer/"]{◊L{TIC-80}} (◊img[#:class "pixelart" #:style "width: 1em" #:src "/pici-80/tic-80.png"]{}).
אלו שתי סביבות שאפשר ליצור בהן משחקים ותוכנות, והן כוללות במקום אחד את כל מה שצריך כדי לתכנת, לצייר, להלחין ולעצב את השלבים.
ההבדלים בין השתיים לא גדולים, והן בנויות באותה רוח◊;
◊numbered-note{
	למעשה, ◊L{PICO-8} נוצרה ראשונה, ובעקבותיה קם גל של „חיקויים” בהשראתה, כש־◊L{TIC-80} הוא המוצלח מביניהם לדעתי.
};
היתרון הגדול של ◊L{PICO-8} הוא שיש קהילה פעילה של יוצרות ויוצרים סביבה, בעוד שהיתרון של ◊L{TIC-80} הוא שהיא בקוד פתוח ובחינם.
יש בהקדמה ◊xref["pici-80/preface/fantasy/index.html"]{סעיף} שמשווה בין השתיים באופן מפורט יותר.
אם ◊gender["את" "אתה"]{} לא רוצה או לא ◊gender["יכולה" "יכול"]{} לשלם עבור ◊L{PICO-8} אפשר להתחיל מ־◊L{TIC-80} החינמית ובשלב מאוחר יותר לקנות ◊L{PICO-8}.
למעשה, אפשר לתכנת ב־◊L{TIC-80} בלי להתקין כלום, ישר מהדפדפן.
}









◊;{


◊TODO{להטמיע את כל מה שנשאר כאן}


{% macro churl(system, label) -%}
⚙⚙⚙
{%- endmacro %}

{% macro picocart(cart) -%}
<img src="/carts/PICO-8/{{ cart }}.p8.png" onClick='
document.getElementById("{{ cart }}").src="/carts/PICO-8/loader.html?cart={{ cart }}";
document.getElementById("{{ cart }}").style="display: block; width:512px;height:512px;border:none";
document.getElementById("{{ cart }}").focus();
document.getElementById("{{ cart }}-img").style="display: none";'
id="{{ cart }}-img" />
<iframe id="{{ cart }}" style="display: none; width:580px;height:540px;border:none"></iframe>
{%- endmacro %}

{% macro ticcart(cart) -%}
<img src="/carts/TIC-80/{{ cart }}.gif" onClick='
document.getElementById("{{ cart }}").src="/carts/TIC-80/loader.html?cart={{ cart }}";
document.getElementById("{{ cart }}").style="display: block; width:512px;height:512px;border:none";
document.getElementById("{{ cart }}").focus();
document.getElementById("{{ cart }}-img").style="display: none";'
id="{{ cart }}-img" />
<iframe id="{{ cart }}" style="display: none; width:580px;height:540px;border:none"></iframe>
{%- endmacro %}






⚙ את הידע שתצברו במשחקת תוכלו להעביר הלאה גם למערכות אחרות.
[LÖVE](https://love2d.org/)
⚙ להגיד שתכף נראה את סלסט שהתחיל בפיקו והתרחב. אבל זה לא אומר, כמובן, שאין ערך עצמאי לדברים שעושים בפיקו/טיק.




TODO/TOADD:
http://pico-ate.com/




⚙ רעיונות: http://www.python.org.il/course/
}◊;
