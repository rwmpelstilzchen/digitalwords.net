#lang pollen

◊define-meta[title]{🪶 קולופון}
◊define-meta[publish-date]{2019-10-26}◊;TODO
◊define-meta[hide-from-sitemap]{true}
◊define-meta[multigender]{true}

◊section{הפונט „בובין”}

בובין

PICO-8: https://www.lexaloffle.com/bbs/?tid=36102


הקול של ביט
◊bullet-list{
	ניקוי


	Effects > Plugins > Delay…
	Delay type: Regular
	Delay level per echo (dB): -6.0
	Delay time (seconds): 0.009
	Pitch change effect: Tempo
	Pitch change per echo (semitones): 0.000
	Number of echoes: 30
	Allow duration to change: Yes
	לחזור על הפעולה (◊kbd{Ctrl}+◊kbd{R})

	
	להעלות את הפיץ׳ של התוצאה ב־3 חצאי טונים
}




◊; Old:

האתר הזה בנוי מחלקים שונים שמתחברים ביחד באופן אוטומטי. 
זוכרים שכתבתי למעלה שצריך ללמוד הרבה שפות?

* את הטקסטים אני כותב בעזרת שפת הסימון [Markdown](https://en.wikipedia.org/wiki/Markdown).
* מידע על כל טקסט (כמו הכותרת שלו, שמשמשת גם ביצירה של תוכן העניינים באופן אוטומטי) כתוב בשפת הסִדרוּת (סריאליזציה) [YAML](https://en.wikipedia.org/wiki/YAML).
* המידע על העיצוב כתוב, כמקובל, ב־[CSS](https://he.wikipedia.org/wiki/%D7%92%D7%99%D7%9C%D7%99%D7%95%D7%A0%D7%95%D7%AA_%D7%A1%D7%92%D7%A0%D7%95%D7%9F_%D7%9E%D7%93%D7%95%D7%A8%D7%92%D7%99%D7%9D).
* החלק שמחליף בין בפניה בלשון „את” ולשון „אתה” כתוב בכמה שורות של [JavaScript](https://he.wikipedia.org/wiki/JavaScript).
* כדי להרכיב את האתר, השתמשתי בתבניות שפוענחו בעזרת מנוע התבניות [Jinja](https://en.wikipedia.org/wiki/Jinja_(template_engine)).
* כדי לחבר הכל ביחד, כתבתי תוכנה קצרה בשפת [Python](https://he.wikipedia.org/wiki/%D7%A4%D7%99%D7%99%D7%AA%D7%95%D7%9F),
שגם משתמשת ב־[Pygments](http://pygments.org/) כדי לצבוע את הקוד.

כל כלי כזה בנוי באופן [מוֹדוּלַרִי](https://he.wikipedia.org/wiki/%D7%9E%D7%95%D7%93%D7%95%D7%9C%D7%A8%D7%99%D7%95%D7%AA); כלומר, אפשר לחבר אותו לחלקים אחרים בכל מני אופנים, כמו חתיכות של לגו.
כשבונים דברים באופן מודולרי זה נותן הרבה חופש; במקרה שלנו, זה מאפשר לדוגמה ליצור בקלות את הפרקים המקבילים של PICO-8 ו־TIC-80 או ליצור פניה בלשון „את” או „אתה” שאפשר לשנות בלחיצת כפתור.

כל קבצי המקור של האתר, שמהם מופק הפלט שהדפדפן יודע להציג, נמצאים ב־[GitLab](https://gitlab.com/rwmpelstilzchen/pici-80). זה אתר שדומה ל־[GitHub](https://he.wikipedia.org/wiki/GitHub) ומאפשר לאנשים מכל העולם לחלוק אחד עם השני דברים שהם יוצרים וליצור דברים ביחד. בדרך כלל מה שמשתפים שם זה תוכנות (כולל קלטות של PICO-8 ו־TIC-80), אבל אפשר לשתף גם טקסטים ותמונות כמו כאן.

הפונט של הכותרות לקוח מהמשחק „[האורגים](https://he.wikipedia.org/wiki/%D7%94%D7%90%D7%95%D7%A8%D7%92%D7%99%D7%9D_(%D7%9E%D7%A9%D7%97%D7%A7_%D7%9E%D7%97%D7%A9%D7%91))”, שיצא בעברית על ידי מחשבת.
[הורדה](http://www.old-games.org/games/loomh);
[סרטון](https://youtu.be/TMp1QB0s6PU);
[פתרון](https://www.youtube.com/watch?v=3p20hdNDp6Q&list=PL204E2977309B4748).
תוכלו להוריד את הפונט, שכולל את האותיות העבריות מהגרסה של מחשבת והלועזיות מהגרסה האנגלית, [כאן](TODO).
<!--
TODO:
https://github.com/dse/bdf2ttf
https://github.com/dse/exact-autotrace
export AUTOTRACE=
לכתוב ל־Darren Embry <https://github.com/dse
-->
