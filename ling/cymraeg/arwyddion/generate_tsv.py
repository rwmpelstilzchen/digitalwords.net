#!/usr/bin/env python

import dominate
from dominate import tags
import os
import yaml

data = list(yaml.load_all(open('/home/me/documents/anki/factory/Cymraeg/signs/deck.yaml'), Loader=yaml.SafeLoader))

id = 0

for img in data[0]:
    for line in img['text']:
        print(id, end="\t")
        print("<img src=\"arwyddion-" + img['file'] + "\" />", end="\t")
        print(line['cy'], end="\t")
        if 'en' in line:
            print(line['en'], end="\t")
        else:
            print("", end="\t")
        if 'en-translation' in line:
            print(line['en-translation'], end="\t")
        else:
            print("", end="\t")
        if 'verbatim' in line:
            print(line['verbatim'])
        else:
            print()
        id = id + 1
