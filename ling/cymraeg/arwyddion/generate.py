#!/usr/bin/env python

import dominate
from dominate import tags
import os
import yaml

data = list(yaml.load_all(open('/home/me/documents/anki/factory/Cymraeg/signs/deck.yaml'), Loader=yaml.SafeLoader))

def indent(level):
    return "  "*level


print('◊table[#:class "fullwidth signtable"]{')
for img in data[0]:
    print(indent(1) + "◊tr{")
    print(indent(2) + '◊td[#:class "thumbnail"]{◊a[#:href "' + os.path.join("images", img['file']) + '" #:class "barelink"]{◊img[#:src "' + os.path.join("thumbnails", img['file']) + '"]{}}}')
    print(indent(2) + '◊td{')
    print(indent(3) + '◊table[#:style "width: 100%"]{')
    for line in img['text']:
        print(indent(4) + '◊tr{')
        print(indent(5) + '◊td[#:style "width: 45%"]{' + line['cy'] + '}')
        print(indent(5) + '◊td[#:style "width: 45%"]{')
        if 'en' in line:
            print(indent(6) + '◊div{' + line['en'] + '}')
        if 'en-translation' in line:
            print(indent(6) + '◊div{(' + line['en-translation'] + ')}')
        if 'verbatim' in line:
            print(indent(6) + '◊div[#:class "verbatim"]{' + line['verbatim'] + '}')
        print(indent(5) + '}')
        print(indent(4) + '}')
    print(indent(3) + '}')
    print(indent(2) + '}')
    print(indent(1) + "}")
print("}")



# with tags.table() as t:
#     for img in data[0]:
#         with tags.tr():
#             tags.td(tags.img(src=os.path.join("thumbnails", img['file'])))
#             with tags.table():
#                 for line in img['text']:
#                     with tags.tr():
#                         tags.td(line['cy'])
#                         with tags.td():
#                             if 'en' in line:
#                                 tags.div(line['en'])
#                             if 'en-translation' in line:
#                                 tags.div(line['en-translation'], cls="mytranslation")
#                             if 'verbatim' in line:
#                                 tags.div("(" + line['verbatim'] + ")", cls="verbatim")
