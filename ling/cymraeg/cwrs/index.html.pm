#lang pollen

◊define-meta[title]{קורס וולשית באוניברסיטה העברית}
◊define-meta[publish-date]{2019-10-26}
◊define-meta[last-update]{2022-02-26}
◊define-meta[toc]{#t}
◊define-meta[no-children]{#t}



◊margin-figure["scrabble-cymraeg.jpg"]{}
בשנות הלימודים 2019–2020 ו־2020-2021 לימדתי קורס דו־שנתי על השפה הוולשית באוניברסיטה העברית (#41581 ו־#41582).
סילבוס זמין באתר האוניברסיטה בעברית (◊link["http://shnaton.huji.ac.il/index.php/NewSyl/41581/1/2020/"]{שנה א׳}; ◊link["http://shnaton.huji.ac.il/index.php/NewSyl/41582/1/2021/"]{שנה ב׳}) ובאנגלית (◊link["http://shnaton.huji.ac.il/index.php/NewSyl/41581/2/2020/"]{שנה א׳}; ◊link["http://shnaton.huji.ac.il/index.php/NewSyl/41582/2/2021/"]{שנה ב׳}); עותק מקומי בעברית (◊xref["ling/cymraeg/cwrs/syllabi/2019.heb.html"]{שנה א׳}; ◊xref["ling/cymraeg/cwrs/syllabi/2020.heb.html"]{שנה ב׳}) ובאנגלית (◊xref["ling/cymraeg/cwrs/syllabi/2019.eng.html"]{שנה א׳}; ◊xref["ling/cymraeg/cwrs/syllabi.2020.eng.html"]{שנה ב׳}).
את כל חומרי ההוראה שהכנתי◊;
◊numbered-note{
	אני אדם, משמע אני טועה.
	אם מצאת טעות או שמשהו לא ברור באחד החומרים כאן, ◊link["https://ac.digitalwords.net/"]{אשמח לשמוע}.
	זה גם המעשה החברי לשאר התלמידות/ים, שלא ילמדו מאותה הטעות (לעשות ◊L{unlearning} זה קשה).
}
אני משחרר ל◊wiki-he["רשות הציבור"]{רשות הציבור}, לרווחת הכלל.

לנוחותכן/ם, קישורים ישירים לעזרים השונים ללא דברי רקע:
◊dl{
    ◊dt{📽️}
	◊dd{מצגת: ◊xref-local["cyflwyniad/2019/cyflwyniad.pdf"]{שנה א׳}, ◊xref-local["cyflwyniad/2019/cyflwyniad-2.pdf"]{שנה ב׳}}

	◊dt{📖}
    ◊dd{◊xref-local["egluriadur"]{מקראה} עם גלוסר לחלק מהטקסטים ושמע לאחד מהם}

	◊dt{📜}
    ◊dd{◊xref-local["egluriadur/beibl"]{דף השוואת תרגומים וולשיים לתנ״ך}}

	◊dt{😿}
    ◊dd{◊xref-local["egluriadur/gofid-glosedig"]{הערות לשוניות לסיפור ◊L{Gofid}}}

	◊dt{📚}
    ◊dd{◊xref-local["#llyfrau"]{ספרים בוולשית}}

	◊dt{🛑}
    ◊dd{◊xref-local["../arwyddion"]{קורפוס של שלטים, מודעות ופליירים דו־לשוניים}}

	◊dt{👂}
    ◊dd{◊xref-local["#gwrando"]{הפניות להקלטות ושידורים}}

    ◊dt{🧠}
    ◊dd{
        חפיסות לימוד לתוכנה ◊L{Anki}:
        ◊link["https://ankiweb.net/shared/info/1600775344"]{רשימת 2,600 מילים בסיסיות};
        ◊link["https://ankiweb.net/shared/info/617887546"]{קורפוס השלטים, המודעות והפליירים};
        ◊link["https://ankiweb.net/shared/info/90991462"]{משפטים מהסיפור ◊L{Gofid}}
    }

	◊dt{📑}
    ◊dd{◊xref-local["allanol"]{הפניות למקורות חיצוניים}}

    ◊dt{✅}
    ◊dd{
        מבדקים:
        ◊xref-local["profion/prawf-1"]{ראשון},
        ◊xref-local["profion/prawf-2"]{שני} (◊xref-local["profion/prawf-2/atebion.html"]{פתרון}),
        ◊xref-local["profion/prawf-3"]{שלישי} (◊xref-local["profion/prawf-3/atebion.html"]{פתרון}),
        ◊xref-local["profion/prawf-olaf"]{מטלה מסכמת} (◊xref-local["profion/prawf-olaf/atebion.html"]{פתרון});
		◊;{
		שנה ב׳:
        ◊xref-local["profion/prawf-olaf-2"]{מטלה מסכמת} (◊xref-local["profion/prawf-olaf-2/atebion.html"]{פתרון})
		}
    }

	◊dt{🔍}
    ◊dd{◊xref-local["hanesyddol"]{הצעות לנושאים למחקר על שינוי לשוני}}
}

למתעניינות/ים בהתאמת העזרים האלה לקורסים אחרים כתבתי ◊xref-local["addysgeg"]{דף המתייחס אל העזרים מנקודת מבט פדגוגית}.



◊section[#:label "cyflwyniad"]{מצגת}
◊margin-figure["cyflwyniad.svg"]{}

את הקורס מלווה מצגת◊;
◊numbered-note{
	היא מופקת בעזרת החבילה ◊L{◊link["https://www.ctan.org/pkg/beamer"]{Beamer}} למערכת הסדר ◊L{◊wiki-he["LaTeX"]{◊LaTeX{}}}.
	קבצי המקור ◊link["https://gitlab.com/rwmpelstilzchen/cwrs-cymraeg/tree/master/presentation"]{זמינים ב־◊L{GitLab}}.
	ר׳ גם ◊old-blog["beamer-hebrew"]{הסבר} על יצירת מצגות בעברית עם ◊L{Beamer}.
}.
המטרה שלה היא לחסוך לי לכתוב על הלוח, ולחסוך לתלמידות/ים להעתיק מהלוח.
ככזאת, היא לא מתוכננת לעמוד בפני עצמה אלא רק להיות גורם מלווה: אין בה הסברים מפורטים והגדרות, אלא בעיקר עזרים חזותיים (קבוצות התחלפות וטבלאות) ודוגמאות — והרבה! — עם גלוסות ותרגום (לימוד של ידע לשוני מופשט תמיד צריך להיות מעוגן בדוגמאות קונקרטיות).

◊;{

◊style{
	#cyflwyniad td:nth-child(1) {
		white-space: nowrap;
		padding-left: 1em;
	}
}

◊table[#:class "booktab" #:id "cyflwyniad"]{
	◊tr{
		◊th{נושא}
		◊th{תת־נושאים}
	}
	◊tr{
		◊td{◊xref-local["cyflwyniad/2021/rhagymadrodd.pdf"]{הקדמה}}
		◊td{
			„כרטיס ביקור”,
			לימוד ומחקר של וולשית,
			קורפורה,
			ספרות יעץ ועזרים,
			הקורס
		}
	}
	◊tr{
		◊td{◊xref-local["cyflwyniad/2021/sylfaen.pdf"]{אבני יסוד}}
		◊td{פונולוגיה, אורתוגרפיה, מוטציות}
	}
	◊tr{
		◊td{◊xref-local["cyflwyniad/2021/enwol.pdf"]{הצירוף השמני}}
		◊td{
		כינויי גוף,
		מספר,
		יידוע,
		סמיכות,
		כינויי שייכות,
		תואר אדנומינלי,
		מבנים עם ◊L{o},
		כינויי רמז,
		ספירה
		}
	}
	◊tr{
		◊td{◊xref-local["cyflwyniad/2021/annherfynol.pdf"]{האינפיניטיב}}
		◊td{מבנה ותפקידים}
	}
	◊tr{
		◊td{◊xref-local["cyflwyniad/2021/adferfol.pdf"]{הצירוף האדוורביאלי}}
		◊td{
			מילות יחס,
			ריכוך,
			קונוורבים
		}
	}
	◊tr{
		◊td{◊xref-local["cyflwyniad/2021/cymalau.pdf"]{דגמי פסוקיות}}
		◊td{
			דגמים תטיים (קיום ושייכות, ודגם בעל נושא פורמלי ריק)
			ודגמים קטיגוריאליים (בעלי נשוא פעלי, אדוורביאלי ושמני).
		}
	}
	◊tr{
		◊td{◊xref-local["cyflwyniad/2021/isgymalau.pdf"]{שעבוד}}
		◊td{
			פסוקיות זיקה,
			פסוקיות תוכן,
			מבנים נסיבתיים
		}
	}
	◊tr{
		◊td{◊xref-local["cyflwyniad/2021/eraill.pdf"]{נושאים נוספים}}
		◊td{
			שלילה,
			מבנים סבילים,
			שאלות,
			תשובות
		}
	}
	◊tr{
		◊td{◊xref-local["cyflwyniad/2021/testunau.pdf"]{טקסטים}}
		◊td{
			משל העורבת השחורה,
			הארי פוטר,
			הנסיך הקטן,
			אליס בארץ הפלאות,
			התנ״ך (היום הראשון ומגדל בבל),
			יגון
		}
	}
	◊tr{
		◊td{◊xref-local["cyflwyniad/2021/byrfoddau.pdf"]{קיצורים בלשניים}}
	}
}

תוכלו להוריד את חלקי המצגת בלחיצה על שמות הנושאים השונים.◊;
◊numbered-note{
	את המצגת הישנה, של המחזור שהתחיל בשנת 2019, ניתן למצוא כאן:
	◊xref-local["cyflwyniad/2019/cyflwyniad.pdf"]{שנה א׳},
	◊xref-local["cyflwyniad/2019/cyflwyniad-2.pdf"]{שנה ב׳}.
}
בשקף הראשון בכל קובץ תאריך שמציין ממתי העותק שברשותכן/ם; אם אתן/ם נעזרות/ים במצגת בבית בבקשה שימו לב שהעותק עדכני.

🚧 שימו לב!
בשלב זה הקישורים לא מובילים עדיין לשום מקום (כשאסיים לעבד את המצגת עבור המחזור החדש אעלה את הקבצים המוכנים).
}

את המצגת של המחזור שהתחיל בשנת 2019, ניתן למצוא כאן:
◊xref-local["cyflwyniad/2019/cyflwyniad.pdf"]{שנה א׳},
◊xref-local["cyflwyniad/2019/cyflwyniad-2.pdf"]{שנה ב׳}.


◊section[#:label "egluriadur"]{מקראה}
◊margin-figure["llyfr-du.jpg"]{◊wiki["cy" "Llyfr_Du_Caerfyrddin"]{הספר השחור של קאירברד׳ין}, כתב־היד הוולשי המוקדם ביותר}◊;https://i2-prod.walesonline.co.uk/incoming/article10997054.ece/ALTERNATES/s615b/black-book.jpg

בנוסף למצגת יצרתי מקראה לקורס.
החלק הראשון של המקראה כוללת קטעים מתורגמים מספרים מוּכרים: התנ״ך, אליס בארץ הפלאות, הנסיך הקטן והארי פוטר ואבן החכמים.
הרציונל מאחורי זה הוא להמנע מלהציב בפני התלמידות/ים אתגר כפול — לא רק התמודדות עם שפה חדשה לגמרי, אלא גם צורך להבין בעזרתה טקסט לא מוכר.
כשהטקסט מוכר מראש אפשר להתמקד בהבנת השפה ולצלוח כמויות גדולות יותר של טקסט בזמן קצר, מה שמתחבר למושג ◊L{comprehensible input} ב־◊wiki-en["input hypothesis"]{◊L{input hypothesis}} של ◊wiki-en["Stephen Krashen"]{◊L{Stephen Krashen}}.‏◊;
◊numbered-note{
	אם זה נושא שמעניין אתכן/ם — וכבלשניות ובלשנים שהולכים לבלות זמן רב ברכישה של השפות שתחקרו אני חושב שכדאי שזה יעניין אתכן/ם — אני ממליץ מאוד לצפות בסרטונים שמופיעים בראש הרשימה של תוצאות החיפוש ◊link["https://www.youtube.com/results?search_query=comprehensible+input"]{„◊L{comprehensible input}” ביוטיוב}.
	הזמן שתשקיעו בלצפות בסרטונים, לקרוא על הנושא, ולאמץ ולהפנים שיטות לימוד יעילות יחזיר את עצמו עשרות מונים.
}
מצד שני, חשוב לזכור שלטקסטים מתורגמים יש מאפיינים לשוניים מיוחדים וחשוב גם להכיר לעומק את המערכת הלשונית של השפה ביצירה מקורית, בין אם במלל ספונטני ובין אם בספרות מקור.
מה שאני טוען זה שכדאי לעשות את זה בשלב שני, אחרי שיש קצת אחיזה בשפה.
את המקראה ניתן לקרוא ◊strong{◊xref["ling/cymraeg/cwrs/egluriadur"]{כאן}}.
המקראה לא סתם כוללת טקסטים בשפת המקור ובתרגום לוולשית, אלא גם תרגום וניתוח מורפולוגי של כל המילים בטקסט: לחיצה על כל אחת מהמילים בחלק העליון של החלון תפנה מיד לערך המתאים בגלוסר שבחלק התחתון.◊;
◊numbered-note{
	יצרתי מערכת קטנה שמאפשרת בקלות יחסית ליצור מקראות כאלה.
	קוד המקור שלה ◊link["https://gitlab.com/rwmpelstilzchen/cwrs-cymraeg/tree/master/texts"]{זמין גם הוא ב־◊L{GitLab}}: גם קבצי הקלט וגם המנגנון שמייצר מהם את המקראה וכתוב ב◊wiki-he{פייתון}.
}
זאת היתה הרבה עבודה טכנית — בעצם אפשר לומר שעשיתי לתלמידות/ים את שיעורי הבית של השיעורים הראשונים — אבל התוצאה תאפשר לרכב בהתחלה עם גלגלי־עזר.
הרעיון הוא שככל שמתקדמים בקריאה יהיה אפשר להעזר פחות ופחות בגלוסר, עד שבסופו של דבר הוא ישמש רק ככלי לאימות התרגום והניתוח של התלמיד.ה.
בבקשה נסו בשלב כמה שיותר מוקדם להשתמש בגלוסר כמה שפחות, ובמקום זאת השתמשו בידע שצברתן.ם, בספרי דקדוק ובמילונים כדי לפענח את הטקסט.
בנוסף לצורה הדיגיטלית של המקראה, שמתאימה לשימוש במחשב, הפקתי גם ◊strong{◊xref["ling/cymraeg/cwrs/egluriadur/argraff.pdf"]{קובץ להדפסה}} שיש בו הרבה ◊L{whitespace}: שוליים רחבים ורווחים גדולים בין השורות ובין המילים.
הרעיון הוא שבין השיעורים ובשיעורים עצמם התלמידות/ים יוכלו לרשום על הדף הערות, תרגום וניתוח של הטקסטים.
כתיבה של ◊wiki-en["Gloss (annotation)"]{גלוסות} על גבי טקסטים בשפות זרות היא פרקטיקה ישנה נושנה, והיא ממשיכה להתקיים מסיבה טובה.◊;
◊numbered-note{
	בהקשר הבלשני כדאי להכיר את ◊link["https://www.eva.mpg.de/lingua/resources/glossing-rules.php"]{כללי כתיבת הגלוסות של לייפציג}, שמהוות את הסטנדרט כיום בפרסומים.
}

כהשלמה לקריאת היום הראשון בסיפור הבריאה התנ״כי וסיפור מגדל בבל יצרתי ◊strong{◊xref-local["egluriadur/beibl"]{דף}} שמציג השוואה בין שלושה תרגומים וולשיים.

הטקסט הראשון שאנחנו קוראות/ים ללא עזרת גלוסר הוא הסיפור ◊L{Gofid} מאת קייט רוברטס.
כתבתי ◊strong{◊xref-local["egluriadur/gofid-glosedig"]{דף}} עם הערות לשוניות לטקסט.


◊section[#:label "corpysau"]{קורפורה}

◊subsection[#:label "llyfrau"]{ספרים}

◊margin-figure["ysgrifennydd.jpg"]{}◊;https://upload.wikimedia.org/wikipedia/commons/0/00/Escribano.jpg
יש הרבה סוגים של עיסוק בלשון.
לדידי אחד הסוגים הכי ריגורוזיים והכי מעניינים מבחינת התוצאות שדרך העבודה שלו מניבה הוא בלשנות אמפירית ששואבת את המידע שלה מייצוג סטטי ובלתי־תלוי של פלט לשוני (כתוב או מוקלט, דהיינו ◊wiki-he["פרדינן_דה-סוסיר#מושגי_יסוד_של_דה_סוסיר"]{◊L{parole}}) וחותרת לבירור מבנה המערכת הלשונית לפיו (דהיינו ◊L{langue}), ב◊wiki-he{הנדסה הפוכה}‏◊;
◊numbered-note{
    כשהייתי רך בשנים אפילו כתבתי על זה ◊old-blog["26"]{פוסט פולמי}.
}.
התוצאה הישירה של התבססות על יצוג כזה של פלט לשוני (קורפוס) היא שיש צורך בכמות גדולה שלו כדי שנוכל להפיק תוצאות מבוססות ומדוייקות, בפרט אם התופעה הנחקרת לא מאוד נפוצה בשימוש.
אני עובד בעיקר על שפה כתובה, ולכן אני נזקק לטקסטים כתובים.
כדי שאלה יהיו שימושיים לדרך העבודה שלי, שנעשית כולה בעזרת כלים ממוחשבים, אני צריך שהטקסט יופיע בצורה של קובץ שאפשר להריץ עליו כל מני שאילתות, להעתיק ממנו, ולערוך אותו בקלות: ◊wiki-en["plain text"]{קובץ טקסט פשוט}.
רצה הגורל ורוב הטקסטים שמעניין אותי לעבוד עליהם לא זמינים בקובץ דיגיטלי נוח, ולכן הייתי צריך להפיק כזה מספר מודפס או מסריקות.
◊old-blog["digitization"]{כאן} תיארתי את התהליך שפיתחתי למטרה הזאת; הוא דורש עבודה רבה אבל נותן תוצאות טובות.

להלן הפלט של התהליך עבור כמה ספרים.
השתמשו בו למטרות לימוד ומחקר, אבל זכרו: אנחנו, כחוקרות/ים של שפת מיעוט שנמצאת תחת איום, אורחות/ים של הדוברות/ים.
בבקשה רכשו עותק של כל ספר שתעשו בו שימוש נרחב; כך נוכל להועיל, ולו במעט לספרות הוולשית ולטיפוח השפה.
המלצות על חנויות יש במצגת; ספרים ישנים אפשר למצוא אצל ◊L{Siop Lyfrau’r Hen Bost}, כפי שמפורט שם, ואפשר בקלות לעשות משלוח בינ״ל במחיר סביר בהחלט.

◊table[#:class "booktab latin"]{
    ◊tr{
        ◊th{awdur.es}
        ◊th{blwyddyn}
        ◊th{llyfr}
    }
    ◊tr{
        ◊td{◊wiki["cy"]{Kate Roberts}}
        ◊td{1925}
        ◊td{◊link["https://gitlab.com/corpws-cymraeg/kate-roberts/o-gors-y-bryniau"]{O Gors y Bryniau}}
    }
    ◊tr{
        ◊td{}
        ◊td{1949}
        ◊td{◊link["https://gitlab.com/corpws-cymraeg/kate-roberts/stryd-y-glep/-/blob/master/stryd.xml"]{Stryd y Glep}}
    }
    ◊tr{
        ◊td{}
        ◊td{1959}
        ◊td{◊link["https://gitlab.com/corpws-cymraeg/kate-roberts/te-yn-y-grug"]{◊L{Te yn y Grug}}}
    }
    ◊tr{
        ◊td{}
        ◊td{1960}
        ◊td{◊link["https://gitlab.com/rwmpelstilzchen/y-lon-wen-raw/"]{Y Lôn Wen}}
    }
    ◊tr{
        ◊td{}
        ◊td{1962}
        ◊td{◊link["https://gitlab.com/rwmpelstilzchen/tywyll-heno-raw"]{Tywyll Heno}}
    }
    ◊tr{
        ◊td{}
        ◊td{1972}
        ◊td{◊link["https://gitlab.com/rwmpelstilzchen/atgofion-i-raw/-/blob/master/01-Kate%20Roberts.xml"]{Atgofion I}}
    }
    ◊tr{
        ◊td{◊wiki["cy"]{Dafydd Parri}}
        ◊td{1981}
        ◊td{◊link["https://gitlab.com/corpws-cymraeg/dafydd-parri/bwrw-hiraeth"]{Bwrw Hiraeth}}
    }
    ◊tr{
        ◊td{◊wiki["cy"]{Caradog Prichard}}
        ◊td{1961}
        ◊td{◊link["https://gitlab.com/rwmpelstilzchen/un-nos-ola-leuad-raw/"]{Un Nos Ola Leuad} (anghyflawn)}
    }
    ◊tr{
        ◊td{◊wiki["cy"]{John Griffith Williams}}
        ◊td{1969}
        ◊td{◊link["https://gitlab.com/rwmpelstilzchen/pigaur-ser-raw"]{Pigau’r Sêr} (anghyflawn)}
    }
}

ספרים דיגיטליים (◊L{e-lyfrau}), בעיקר חדשים, ניתן למצוא באתר ◊link["https://www.gwales.com/ebooks/?lang=CY"]{◊L{gwales.com}} ובאתרי ההוצאות לאור השונות.
אם תצטרכו עזרה בהתמודדות עם ◊link["https://www.defectivebydesign.org/"]{◊L{DRM}} כדי להשמיש ספר דיגיטלי למטרות מחקר, אשמח לעזור.



◊subsection[#:label "arwyddion"]{קורפוס שלטים, מודעות ופליירים}
◊margin-figure["fel-malwen.jpg"]{}

בביקור האחרון שלי בוויילס, לרגל ◊link["http://cyngresgeltaidd.bangor.ac.uk/"]{כנס} ו◊link["https://dysgucymraeg.cymru/dysgu/dod-o-hyd-i-gwrs/"]{אולפן}, צילמתי הרבה שלטים, מודעות ופליירים עבור התלמידות/ים בקורס.
רוב השימוש הכתוב בוולשית במרחב הציבורי הוא דו־לשוני, מה שהופך אותו לאידיאלי עבור לומדות/ים, אם כי לא תמיד ההקבלה היא אחד לאחד.
זה אולי לא הקורפוס הכי מרתק מבחינת התוכן אבל הוא שימושי ללימוד אוצר מילים ומבנים תחביריים בהקשר.
כל התמונות, כולל גרסה טקסטואלית שלהן◊;
◊numbered-note{
	הערות טכניות:
	עשיתי האחדה בשימוש באותיות רישיות;
	שורות שמורכבות כמעט לחלוטין ממלל אנגלי (כמו ברשימת השמות של הכנסיות) השמטתי;
	מסד הנתונים של תוכן השלטים זמין ◊link["https://gitlab.com/rwmpelstilzchen/anki-factory/blob/master/Cymraeg/signs/deck.yaml"]{כאן};
	הסקריפט שמפיק ממנו את הדף באתר זמין ◊link["https://gitlab.com/rwmpelstilzchen/digitalwords.net/blob/master/ling/cymraeg/arwyddion/generate.py"]{כאן};
	במקור מאגר השלטים היה אמור לשמש בשביל חפיסה לאנקי בלבד (ר׳ ב◊xref-id["anki"]{הסעיף הבא}), ולכן אם שורה מופיעה בשתי תמונות בגרסה הטקסטואלית היא תופיע רק בתמונה הראשונה.
},
זמינות ◊strong{◊xref["ling/cymraeg/arwyddion"]{כאן}}.



◊subsection[#:label "gwrando"]{הקלטות ושידורים}
◊margin-figure["cymraeg.svg"]{}◊;https://publicdomainvectors.org/en/free-clipart/Cymraeg-logo/51547.html

◊;בשלב ראשון, עוד לפני שלומדים את השפה ממש, כדאי להרגיל בה את האוזן, שהצלילים של השפה ואופן הנגינה שלה לא יהיו זרים לגמרי.
◊;ברשת אפשר לשמוע וולשית במגוון אתרים, ביניהם:

מקורות להאזנה למלל בוולשית:

◊bullet-list{
	ערוץ הרדיו ◊link["https://www.bbc.co.uk/radiocymru"]{◊L{Radio Cymru}}◊;
	◊numbered-note{
		בניגוד לערוץ הטלוויזיה ◊link["https://www.bbc.co.uk/tv/s4c"]{◊L{S4C}}, שזמין לצפיה רק בממלכה המאוחדת, לרדיו ניתן להקשיב מכל מחשב שמחובר לאינטרנט.
		אם תרצו לצפות בטלוויזיה וולשית, אפשר להשתמש ב־◊wiki-he{VPN} כדי לעקוף את החסימה על ידי התחברות דרך מחשב מתווך שנמצא בממלכה המאוחדת.
	}.
	לחצו ◊link["https://www.bbc.co.uk/sounds/play/live:bbc_radio_cymru"]{כאן} להאזנה לשידורים בזמן אמת או ◊link["https://www.bbc.co.uk/radiocymru"]{כאן} לבחירה של תוכנית מוקלטת.
	בפרט רלוונטית לענייננו התוכנית ◊link["https://www.bbc.co.uk/programmes/p02nrvyj"]{◊L{Pigion}}, שמיועדת ללומדות/ים; היא בוולשית קלה ובדיבור איטי וברור, והפרקים בה מלווים בגלוסר למילים היותר קשות.


	באתר ◊link["https://amgueddfa.cymru/"]{המוזיאון הלאומי הוולשי} תוכלו להאזין ל◊link["https://amgueddfa.cymru/casgliadau/storigwerin/"]{מספרות ומספרי סיפורים מרחבי וויילס}.
	כל הסיפורים מתומללים ומתורגמים, כך שניתן לעקוב אחרי ההקלטה ביחד עם הטקסט הכתוב והתרגום.


	יש תרגום חדש של התנ״ך לוולשית דיבורית קלה בשם ◊L{◊link["https://beibl.net"]{beibl.net}}.
	כל הספרים, אם אני לא טועה, מוקלטים.


	אם דיבור ספונטני הוא שמעניין אתכן/ם, ◊link["http://siarad.org.uk"]{◊L{Corpws Siarad Bangor}} הוא המקבילה הוולשית של ◊link["http://cosih.com/"]{מעמ״ד} העברי: כ־40 שעות של וולשית בדיבור ספונטני.
	כל ההקלטות מתומללות◊;
	◊numbered-note{
		אם תרצו לעבוד על ההקלטות עם תוכנת ◊link["http://www.fon.hum.uva.nl/praat/"]{◊L{Praat}} תצטרכו להמיר את הקבצים שזמינים באתר בפורמט ◊L{CHAT} לפורמט ◊L{TextGrid} ש־◊L{Praat} יודעת לעבוד איתו.
		הדרך היחידה שמצאתי לעשות את זה ובאמת עובדת היא בעזרת ◊link["https://tla.mpi.nl/tools/tla-tools/elan/"]{ELAN}: טוענים את קובץ ה־◊L{CHAT} ושומרים כ־◊L{TextGrid}.
	},
	מנותחות ומתורגמות.


	◊link["https://parallel.cymru/"]{◊L{Parallel.cymru}} הוא מגזין דו־לשוני מעולה.
	לא רק שכל התוכן בו דו־לשוני (כך שאם משהו לא ברור לכן/ם בהבנה של הטקסט הוולשי אפשר להעזר במקבילה האנגלית), אלא שגם לחלקו יש גרסת שמע בנוסף לגרסה הכתובה.


	אחרון חביב הוא מטא־מקור עשיר עם שם חמוד — ◊link["https://ypod.cymru/"]{◊L{Y Pod}} — שמרכז עשרות פודקאסטים בנושאים שונים, וכמה תחנות רדיו.◊;
	◊numbered-note{
		בהערת אגב: יש מקבילה עברית בשם „◊link["https://podcastim.org.il/"]{פודקאסטים}”.
	}
	יש חלוקה לפי קטיגוריות, כולל קטיגוריה של ◊link["https://ypod.cymru/learnwelsh"]{פודקאסטים שמתאימים ללומדות·ים}.
	אפשר להאזין דרך האתר, וקיימת גם ◊link["https://ypod.cymru/blog/ap-podlediadau-cymraeg/"]{אפליקציה} שמרכזת את הכל; אם תרצו להאזין בעזרת אפליקציה כללית, אני ממליץ על ◊link["https://f-droid.org/en/packages/de.danoeh.antennapod"]{◊L{AntennaPod}} לפודקאסטים ו־◊link["https://f-droid.org/en/packages/net.programmierecke.radiodroid2"]{◊L{RadioDroid}} לרדיו.◊;
	◊numbered-note{
		המלצות כלליות על אפליקציות חופשיות לאנדרואית תוכלו למצוא ◊xref["tech/android-libre/#g55243"]{כאן}.
	}
}

◊subsubsection[#:label "gwrando:youtube"]{ערוצים ב־◊L{YouTube}}

◊bullet-list{
	◊link["https://youtu.be/0muJdaEe3hU"]{הקלטות של דוברות ודוברים מאיזורים שונים בוויילס ומחוצה לה}.


	ב־◊link["https://wikitongues.org/"]{◊L{Wikitongues}} — מיזם שמתעד, אוסף ומנגיש הקלטות וידאו של דוברי שפות שונות מרחבי העולם — יש שלוש הקלטות בוולשית:
	◊bullet-list{
		◊link["https://youtu.be/fvtbdq3WiyU"]{Hywel Gwynfryn} (מגיש ב־◊L{BBC Cymru}; ר׳ ◊wiki["cy" "Hywel Gwynfryn"]{ויקיפדיה}).


		◊link["https://youtu.be/JUa_phPM77s"]{Sandra Wyn} (לא דוברת ילידית אלא דוברת ◊L{L2}).


		◊link["https://youtu.be/fQOCFYGadcQ"]{Nigel Morgan}.
	}


	◊link["https://www.youtube.com/user/colegcymraeg/videos?view=0&sort=p&flow=grid"]{הערוץ של ה־◊L{Coleg Cymraeg}}, גוף שתומך בשימוש בשפה הוולשית בהשכלה גבוהה (ר׳ ◊wiki["cy" "Y Coleg Cymraeg Cenedlaethol"]{ויקיפדיה}).


	◊link["https://www.youtube.com/user/AthroCymraeg/videos"]{הערוץ בשם ◊L{AthroCymraeg}} הוא אוצר אקלקטי של תסכיתים, סרטים ושלל שונות בוולשית.
	בפרט רלוונטית עבורנו ◊link["https://youtu.be/Bj_RomgE0aw"]{התסכית של הסיפור ◊L{Gofid}}, שנקרא בקורס.


	המון סרטונים שמיועדים ללומדות/ים, ברמות שונות, תוכלו למצאו ב◊link["https://www.youtube.com/c/DysguCymraegLearnWelsh/videos"]{ערוץ ◊L{Dysgu Cymraeg}}.
	רובם נוצרו כחלק מתוכנית הלימודים באולפנים (◊L{wlpanau}) וקורסים שפזורים ברחבי וויילס (ועכשיו גם באופן מקוון מכל מקום 😷).
	אם תרצו להשתתף בלימודים באופן מרוכז או בשיעורים שבועִיים, תוכלו למצוא מידע ◊link["https://dysgucymraeg.cymru/"]{כאן}.


	אמנם כאמור ה־◊L{S4C}, ערוץ הטלוויזיה הוולשי, זמין לצפיה רק מאיזור גיאוגרפי מסויים, אבל יש תכנים שלהן/ם גם ביוטיוב; שני הערוצים הרלוונטיים הם ◊link["https://www.youtube.com/c/S4C/videos"]{◊L{S4C}} ו־◊link["https://www.youtube.com/c/HanshS4C/videos"]{◊L{Hansh}}.


	ב◊link["https://www.youtube.com/c/WelshGovernmentOfficial/videos"]{ערוץ של השלטון הוולשי} (◊L{Llywodraeth Cymru}) התוכן אולי לא הכי מרתק בעולם, אבל יש לו יתרון חשוב: חלק גדול מהסרטונים כוללים כתוביות בוולשית, מה שמקל מאוד על ההבנה שלהם ללומדות/ים.


	ערוץ היוטיוב של ◊link["https://www.youtube.com/c/Theatr/videos"]{התיאטרון הלאומי הוולשי} גם הוא כולל כתוביות דו־לשוניות, אבל בו לא רק הקנקן מתאים לצרכינו אלא גם התוכן עשיר ומרתק: הקלטות מלאות של הצגות ושיח עליהן.
	ההצגות מגוונות, חדשות וישנות, והמשחק נהדר.
	זאת דרך מעולה לא רק להעמיק את הידיעה של השפה אלא גם להחשף לרבדים נוספים של התרבות; בשיעורים התמקדנו בעיקר בתרבות ספרותית כתובה, וכאן יש לנו הזדמנות להרחיב גם לתרבות של הדרמה על הבמה.
	

	למעלה דיברנו על ◊L{comprehensible input}.
	יש ◊link["https://www.youtube.com/channel/UCNtU6lkGwuoNHJB_FMZjK5w/videos"]{ערוץ יוטיוב} שמתמחה בדיוק בזה: סרטונים שמתוך הקונטקסט ובעזרת עזרים חיצוניים כמו ציורים, הבעות פנים ומימיקה מאפשרים להבין בקלות מלל בוולשית.


	בהקשר הזה, סיפורים לילדים הם כלי מעולה ללימוד שפה מחשיפה:
	◊bullet-list{
		שפה שמותאמת למי שעדיין לא רכשו את כל מורכבות השפה? ✅.


		חזרה תבניתית ומשפטים קצרים?  ✅.


		ציורים שעוזרים להבין מה קורה? ✅.


		גם ספרות ילדים מקורית וגם ספרות ילדים מתורגמת שאולי כבר מוכרת לכן·ם?  ✅.


		שילוב של טקסט ומלל, ובפרט מלל דרמטי ובטון מותאם לילדים, שעוזר להבין? ✅.
	}
	יש לא מעט סרטונים של סיפורי ילדים בוולשית.
	הכי פשוט לחפש ◊link["https://www.youtube.com/results?search_query=stori+Cymraeg"]{◊L{stori Cymraeg}}, לעבור על התוצאות ולהסתכל גם על התוכן בערוצים שהעלו את הסרטונים שבתוצאות.


	מה יותר טוב ממשחקי מחשב + וולשית?
	◊link["https://linktr.ee/YnChwarae"]{◊L{Yn Chwarae}} הוא ערוץ ביוטיוב ובטוויץ׳ של א·נשים שמשחקות·ים משחקי מחשב ומדברות·ים על זה בוולשית.
	חלק מהמשחקים, כמו פוקימון הקלאסי, אפילו בוולשית!
}


◊subsubsection[#:label "gwrando:cerddoriaeth"]{מוזיקה}

◊bullet-list{
	◊link["https://brigyn.com/"]{◊L{Brigyn}} הוא הרכב של שני אחים מחבל ◊wiki["cy" "Eryri"]{◊L{Eryri}}.
	הם עושים מוזיקה נהדרת, ולכל השירים שלהם יש מילים באתר או ב־◊link["https://brigyn.bandcamp.com/"]{◊L{bandcamp}}, כך שבשביל מי שלומדות/ים וולשית זה בדיוק מתאים.
}







◊section[#:label "anki"]{חפיסות לאנקי}
◊margin-figure["srs.png"]{}◊;https://d16qt3wv6xm098.cloudfront.net/--WD2D2kRNeq4CHothPq2OhfSCqmkmn5/_.jpg

כלי העזר הרביעי שהכנתי הוא חפיסות של כרטיסים ל◊wiki-he["אנקי (תוכנה)"]{אנקי} (◊L{Anki}), תוכנה שמאפשרת ללמוד ולשנן באופן יעיל.
בהקשר של רכישת שפה היא שימושית בעיקר לאוצר מילים, צורות נטיה ודרכי ביטוי.
הרעיון הוא פשוט: מוצגת או מושמעת מילה או משפט בשפה שאת/ה לומד/ת ואת/ה צריך/ה לענות מה הפירוש (או, בכיוון ההפוך, מוצג הפירוש וצריך לענות בשפה הנרכשת).
הצלחת? התוכנה תרחיק◊;
◊numbered-note{
	בגידול ◊wiki-he["פונקציה מעריכית"]{מעריכי} עם ◊wiki-he["חזקה (מתמטיקה)"]{בסיס} דיפולטיבי 2.5 שיכול להשתנות לפי דרגת הקושי, מה שאומר שבאופן אידיאלי ההשקעה בכל כרטיס היא ◊wiki-he["לוגריתם"]{לוגריתמית} על פני הזמן, שזה מצויין.
}
את הפעם הבא שהמילה או המשפט יופיעו;
לא הצלחת? התוכנה תציג את הכרטיס שוב בקרוב ותקצר את טווח הזמן שבין החזרות שלו.
ככה התוכנה יכולה להזכיר את פריט המידע רגע לפני שאת/ה שוכח/ת אותו, מה שאומר שאת/ה משקיע/ה את מינימום הזמן בשינון: בלי לחזור על הידוע והמוכר, אבל גם בלי לחכות עד שמה שלמדתן כבר נשכח.◊;
◊numbered-note{
	אני משתמש באנקי כבר יותר משנה ואני יכול להעיד שהיא ◊em{מאוד} אפקטיבית.
	בעזרת אנקי וחילופי שפה הצלחתי להגיע לרמה דיבורית בנורווגית ממש מהר, מה שלא הייתי יכול לעשות לולי אנקי.
}
אם זה משהו שמעניין אותך (ו, כאמור, כדאי שזה יעניין אותך לאור זה שרכישת שפה היא תהליך שלוקח הרבה זמן וטוב לייעל אותו כמה שניתן), יש באתר ◊xref["anki"]{דף על אנקי}.

אז הכנתי כמה חפיסות ללימוד וולשית.
השימוש בהן הוא כמובן לא חובה בקורס, אבל יכול לעזור מאוד בלימוד השפה.
אני ממליץ לקחת את החפיסות ולאחד אותן לחפיסה גדולה אחת באופן מסורג: א׳ ב׳ ג׳ א׳ ב׳ ג׳ וחוזר חלילה.

אגב, בעזרת ◊xref["anki/lookup"]{הטריק הזה} בכל החפיסות לחיצה על מילה וולשית תוביל לערך ב־◊link["http://www.geiriadur.ac.uk/"]{◊L{Geiriadur Prifysgol Cymru}}.



◊subsection[#:label "anki:cbac"]{רשימת המילים של ◊L{CBAC}}

חפיסה אחת מתמקדת באוצר מילים.
היא מבוססת על אוצר המילים שמוצע על ידי ◊L{◊wiki["cy"]{CBAC}}◊;
◊numbered-note{
	◊L{◊wiki-en["WJEC (exam board)"]{WJEC}} באנגלית, גוף וולשי שעוסק במבחנים, דומה חלקית ל◊wiki-he["מרכז ארצי לבחינות ולהערכה"]{מרכז הארצי לבחינות ולהערכה}.
}
עבור שלוש הרמות הראשונות:
◊link["https://cbac.co.uk/qualifications/welsh-for-adults/welsh-for-adults-entry/Mynediad%20-%20geirfa%20graidd-e.pdf"]{◊L{mynediad}} („כניסה”, מקבילה ל־◊L{A1} ב◊wiki-he["המדד האירופי להערכת שפות"]{מדד האירופי להערכת שפות}),
◊link["https://cbac.co.uk/qualifications/welsh-for-adults/welsh-for-adults-foundation/Sylfaen%20Core%20Words.pdf"]{◊L{sylfaen}} („יסוד, בסיס”, מקבילה ל־◊L{A2})
ו־◊link["https://cbac.co.uk/qualifications/welsh-for-adults/welsh-for-adults-intermediate/Canolradd%20Core%20Words.pdf"]{◊L{canolradd}} („דרגה אמצעית”, מקבילה ל־◊L{B1}).
לקחתי את רשימת המילים (יותר מ־2,600 ערכים…), תרגמתי אותה, יבאתי אותה לאנקי, הוספתי הקלטות מ־◊L{◊link["https://forvo.com/"]{Forvo}} ו(הכי חשוב…) אימוג׳ים.
כל רמה ברשימות של ◊L{CBAC} מסודרת לפי סדר אלפביתי במקור; את החפיסה בחרתי לסדר לפי הרמות, כמובן, אבל בתוך כל רמה הסדר אקראי, כי ללמוד בסדר אלפביתי זה מאוד מבלבל.
את התוצאה תוכלו להוריד ◊strong{◊link["https://ankiweb.net/shared/info/1600775344"]{כאן}}.

המילים ש־◊L{CBAC} בחרו הן די דיבוריות, ולכן חלקן פחות מתאימות לשפה ספרותית.
יש גם המון אנגליציזמים; מילים מאנגלית שיש להן מקבילה וולשית רגילה בשימוש נפוץ מסומנות בחפיסה, אבל לא כולן ולא באופן שיטתי.

בכרטיסים שמציגים עברית בצד השאלה ומצפים לתשובה בוולשית יש בעיה עקרונית: מילים נרדפות וקרובות.
אם לדוגמה הופיעה לכן/ם המילה העברית „אשה”, האם התשובה הנכונה היא ◊L{◊link["http://geiriadur.ac.uk/gpc/gpc.html?dynes"]{dynes}},‏ ◊L{◊link["http://geiriadur.ac.uk/gpc/gpc.html?gwraig"]{gwraig}},‏ ◊L{◊link["http://geiriadur.ac.uk/gpc/gpc.html?benyw"]{benyw}} או אולי ◊L{◊link["http://geiriadur.ac.uk/gpc/gpc.html?menyw"]{menyw}}?
אם עניתן/ם תשובה אחת ועל המסך הופיעה אחרת, האם זה נחשב כטעות?
דרך אחת לפתור את הבעיה הזאת היא לכתוב בצד השאלה רשימה של מילים קרובות שאינן התשובה המסויימת של הכרטיס הזה.
במקרה דנן עבור הכרטיס „אשה←◊L{gwraig}” יופיעו בצד השאלה המילים ◊L{dynes},‏ ◊L{benyw} ו־◊L{menyw} מסומנות ב־×.
גם מילים רחוקות יותר שעלולות לבלבל מופיעות, בתוך סוגריים, אבל לא באופן שיטתי.




◊subsection[#:label "anki:arwyddion"]{קורפוס השלטים}

לימוד אוצר מילים במנותק מהקשר הוא בעייתי ועלול להיות מטעה.
חשבו, נגיד, על לימוד של מילה כמו ◊L{get} באנגלית בדרך כזאת, שיש לה ◊link["https://en.wiktionary.org/wiki/get#Verb"]{31 פירושים בוויקימילון}.
זה מוביל אותנו לחפיסה הבאה.
חפיסות שבנויות לא ממילים בודדות אלא ממשפטים הן הרבה יותר מוצלחות מחפישות של אוצר מילים: הן נותנות דוגמאות bite-sized של שפה בשימוש חי, לא משהו מופשט ונטול־הקשר אלא קונקרטי וברור.

ממאגר הצילומים של השלטים יצרתי חפיסה לאנקי◊;
◊numbered-note{
	יש תוסף לאנקי שנקרא ◊L{◊link["https://ankiweb.net/shared/info/1374772155"]{image occlusion}} (◊link["https://www.youtube.com/results?search_query=Anki+%22image+occlusion%22"]{סרטוני הסבר}).
	לי אין הזמן והסבלנות להשתמש בו וליצור כרטיסים גרפיים שכל פעם חלק אחד בהם חסר.
	אם לכן/ם יש את הזמן, הסבלנות והרצון לעשות את זה, אשמח להעלות את התוצאה לכאן לרווחת הכלל.
},
שניתן להוריד ◊strong{◊link["https://ankiweb.net/shared/info/617887546"]{כאן}}.◊;
◊numbered-note{
	מסד הנתונים הוא ◊link["https://gitlab.com/rwmpelstilzchen/anki-factory/blob/master/Cymraeg/signs/deck.yaml"]{אותו מסדר הנתונים ששימש ליצירת הדף באתר}; הסקריפט ליצירת קובץ ה־◊wiki-en["Tab-separated_values"]{◊L{TSV}} שייבאתי לאנקי זמין ◊link["https://gitlab.com/rwmpelstilzchen/digitalwords.net/blob/master/ling/cymraeg/arwyddion/generate_tsv.py"]{כאן}.
}
החפיסה כוללת כולל גם משפטים ארוכים, שפחות מתאימים ללימוד בשיטה הזאת ואפשר פשוט למחוק אותם אם הם מכבידים.



◊subsection[#:label "anki:audio"]{משפטים עם שמע}

אפילו יותר טוב מחפיסות עם משפטים דו־לשוניים הן חפיסות שיש בהן גם שמע.
ככה אפשר להתאמן גם על הבנת הנשמע ועל ההיגוי, המקצב והאינטונציה בעזרת ◊L{◊wiki-en["Speech shadowing"]{shadowing}}◊;
◊numbered-note{
	אם אתן/ם לא מכירות את הטכניקה, כדאי להכיר, במיוחד אם יש לכן/ם כוונה ללמוד את השפה לרמה כזאת שתוכלו גם לדבר בה (להפיק מלל ולקיים שיחה, ולא רק לקרוא, לכתוב ולהקשיב).
		ההיבט האוטומטי, גם המנטלי וגם המוטורי, חשוב לא פחות מהיכולת לנתח שפה באופן מודע: הראשון הכרחי לרכישת שפה (◊L{language acquisition}), בעוד שללימוד שפה (◊L{language learning}) מספיק השני.
		כמו עם ה־◊L{comprehensible input} קודם, גם כאן כדאי לצפות ב◊link["https://www.youtube.com/results?search_query=shadowing"]{סרטונים המובילים ביוטיוב}.
}.
מכל הדרכים, לימוד בעזרת משפטים עם שמע היא הדרך הכי אפקטיבית שאני מכיר עד שמגיעים לרמת שליטה שבה אפשר פשוט לעשות ◊L{immersion} (לקרוא, להקשיב, לשוחח באופן חופשי ולקלוט את החסר בלי השקעה מכוונת).

יצרתי◊;
◊numbered-note{
	אם מעניין אתכן/ם הצד הטכני, תוכלו לקרוא עליו ב◊old-blog["anki#_13"]{פוסט שכתבתי}.
	בגדול, השתמשתי ב־◊link["http://www.fon.hum.uva.nl/praat/"]{◊L{Praat}} כדי לחתוך קטעים מהטקסט, ושילבתי את הקטעים החתוכים עם המשפטים הדו־לשוניים הכתובים לידי חפיסה אחת.
	את ◊L{Praat} כל בלשן.ית שעוסק.ת בשפות חיות צריך.ה להכיר.
}
בינתיים חפיסה עבור סיפור שנקרא בכיתה, סיפור עצוב שכבר צילק את נפשן/ם של תלמידות/י החוג בעבר: ◊L{Gofid} „יגון” מאת ◊wiki-he{קייט רוברטס}, מקובץ הסיפורים ◊L{Te yn y Grug} „תה בין שיחי האברש”, שזכה ל◊link["https://sainwales.com/cy/store/sain/sain-scd-2471"]{הקראה} בפי להקראה בפי Bethan Dwyfor ו־Merfyn Pierce Jones.
◊strong{◊link["https://ankiweb.net/shared/info/90991462"]{הורדה}}.
בעתיד אני מתכוון ליצור חפיסות גם משאר הסיפורים שבקובץ.

◊;{
	השני הוא הרצאת רדיו מכוננת של ◊wiki-en{Saunders Lewis}, בשם ◊wiki-en{Tynged yr Iaith} „גורל השפה”.
	הטקסט הדו־לשוני וההקלטה לקוחים מ◊link["https://parallel.cymru/tynged-yr-iaith/"]{הדף הזה} מהאתר המעולה ◊L{◊link["https://parallel.cymru/"]{Parallel.cymru}}.
	◊strong{◊xref["anki/my-decks/Cymraeg: Tynged yr Iaith.apkg"]{הורדה}}.
}



◊section[#:label "cyhoeddiadau"]{הפניות למקורות חיצונים}

◊margin-figure["bangor.jpg"]{◊link["https://www.bangor.ac.uk/welshlibrary/shankland.php.cy"]{אולם הקריאה על שם שנקלנד}, ◊link["https://www.bangor.ac.uk/"]{אוניברסיטת בנגור}}◊;https://pbs.twimg.com/media/C6-HfW7W0AALNi1?format=jpg&name=large
אנחנו לומדים וולשית לא רק כקורס שאמור להקנות יכולת לשונית אלא בראש ובראשונה כקורס בלשני.
יש ספרות מחקרית ענפה על השפה; אמנם פחות מבשפות מוכרות יותר, אבל בהחלט יש עיסוק בלשני בוולשית לשלביה.

הספרים שעוסקים בשפות קלטיות ב◊link["https://mslib.huji.ac.il/"]{ספריה} נמצאים במדף הירוק ◊L{PB} שבקומה ארבע.
זה איזור בספריה שיקר מאוד ללבי, אבל האמת חייבת להאמר שהוא לא מאוד עשיר, וגם בתחום של גישה למאגרים אלקטרוניים מהאוניברסיטה העברית יש לקוּנוֹת.

פתרון אחד לגישה לספרים שלא נמצאים בספריה הוא להשתמש באתרים כמו ◊L{◊wiki-en{Library Genesis}} או ◊L{◊wiki-he{Sci-Hub}}, אם זה תואם את עולם הערכים שלכן/ם.
עם השנים גיבשתי לי ספריה דיגיטלית◊;
◊numbered-note{
	קטלוג של הספריה הדיגיטלית שלי ◊link["https://gitlab.com/rwmpelstilzchen/bibliography.bib/blob/master/bibliography.bib"]{זמין ב־◊L{GitLab}} כקובץ ◊link["https://www.ctan.org/pkg/biblatex"]{Bib◊LaTeX{}}.
}
ופיזית של ספרים שעוסקים בשפות שאני חוקר.
אשמח לחלוק, לשלוח קבצים ולהשאיל ספרים לפי הצורך.

כמה מהמקורות החיצוניים שהפנתי אליהם בשיעורים העלתי ל◊strong{◊xref["ling/cymraeg/cwrs/allanol"]{כאן}}.



◊section[#:label "gwaith-cartref"]{שיעורי־בית}

שיעורי־הבית מהשיעור השני זמינים ◊xref["ling/cymraeg/cwrs/gwaith-cartref/02"]{כאן}.

החל מאמצע הסמסטר הראשון התחלנו ◊xref-local["egluriadur"]{לקרוא טקסטים} בסבב בשיעור.
שיעורי הבית הקבועים הם להכין קדימה בטקסט כדי לקרוא, לתרגם, לנתח ולהוסיף הערות בשיעור.

◊margin-figure-frame["gwaith-cartref/Białowieża/Tu Mewn i Goedwig Hynaf Ewrop-o0zs-p8kKe0.webp"]{שריד תָּחוּם שמהווה המשך ישיר למשהו שפעם היה קיים באיזורים נרחבים של אירופה — התיאור הזה מתאים הן ליער הקדמוני והן לשפות הקלטיות.}
הטקסט האחרון שלנו הוא המיני־דוקו של ◊link["https://www.facebook.com/hanshs4c"]{ערוץ Hansh} של ◊link["http://s4c.cymru/"]{S4C} ו־◊link["https://www.tomdiserens.com/"]{◊L{Tom Diserens}}, אקולוג וביולוג שכותב דוקטורט על יונקים ב◊wiki-he{יער ביאלובייסקה}.
פירקתי את הקובץ המקורי למרכיבים בעזרת ◊link["https://yt-dl.org/"]{◊L{youtube-dl}} ותוכנות נוספות כדי להקל על העבודה עליו:

◊bullet-list{
	בסרט כמות שהוא תוכלו לצפות ◊link["https://youtu.be/o0zs-p8kKe0"]{ביוטיוב}.
	יש לו כתוביות באנגלית, אך לא בוולשית.
	זה מעולה למטרה שלנו: כך נוכל להתמקד בנסיון לפענח את המלל הוולשי הדבור ללא תיווך של תמליל מצד אחד, אבל בלי להיות אבודות/ים לגמרי מצד שני.


	אם תרצו להוריד את קובץ הווידאו כדי לצפות בו באופן מקומי או לעבד אותו עם ◊link["https://tla.mpi.nl/tools/tla-tools/elan/"]{ELAN}, הוא זמין ◊xref-local["gwaith-cartref/Białowieża/Tu Mewn i Goedwig Hynaf Ewrop-o0zs-p8kKe0.mkv"]{כאן}.


	התרגום האנגלי בקובץ כתוביות ◊wiki-en["WebVTT"]{◊L{WebVTT}}, שאפשר לפתוח בכל ◊wiki-he{עורך טקסט}, זמין ◊xref-local["gwaith-cartref/Białowieża/Tu Mewn i Goedwig Hynaf Ewrop-o0zs-p8kKe0.en.vtt"]{כאן}.


	לצערי ◊link["http://www.fon.hum.uva.nl/praat/"]{◊L{Praat}} לא יודעת לעבוד עם פורמטים מתקדמים של שמע (כמו פורמט ◊wiki-he["אופוס (מקודד)"]{אופוס} שמוטמע בתוך קובץ ה◊wiki-he["MKV"]{מטרושקה} לדעיל), ולכן המרתי אותו לפורמט ◊wiki-en["WAV"]{◊L{WAV}}, שתוכלו להוריד ◊xref-local["gwaith-cartref/Białowieża/Tu Mewn i Goedwig Hynaf Ewrop-o0zs-p8kKe0.wav"]{כאן}.


	כדי לחסוך לכן/ם את העבודה הטכנית שבחלוקה ידנית של קובץ השמע לסגמנטים, עבודה שחלקה הגדול הוא טכני ולא בלשני, העלתי ◊xref-local["gwaith-cartref/Białowieża/Tu Mewn i Goedwig Hynaf Ewrop-o0zs-p8kKe0.TextGrid"]{לכאן} גם קובץ ◊L{TextGrid} ריק, שמתאים לעבודה עם ◊L{Praat} ו־◊L{ELAN} (לקחתי את הקובץ שבו תמללתי את השמע ובחרתי באפשרות של הסרת כל הטקסט).
}



◊section[#:label "profion"]{מבדקי־בית ומטלה מסכמת}
◊margin-figure["speech-circuit.png"]{}◊;https://www.researchgate.net/profile/Timothy_Crow/publication/7207002/figure/fig1/AS:394523332104198@1471073100483/The-relationship-between-speaker-and-hearer-according-to-de-Saussure-1916.png

◊subsection[#:label "profion-A"]{שנה א׳}

במהלך השנה התקיימו שלושה מבדקי־בית: קצת אחרי אמצע הסמסטר הראשון, בין הסימסטרים ובאמצע הסימסטר השני.
המטרה שלהם היא לתת לתלמידות/ים הערכה פרטנית של התקדמותן/ם ולי תמונת מצב של הכיתה, כפרטים וכקבוצה.
לצערי אין לי ברירה אלא לתת ציונים; המשקל הכולל של המבדקים הוא 25% מהציון הסופי.

◊bullet-list{
	◊xref-local["profion/prawf-1"]{המבדק הראשון}


	◊xref-local["profion/prawf-2"]{המבדק השני} (◊xref-local["profion/prawf-2/atebion.html"]{פתרון})


	◊xref-local["profion/prawf-3"]{המבדק ההשלישי} (◊xref-local["profion/prawf-3/atebion.html"]{פתרון})
}

משקלה של ◊xref-local["profion/prawf-olaf"]{המטלה המסכמת} (◊xref-local["profion/prawf-olaf/atebion.html"]{פתרון}) הוא 50% מהציון הסופי.



◊subsection[#:label "prawf-B"]{שנה ב׳}

משקלה של ◊xref-local["profion/prawf-olaf-2"]{המטלה המסכמת} (◊xref-local["profion/prawf-olaf-2/atebion.html"]{פתרון}) הוא 70% מהציון הסופי.


◊section[#:label "ieitheg hanesyddol"]{נושאים של שינוי לשוני}
◊margin-figure["stammbaum.jpg"]{}

בכמה הזדמנויות ביקשו ממני תלמידות/ים הפניות לנושאים של שינוי לשוני בוולשית, בשביל לכתוב עליהם בקורסים אחרים.
כדי שכלל התלמידות/ים יוכלו להפיק מכך תועלת העלתי ◊strong{◊xref-local["hanesyddol"]{רשימה של נושאים}} לאתר.
היא מאוד חלקית בשלב זה, ובעתיד אולי אוסיף לה נושאים
