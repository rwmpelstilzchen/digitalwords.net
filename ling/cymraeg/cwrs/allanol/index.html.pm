#lang pollen

◊define-meta[title]{חומרים חיצוניים}
◊define-meta[publish-date]{2019-10-26}
◊define-meta[last-update]{2021-06-08}
◊define-meta[hide-from-sitemap]{true}
◊define-meta[toc]{true}



◊section{וולשית מודרנית}
◊margin-figure-frame["../bangor.jpg"]{◊link["https://www.bangor.ac.uk/welshlibrary/shankland.php.cy"]{אולם הקריאה על שם שנקלנד}, ◊link["https://www.bangor.ac.uk/"]{אוניברסיטת בנגור}}◊;https://pbs.twimg.com/media/C6-HfW7W0AALNi1?format=jpg&name=large

◊subsection{ספרות יעץ}


◊subsubsection[#:label "gramadegau"]{דקדוקים}

◊bullet-list{
	ספר הדקדוק של ◊L{Thorne} (◊xref-local["Thorne, D. A. - 1993 - A comprehensive Welsh grammar.pdf"]{קובץ סרוק}; עותק בספריה: ◊code{PB 2123 T46}).


	ככל הידוע לי, לספר ◊L{Gramadeg y Gymraeg} של ◊L{Peter Wynn Thomas}, שכתוב בוולשית והוא הנרחב, המקיף והמעמיק ביותר, אין עותק דיגיטלי.
	עותק פיזי זמין בספריה, ב־◊code{PB 2123 T45}.
}


◊subsubsection[#:label "geiriaduron"]{מילונים}

◊bullet-list{
	המילון הגדול והחשוב ביותר לשפה הוא ה־◊link["http://www.geiriadur.ac.uk/"]{◊L{Geiriadur Prifysgol Cymru}}.

	זה מילון מקוון, אבל בעזרת ◊link["http://www.geiriadur.ac.uk/apiau-android-ac-ios/"]{האפליקציה לטלפון} תוכלו להשתמש בעותק אופליין בטלפון.
	האפליקציה מיושנת◊;
	◊numbered-note{
		אם אני מבין נכון, זאת הסיבה שהיא דורשת הרשאות מוגזמות.
		היא פשוט נכתבה בזמן שמערכת ההרשאות עוד לא היתה בשלה מספיק.
	}, אבל עובדת.
	אין בנמצא קובץ סטטי של המהדורה החדשה (השניה, שמתהווה), אבל יש ◊xref-local["2002 - Geiriadur Prifysgol Cymru - fersiwn cryno o'r argraffiad cyntaf.pdf"]{גרסה מקוצצת של המהדורה הראשונה} (ב־◊link["https://libgen.is/book/index.php?md5=95896B21E57619F726A9BCAB9203024F"]{◊L{LibGen}} יש גרסה פחות נוחה, שמחולקת למספר קבצים).
	אם תרצו להתאמן בהרמת משקולות, תוכלו למצוא את כרכי המהדורה הראשונה בספריה ב־◊code{PB 2191 G45}, ולצדם חוברות של המהדורה השניה.

	אפשר לחפש מילים דרך הממשק של האתר, אבל שיטה זריזה יותר היא להשתמש במילת מפתח דרך שורת הכתובת של הדפדפן.◊;
	◊numbered-note{
		קישור ישיר לחיפוש במילון ובחירת התוצאה הראשונה בנוי לפי המבנה ◊link["http://geiriadur.ac.uk/gpc/gpc.html?cath"]{◊code{http://geiriadur.ac.uk/gpc/gpc.html?cath}} (כשאת ◊L{cath} נחליף, כמובן, באיזו מילה שנרצה).
		אפשר לנצל את זה בשביל ליצור חיפושים מהירים.
		השורות הבאות הן הוראות ל◊link["https://www.mozilla.org/cy/firefox/"]{פיירפוקס} (עם ממשק וולשי, כמובן), אבל אני בטוח שיש דרך מקבילה לזה בכל דפדפן מודרני.
		מה שנרצה לעשות הוא ליצור סימניה (בוקמרק) שב־◊L{lleoliad} שלה כתוב ◊code{http://geiriadur.ac.uk/gpc/gpc.html?%s} וב־◊L{allweddair} שלה כתוב ◊code{gpc}, ואז נוכל להקליד ◊code{gpc cath} בשורת הכתובת ולהגיע ישר לדף המתאים במילון.
		כדי להוסיף סימניה חדשה באופן כזה שמאפשר לערוך את ה־◊L{allweddair} נבחר ◊L{Nodau Tudalen → Dangos Pob Nod Tudalen} (קיצור דרך ◊L{Ctrl+Shift+O}) ואז על ◊L{Trefnu → Nod Tudalen Newydd}.
		טכניקה אחרת, שעובדת באתרים אחרים אבל שמשום מה לא עובדת ב־◊L{GPC} היא ללחוץ מקש ימני על שורת החיפוש ולבחור ב־◊L{Ychwanegu Allweddair i’r Chwilio}.
	}


	הספר ◊link["https://archive.org/details/welshvocabularyo00fyneuoft/"]{◊L{The Welsh vocabulary of the Bangor District}} מאת ◊L{Fynes-Clinton} הוא אוצר בלום של מידע על וולשית צפונית מתחילת המאה העשרים, מה שרלוונטי במיוחד בקריאה של הדיאלוגים אצל ◊wiki-he{קייט רוברטס} או ספרים כמו ◊wiki["cy" "Un Nos Ola Leuad"]{◊L{Un Nos Ola Leuad}}, שכתוב כולו בשפה דיבורית מאוד.


	◊xref-local["LexiconWE_main.par.txt"]{המילון של ◊L{Mark Nodine}}.
	הוא חלקי הן בכיסוי והן בעומק, אבל היתרון שלו הוא שהוא בקובץ טקסט פשוט, מה שמאפשר להריץ עליו שאילתות בלי לחכות לתגובה של השרת המרוחק.


	מיזם וויקימילון בוולשית נקרא ◊link["https://cy.wiktionary.org/"]{◊L{Wiciadur}} (לפי ◊L{Geir·iadur}).
	גם ◊link["https://en.wiktionary.org/"]{◊L{Wiktionary}} האנגלי כולל לא מעט מילים וולשיות.
	במילון מופיעות טבלאות נטיה, ואפשר לחפש גם לפי מילים בצורה הנוטה (לדוגמה, חיפוש ◊link["https://en.wiktionary.org/wiki/Special:Search?search=gwyddwn&ns0=1"]{◊L{gwyddwn}} יוביל אל ◊link["https://en.wiktionary.org/wiki/gwybod"]{◊L{gwybod}}), אם כי לא בשילוב של הצורה הנוטה ומוטציות ביחד (לדוגמה, ◊L{wyddwn} לא יניב תוצאות).
	הן טבלאות הנטייה והן טבלאות המוטציות מופקות באופן אוטמוטי למחצה, ועלולות לכלול טעויות; מצד שני, כאן בניגוד למילונים האחרים כל אחת·ד יכול·ה לתקן את הטעויות בקלות…
}



◊subsection{מחקר בלשני}

◊subsubsection{ה־◊L{yn}־ים}

◊bullet-list{
	◊link["https://onlinelibrary.wiley.com/doi/pdf/10.1111/1467-968X.12052"]{מאמר של ◊L{Sims-Williams} על ארבעת סוגי ה־◊L{yn}} (ב־◊link["https://sci-hub.se/https://onlinelibrary.wiley.com/doi/pdf/10.1111/1467-968X.12052"]{◊L{Sci-Hub}}).


	◊link["https://www.jstor.org/stable/3086707"]{מאמר של ◊L{Gensler} על ההתגלגלות של ◊L{yn} הנשואי} (ב־◊link["https://sci-hub.se/https://www.jstor.org/stable/3086707"]{◊L{Sci-Hub}}).
}



◊subsubsection{מערכת המוטציות}

◊bullet-list{
	◊link["https://dx.doi.org/10.5334/gjgl.1007"]{מאמר ◊L{Hammond} ואח׳ (2020)}, שעוסק בהשפעה של קטיגוריות שונות על ההתנהגות של מערכת המוטציות.
	הגישה של המאמר מעניינת, ניסויית, ואינה נטולת מכשולים ובעיות.


	◊link["https://doi.org/10.1002/9781444335262.wbctp0117"]{מאמר של ◊L{Hannahs} (2011)} על המוטציות הקלטיות בכרך השלישי של ה־◊L{companion} של ◊L{Blackwell} לפונולוגיה (ב־◊link["https://sci-hub.se/https://doi.org/10.1002/9781444335262.wbctp0117"]{◊L{Sci-Hub}}).


	◊link["https://doi.org/10.1002/9781444335262.wbctp0065"]{מאמר של ◊L{Grijzenhout} (2011)} על מוטציות של עיצורים באופן כללי, באותו הספר (ב־◊link["https://sci-hub.se/https://doi.org/10.1002/9781444335262.wbctp0065"]{Sci-Hub}).


	◊link["https://aisberg.unibg.it/handle/10446/115"]{מאמר של ◊L{Phillips} (2007)} על תת־מערכות נדירות ובסכנת הכחדה בשפות קלטיות ובפרט בוולשית; כולל סעיף על המוטציות.


	◊link["https://www.academia.edu/42202931"]{מאמר של ◊L{Zimmer} (2005)} על המוטציות הקלטיות מנקודת מבט טיפולוגית.


	ספר בעריכת ◊L{Fisiak} (1996) שבו יש מאמר מאת ◊L{Hickey} על המוטציות הקלטיות, ושינויים הגאיים וטיפולוגיים (ב־◊link["http://libgen.is/book/index.php?md5=9B0B4402E5CCA7551B6540197DD9977D"]{◊L{LibGen}}).


	ספר של ◊L{Ball} (1992) על המוטציות בוולשית (ב־◊link["http://libgen.is/book/index.php?md5=75F9C0D1CD9A0589355D41A0267A0FFB"]{◊L{LibGen}}).


	מאמר של ◊L{Martinet} (1952) על ריכוך בקלטית בהשוואה לעיצורים בשפות רומאניות מערביות (ב־◊link["https://www.jstor.org/stable/410513"]{◊L{JSTOR}}; ב־◊link["https://sci-hub.se/https://www.jstor.org/stable/410513"]{◊L{SciHub}}).◊;
	◊numbered-note{
		ר׳ עמ׳ 710 (◊L{§ 132}) בספר ◊L{Language and History in Early Britain} של ◊L{Jackson} (ר׳ ◊xref-local["#celteg"]{למטה}) להתייחסות לנקודות שבהן הידע המדעי התקדם מאז המאמר של מרטינה.
	}
}



◊subsubsection[#:label "contact"]{לשונות במגע}

◊bullet-list{
	◊link["https://www.routledge.com/Language-Contact-and-the-Origins-of-the-Germanic-Languages-1st-Edition/Schrijver/p/book/9781138245372"]{ספר של ◊L{Schrijver} (2013)} שעוסק במקור של שפות גרמאניות ומגע בין לשונות עוסק, בין היתר, בהשפעה קלטית (ב־◊link["https://libgen.is/book/index.php?md5=C498985A3BBC973885F0C710B3E90E51"]{◊L{LibGen}}).


	ב־2009 יצא ◊link["https://www.cambridge.org/core/journals/english-language-and-linguistics/issue/reevaluating-the-celtic-hypothesis/2C5F3D0D01E61DDA24A6AC08FA0DE0B5"]{גליון מיוחד} של כתב־העת ◊link["https://www.cambridge.org/core/journals/english-language-and-linguistics"]{◊L{ English Language & Linguistics}} שעוסק בהשפעה קלטית על אנגלית.


	ספר של ◊L{Filppula} ואח׳ (2008) על המגע בין שפות קלטיות ואנגלית (ב־◊link["https://libgen.is/book/index.php?md5=934E13588328AC1A007C2AB6702D866F"]{◊L{LibGen}}).
}



◊subsubsection[#:label "Shisha-Halevy"]{פרסומים מאת אריאל ששה־הלוי}

◊bullet-list{
	◊link["https://arielshishahalevy.huji.ac.il/publications/2015a"]{פרזנטטיבים בנרטיב}.


	◊link["https://arielshishahalevy.huji.ac.il/publications/2014a"]{◊L{fe-} ו־◊L{mi-} בנרטיב}.


	◊link["https://arielshishahalevy.huji.ac.il/publications/2010"]{קונוורבים קלטיים}.


	◊link["https://arielshishahalevy.huji.ac.il/publications/2005"]{תחביר של מכתבים}.


	◊link["https://arielshishahalevy.huji.ac.il/publications/2003d"]{השוואה טיפולוגית בין תחביר קלטי ומצרי}.


	◊link["https://arielshishahalevy.huji.ac.il/2003c"]{צמידות (יונקטורה, קוהזיה)}.


	◊link["https://arielshishahalevy.huji.ac.il/publications/structural-studies-modern-welsh-syntax-aspects-grammar-kate-roberts"]{ספר על התחביר של קייט רוברטס}‏◊numbered-note{אין גרסה דיגיטלית בשלב זה.}; עוסק ב־◊L{gwneud}, ב־◊L{un} ובדגמי המשפט השמני.


	◊link["https://arielshishahalevy.huji.ac.il/publications/modern-literary-welsh-narrative-grammar-two-features-described"]{האינפיניטיב והאאוריסט (=הווה(־עתיד)) בנרטיב} ותת־נושאים נוספים.


	◊link["https://arielshishahalevy.huji.ac.il/publications/review-g-king-modern-welsh-comprehensive-grammar"]{סקירה שקוטלת את הספר של גרת׳ קינג}.


	במאמרים על וולשית תיכונה יש התייחסות גם לוולשית מודרנית:
	◊bullet-list{
		◊link["https://arielshishahalevy.huji.ac.il/publications/structural-sketches-middle-welsh-syntax-ii-noun-predication-patterns"]{השאה שמנית}.


		◊link["https://arielshishahalevy.huji.ac.il/publications/structural-sketches-middle-welsh-syntax-i-converter-systems"]{מערכת הממירים}.
	}
}



◊subsubsection{נושאים נוספים}

◊bullet-list{
	◊link["https://www.researchgate.net/publication/334415515"]{תמסיר של ◊L{Hewitt} על הדמיון והשוני המבניים בין האקוזטיב הערבי והריכוך הוולשי התחבירי} (ב־◊link["https://www.academia.edu/39804216/"]{◊L{Academia.edu}}; ב־◊link["https://www.scribd.com/document/299542629/"]{◊L{Scribd}}).
}


◊subsection[#:label "cymraeg-misc"]{שונות}

◊bullet-list{
	ההרצאה ◊wiki-en["English and Welsh"]{◊L{English and Welsh}} מאת טולקין, שפורסמה בספר ◊wiki-en["The Monsters and the Critics, and Other Essays"]{◊L{The Monsters and the Critics, and Other Essays}} (בספריה: ◊code{PR 6039 O32 A16}; ב־◊link["https://libgen.is/book/index.php?md5=03DBCD192CC639BF6661759375708F1D"]{◊L{LibGen}}).‏◊;
	◊numbered-note{
		באותו הספר מופיע גם המאמר ◊L{A Secret Vice}.
		הוא אמנם לא קשור ישירות לוולשית (אם כי קשור דרך ◊wiki["cy" "Sindarin"]{סינדרין} וסוגיית האסתטיקה לשונית), אבל מעניין לקריאה.
	}
}




◊section[#:label "cymraeg-canol"]{וולשית תיכונה}
◊margin-figure-frame["pwyll-gwyn.jpg"]{תחילת הטקסט של הענף הראשון של המבינוגי, מהספר הלבן}


◊subsection{ריכוז הפניות}

אם תרצו למצוא מקורות ראשיים ומשניים מעבר למה שמופיע כאן, תוכלו להתחיל את החיפוש בשני הדפים האלה:

◊bullet-list{
		 דף בשם „◊link["https://www.digitalmedievalist.com/opinionated-celtic-faqs/learn-medieval-welsh/"]{◊L{What Do I Need to Learn Medieval Welsh?}}” מאת ◊link["https://www.digitalmedievalist.com/"]{◊L{Lisa L. Spangenberg}}.


		 ספציפית למבינוגי, ◊L{Brenda Thompson} יצרה ◊link["https://mabinogionpathfinder.wordpress.com/"]{◊L{pathfinder}}.
}



◊subsection{ספרות יעץ}

◊bullet-list{
	◊link["https://books.dias.ie/index.php?main_page=product_info&cPath=11_29&products_id=200"]{ספר הדקדוק של ◊L{Evans} לוולשית תיכונה} (בספריה: ◊code{PB 2121 E9}; ב־◊link["http://libgen.is/book/index.php?md5=4EDC1BD11581E6EA810F58F58373BF4A"]{◊L{LibGen}}).
}



◊subsection{טקסטים}

עם טקסטים עתיקים אפשר לעבוד בשתי צורות: האחת היא ישירות מהמקור או צילום פקסימיליה שלו, והאחרת היא בתיווך של מהדורות.
לשתי הדרכים יש יתרונות וחסרונות, ובאופן כללי השיטה המתאימה ביותר לעבודה בלשנית היא לחסוך זמן ולהשתמש במהדורה מודרנית מלבד במקרים מסויימים שדורשים בדיקה מול המקור: שינויים בכתב־היד, מידע שחסר במהדורה ומקומות שבהם יש לכן/ם חשד שנפלה טעות בידי המהדיר/ה.
עיקר העניין שלנו הוא בלשון, אבל לא נכון להתעלם ממאפיינים רלוונטיים של המדיום ואופן מסירת הפלט הלשוני, ולכן עלינו להתייחס ולהיות מודעות/ים למאפיינים פליאוגרפיים, קודיקולוגיים ואחרים שקשורים לטקסט.

◊bullet-list{
	אחד הפרוייקטים החשובים בתחום מדעי הרוח הדיגיטליים של כתיבה בוולשית תיכונה הוא ◊link["http://www.rhyddiaithganoloesol.caerdydd.ac.uk/cy/"]{◊L{Rhyddiaith Gymraeg 1300-1425}}, וכשמו כן הוא: הוא מרכז פרוזה וולשית בטווח השנים האלה במהדורה דיגיטלית.
	המהדורה מנסה לחקות באופן מדוייק את הכתוב בכתב־היד (מה שמכונה „◊L{type facsimile}”) אבל היא כוללת גם מידע ◊wiki-en["Diplomatics#Diplomatic_editions_and_transcription"]{דיפלומטי}, השלמות והערות.


	ל־◊link["https://www.dias.ie/"]{◊L{Dublin Institute for Advanced Studies}} יש מהדורות מעולות למגוון ◊link["https://books.dias.ie/index.php?main_page=index&cPath=11_29"]{טקסטים וולשיים}.
	בהמשך לדרך שבה למדנו וולשית מודרנית, נעבוד עם המהדורה שלהם לענף הראשון של המבינוגי — ◊L{Pwyll Pendeuic Dyuet} — שכוללת הקדמה, טקסט (עם השוואה בין כתבי־היד), הערות וגלוסר שמותאם בדיוק לטקסט.
	הספר נמצא בספריה ב־◊code{PB 2273 M3 P88}.
	סרקתי אותו והעלתי ל־◊link["https://libgen.is/book/index.php?md5=2E4B22401831774955BEB492FDF17618"]{◊L{LibGen}} (◊xref-local["Thomson, R. L. - 1957 - Pwyll Pendeuic Dyuet.pdf"]{כאן} תוכלו להוריד עותק מקומי משופר מעט).


	צילומי פקסימיליה לכתבי־היד שכלולים בהם ענפי המבינוגי זמינים באתרי הספריות שמחזיקות את העותקים הפיזיים:
	◊link["https://syllwr.llyfrgell.cymru/4396205"]{הספר הלבן} (◊L{Peniarth 4}), באתר הספריה הלאומית הוולשית, ו◊link["https://digital.bodleian.ox.ac.uk/inquire/p/b704febf-c9c4-4f23-8312-cb7cd67ef6c6"]{הספר האדום} (◊L{Jesus College, Oxford, MS 111}), באתר הספריה הבודליאנית.


	אי אפשר לדבר על המבינוגי בלי להזכיר את המהדורה החשובה של ◊L{Ifor Williams} (זמינה ◊link["https://archive.org/details/pedeirkeincymabi00will/"]{בארכיון האינטרנט} וכן בספריה בעותק מודפס: ◊code{PB 2273 M3 P4}), על ההקדמה שלה וההערות הרבות והמחכימות שבה.
}



◊subsection{מחקר בלשני}

העיסוק בוולשית תיכונה, בדומה לכל שפה מוקדמת אחרת, מערב מערך אינטר־דיסציפלינרי של תחומי־ידע: ארכיאולוגיה, היסטוריה, בלשנות, ספרות, קודיקולוגיה, פליאוגרפיה ונוספים.
כל התחומים האלה מעניינים, מעשירים אחד את השני, ומהווים חלק מתמונה רחבה יותר שכוללת את ◊wiki-en["Medieval studies"]{לימודי ימי־הביניים} ואת ◊wiki-en["Celtic studies"]{הלימודים הקלטיים} (קלטולוגיה).

בהקשר הבלשני אחד החוקרים המעניינים לדעתי הוא ◊link["https://www.vanhamel.nl/codecs/Poppe_(Erich)"]{אריך פופה}, שעוסק בשפה גם מנקודת מבט של בלשנות הטקסט.
ל◊xref-id["Shisha-Halevy"]{אריאל ששה־הלוי} יש שני מאמרים על וולשית תיכונה.



◊subsection{שונות}

◊bullet-list{
	◊wiki-en["Alan Lee (illustrator)"]{אלאן לי} הוא אמן שאני מאוד אוהב את הציורים שלו, הן איוריו לספרים של טולקין והן לספרים אחרים.◊;.
	◊numbered-note{
		אם אהבתן/ם את הציורים של לי, כדאי להתבונן גם באלו של ◊wiki-he{ארתור רקהאם}.
		הם שונים, אבל יש נקודות דמיון רבות ביניהם.
	}
	הוא צייר גם סצינות מענפי המבינוגי וטקסטים וולשיים ימי־ביניימיים נוספים, באותו אופן רגיש, מדוייק ויפיפה שבו הוא מאייר תמיד.
	אפשר להוריד סריקה של הציורים מ־◊L{LibGen} ◊link["https://libgen.is/book/index.php?md5=0E445B1F1A7E3E03451F865D9D3BF832"]{כאן}, אם כי מדובר בסריקה מספר מודפס, מה שיוצר ◊wiki-he["תבניות מוארה"]{תבניות מוארה} בתמונות.


	המבינוגי תורגם לשפות רבות, ומספר פעמים לאנגלית (סקירה תוכלו למצוא בערך ◊wiki-en["Mabinogion"]{◊L{Mabinogion}} בוויקיפדיה האנגלית).
	יש ◊link["http://folkmasa.org/masa/a0402.htm"]{תרגום עברי} לענף הראשון.
	זה לא תרגום מדוייק, בלשון המעטה, והוא תרגום־של־תרגום, ועדיין ראיתי לנכון לקשר אליו בגלל שכל כך נדיר למצוא תרגום של ספרות וולשית לעברית.
}

◊section[#:label "hen-gymraeg"]{וולשית עתיקה}

◊bullet-list{
	המילון האטימולוגי של ◊L{Falileyev} זמין בספריה הן בגישה מקוונת והן בעותק מודפס (◊code{PB 2188 F34 2000}).
	אפשר להוריד גם מ־◊link["http://libgen.is/book/index.php?md5=D6CFE0B5AF89130F94CC45AD38FA2A48"]{◊L{LibGen}}.
}




◊section[#:label "celteg"]{קלטית}
◊margin-figure["botorrita-1.jpg"]{◊wiki-en["Botorrita plaque"]{לוח בוטוריטה א׳}, הטקסט הארוך ביותר ב◊wiki["cy" "Celtibereg"]{קלטיברית} שיש בידנו.}

◊bullet-list{
	הספר ◊xref-local["Jackson, K. H. - 1953 - Language and history in early Britain.pdf"]{◊L{Language and History in Early Britain}} מאת ◊L{Jackson} (בספריה: ◊code{PB 1015 J34 1994}).


	המילון האטימולוגי לפרוטו־קלטית מאת ◊L{Matasović} (ב־◊link["https://libgen.is/book/index.php?md5=0C0AA52928365C8D6103BD36C2F078D5"]{◊L{LibGen}}).
}


◊section[#:label "ie"]{הודו־אירופית}
◊margin-figure["../stammbaum.jpg"]{}

◊bullet-list{
	◊link["https://benjamins.com/catalog/z.172"]{מבוא לבלשנות הודו־אירופית משווה של ◊L{Beekes}} (ב־◊link["https://libgen.is/book/index.php?md5=1F7382497C1149137A62EB7EB699D876"]{◊L{LibGen}}).


	◊link["https://www.press.uchicago.edu/ucp/books/book/chicago/D/bo23850326.html"]{מילון סמנטי הודו־אירופי משווה של ◊L{Buck}} (ב־◊link["Buck, C. D. - 1949 - A dictionary of selected synonyms in the principal Indo-European languages.pdf"]{◊L{LibGen}}; בספריה: ◊code{P 765 B8}).


	◊wiki-en["The Horse, the Wheel and Language"]{◊L{The Horse, the Wheel and Language}} מאת ◊L{David W. Anthony} (ב־◊link["https://libgen.is/book/index.php?md5=33FE5149DEF38A724C3E18B84B2642CA"]{◊L{LibGen}}; יש גם גישה מקוונת דרך האוניברסיטה):
	מבוא טוב לריאליה והתרבות ההודו־אירופית הקדומה, כפי שהיא משתקפת בממצאים לשוניים ואחרים.


	הנושא של בלשנות היסטורית הודו־אירופית הוא גדול וחשוב, גם בהיסטוריה של הבלשנות וגם בהבנת אחד הגורמים המשפיעים ביותר על האנושות כמו שאנחנו מכירות/ים אותה היום — התרבות של ההודו־אירופים הקדומים והתרבויות שהתפתחו או הושפעו ממנה.
	באופן טבעי, נושא גדול וחשוב כמו זה מושך אליו הרבה תשומת לב מחקרית; חלקה הגדול ראוי ועומד על בסיס מדעי איתן, אבל חלקה לא.
	בהקשר הזה כדאי לקרוא את ◊link["https://www.cambridge.org/il/academic/subjects/languages-linguistics/historical-linguistics/indo-european-controversy-facts-and-fallacies-historical-linguistics"]{◊L{The Indo-European Controversy}} של ◊L{Pereltsvaig} ו־◊L{Lewis}, שעושה סדר בבלגאן וחותך דברים בלהב חד ומחושב (ב־◊link["https://libgen.is/book/index.php?md5=02ECCB095D00DE53C5BB49F73544EF4B"]{◊L{LibGen}}; בספריה: ◊code{P 569 P47 2015} או בגישה מקוונת).
	הספר קרוב לנושא של ◊link["https://youtu.be/4jHsy4xeuoQ"]{ההרצאה הזאת} שנשאו המחברות/ים.
	יש לכן/ם עודף זמן וסבלנות? ב־◊link["http://languagehat.com/"]{◊L{languagehat.com}} יש ◊link["http://languagehat.com/the-indo-european-controversy-an-interview/"]{דיון באורך הגלות} על הספר.


	שני מקורות עם מטרה משותפת, ודרכים אחרות להגיע אליה: להנגיש שפות הודו־אירופיות עתיקות על ידי מתן מבוא ידידותי שמצד אחד לא נכנס מדי לעומק כדי להיות קורס או ספר לימוד שלם, אבל מצד שני נותן בדיוק את קצה החוט שצריך כדי שיהיה אפשר לבסס היכרות עם השפה ולהמשיך משם:
	◊bullet-list{
		האחד נקרא ◊link["https://spw.uni-goettingen.de/projects/aig/"]{◊L{Glottothèque: Ancient Indo-European Grammars online}}, והוא פרוייקט של אוני׳ גטינגן שמרכז הרצאות מבוא לפי מבנה של הקדמה על ההיבטים הלשוניים והחוץ לשוניים, פונולוגיה, מורפולוגיה, תחביר ודיון בטקסטים קונקרטיים.


		השני נקרא ◊link["https://lrc.la.utexas.edu/eieol"]{Early Indo-European Online}, והוא פרוייקט של אוני׳ טקסס שבנוי כריכוז של ספרוני לימוד מקוונים, כשההסברים משולבים עם טקסטים מגולסים.
	}
	השפה הקלטית היחידה שנסקרת במקורות האלה היא אירית עתיקה (קישורים ישירים: ◊link["https://spw.uni-goettingen.de/projects/aig/lng-sga.html"]{◊L{Glottothèque}} ו־◊link["https://lrc.la.utexas.edu/eieol/iriol"]{◊L{EIEOL}}).
}



◊section[#:label "eraill"]{בלשנות כללית ופרסומים על שפות אחרות}
◊margin-figure["../speech-circuit.png"]{}◊;https://www.researchgate.net/profile/Timothy_Crow/publication/7207002/figure/fig1/AS:394523332104198@1471073100483/The-relationship-between-speaker-and-hearer-according-to-de-Saussure-1916.png

◊bullet-list{
	◊link["https://www.cambridge.org/core/books/case/0EDD48C1A8509953AD3F23DB5102A00B"]{הספר של ◊L{Blake} על יחסות} (ב־◊link["https://libgen.is/book/index.php?md5=32E3CFF7F9F80E4764B1B434ABD50BDD"]{◊L{LibGen}}).


	מאמרים מאת גדעון גולדנברג:
	◊bullet-list{
		◊xref-local["Goldenberg, G. - 1998 - Attribution in Semitic languages.djvu"]{על אטריבוציה בשפות שמיות}.


		◊xref-local["Goldenberg, G. - 1998 - Tautological infinitive.djvu"]{על האינפיניטיב הטאוטולוגי}.
	}
	


	◊link["https://www.degruyter.com/view/product/143498"]{האסופה על קונוורבים, בעריכת ◊L{Haspelmath} ו־◊L{König}} (ב־◊link["https://libgen.is/book/index.php?md5=713B12305E83765D4AD23125869110E9"]{◊L{LibGen}}).


	הפרקים הרלוונטיים למבנה מערכות הגופים והכינויים בספר „בעיות בבלשנות כללית” (◊L{Problèmes de linguistique générale}) של ◊wiki-he["אמיל בנבניסט"]{◊L{E. Benveniste}} הם 18, 20, ו־21 (ב־◊L{LibGen}: ◊link["https://libgen.is/book/index.php?md5=68BDA7551BA3D4672DD2EF0B6764C850"]{במקור בצרפתית}, ◊link["https://libgen.is/book/index.php?md5=03660B258F695F274B9B870993A050EC"]{בתרגום לאנגלית}).


	הספר ◊L{The Philosophy of Grammar} מאת ◊L{Jespersen} (ב־◊link["https://libgen.is/book/index.php?md5=5C0C4106B4D2BF5D11082FC61D2C9B9E"]{◊L{LibGen}}; בספריה: ◊code{P 201 J55 1992}).
	הפרקים שהזכרנו מתוכו הם הראשון („הדקדוק החי”) והשביעי („חמש הדרגות”).


	◊link["https://doi.org/10.1515/ling.1987.25.3.511"]{המאמר של ◊L{Sasse} על ההבחנה בין מבעים תטיים וקטיגוריאליים} (ב־◊link["https://sci-hub.se/https://doi.org/10.1515/ling.1987.25.3.511"]{Sci-Hub}).


	הספר ◊L{Omkring Sprogteoriens Grundlæggelse} מאת ◊L{Hjelmslev}.
	יש בספריה עותק של המקור בדנית (◊code{P 105 H5 1966}).
	אם תבחרו להסתפק בקריאת תרגום לאנגלית, יש ◊link["https://libgen.is/book/index.php?md5=C7856E7860FC08001E18D0341C5E127C"]{עותק דיגיטלי} ב־◊L{LibGen} ועותק פיזי בספריה (◊code{P 105 H513 1969}).
}




◊;		בנוסף להיכרות עם הדקדוק דרך הספר של ◊L{Evans} — שלא אמורה להיות מאתגרת עבורכן/ם (אחרי הכל, גם אם וולשית השתנתה מאז ימי הביניים הרבה נשאר דומה) — השלב הבא הוא לקרוא טקסטים עם פיגומים, כמו אצלנו בקורס.
◊;		ל־◊link["https://www.dias.ie/"]{◊L{Dublin Institute for Advanced Studies}} יש מהדורות מעולות לשני הענפים הראשונים של ה◊wiki-he{מבינוגי}, שנערכו על ידי שני אנשים שונים ששם המשפחה שלהם ◊L{Thomson}; יש להן הקדמה, טקסט (עם השוואה בין כתבי־היד), הערות וגלוסר שמותאם בדיוק לטקסט (מוכר מאיפשהו? 🙂).
◊;		אם מעניין אתכן/ם ללמוד ולקרוא וולשית תיכונה, אשמח לעזור ולהנחות ביחידות/ים ובקבוצה.
◊;		עיסוק בוולשית תיכונה מנקודת מבט של בלשנות הטקסט תוכלו למצוא במחקריהם של ◊link["https://www.vanhamel.nl/codecs/Poppe_(Erich)"]{אריך פופה} ואריאל ששה־הלוי (ר׳ בהמשך).
