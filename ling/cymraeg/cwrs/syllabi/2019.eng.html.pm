#lang pollen

◊define-meta[title]{Syllabus for the course ‘Introduction to the Structure of Modern Welsh’}
◊define-meta[publish-date]{2019-10-26}
◊define-meta[language]{eng}
◊define-meta[toc]{true}
◊define-meta[hide-from-sitemap]{true}


This is a copy of the course’s syllabus from the ◊link["http://shnaton.huji.ac.il/index.php/NewSyl/41581/2/2020/"]{University website}, in the structure dictated by the University (with minor changes).



◊section{Administration}

◊style{
dt {
    float: right;
    width: 30%;
    text-align: right;
    padding: .25em;
    clear: left;
}
dd {
    float: right;
    width: 60%;
    padding: .25em 0;
}
dl:after {content:"";display:table;clear:both;}
}

◊(define (row key . value)
	`(tr
		(td ,key)
		(td ,@value)
	 )
 )

◊table{
	◊row["HU Credits"]{4}
	◊row["Degree"]{1◊sup{st} degree (Bachelor)}
	◊row["Responsible department:"]{Linguistics}
	◊row["Semester"]{Yearly}
	◊row["Teaching language"]{Hebrew}
	◊row["Campus"]{Mount Scopus}
	◊row["Course coordinator"]{Júda Ronén}
	◊row["Coordinator email"]{◊link["mailto:foo@digitalwords.net"]{◊code{foo@digitalwords.net}}}
	◊row["Coordinator office hours"]{Contact the teacher in order to set a meeting (preferably just before or after class)}
	◊row["Teaching staff"]{Júda Ronén}
	◊row["Attendance requirements (%)"]{100 (given a proper reason partial attendance will be allowed, of course)}
}

Course evaluation:

◊bullet-list{
	25% — Participation.◊br{}
    Active participation and reading the texts, which is obligatory.


	25% — Assignments.◊br{}
    Three assignments will be given: in the middle of each semester and between semesters.
    Their purpose is aiding in evaluating the students’ progress, both individually and as a whole.


	50% — Project work (final assignment).◊br{}
    a choice will be given between an assignment in a standard format (a fragment for translation and analysis and several questions) and a small-scale research project.
    Students who chose the latter will receive instruction and an opportunity to present the results of research in one of the last classes.
}



◊section{Course description}

◊subsection{General description}

Welsh is a Brythonic language of the Celtic branch of the Indo-European language family.
It is spoken by more than half a million speakers in Wales (Britain), who make about a fifth of the population.
Welsh has a rich literary tradition and a vibrant contemporary literary scene.
Like the other Celtic languages and unlike other languages in the area, it is generally not considered a part of the Standard Average European Sprachbund, showing areally and typologically special phonological, morphological and syntactic features.
The course is a linguistic introduction to this language; the students will learn the basics of the grammar, acquire vocabulary and read texts — all interwoven with discussion of relevant general linguistic questions.
With regards to content and structure, the course is suitable for both beginning and advanced students; no prior knowledge of Welsh or linguistics is required.



◊subsection{Course aims}

Acquiring knowledge of the linguistic structure of Welsh and the research about it, from a particular descriptive point of view as well as a general comparative one.
Students will learn to read and analyse written texts and audio recordings: belles-lettres, poetry, conversations, folktales and media.
Focus will be given not only to the linguistic, scholarly aspects but also to the practical skill of passive command of the language, without which a linguistic research cannot be done effectively.



◊subsection{Learning outcomes}

On successful completion of this module, students should be able to read/listen to diverse texts in Welsh and analyse them.
This course is preparatory to ‘Advanced Investigations in Welsh’ (will be taught next year), after which the students will be able to take an active part in the academic world of Welsh linguistics.



◊subsection{Teaching arrangement and method of instruction}

The first classes will lay the foundations of the grammar.
These will be basically frontal, but with an active role on the part of the students.
Afterwards widening and deepening the knowledge will be done by reading and discussing texts and the linguistic questions that arise from them.
All students have to take an active part in reading.
Aids and instruction will be given to students who wish to keep learning beyond what is taught in class: additional texts, scholarly literature and learning aids.



◊subsection{Course content}

The first classes will be dedicated to introduction and laying the foundations of grammar.
Afterwards, we will proceed to reading texts (and listening to audio recordings).
In order to help acclimatizing to a new language the first (easy and short) texts will have a digital glossary made for this purpose.


◊subsection{Reading}

Required reading:
The texts will be provided during the course of the course, both electronically and in printed form.

Additional reading material:
Reference to reference works such as dictionaries and grammars will be given in the first class.
During the course of the course additional, optional references to scholarly literature will be given.



◊subsection{Additional information}


The language of instruction is Hebrew.
If you are not perfectly fluent in Hebrew but think you will be able to take the course, you are very welcome.
You can participate in classes in English and submit the assignments and the final project in English.
If needed, help will be given during office hours or email.

Don’t hesitate to contact the teacher if you have any questions, during the course or before it, by ◊link["mailto:foo@digitalwords.net"]{email}.
I will be happy to help.
