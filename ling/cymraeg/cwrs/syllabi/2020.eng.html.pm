#lang pollen

◊define-meta[title]{Syllabus for the course ‘Topics in Modern Welsh grammar’}
◊define-meta[publish-date]{2020-9-11}
◊define-meta[last-update]{2020-9-24}
◊define-meta[language]{eng}
◊define-meta[toc]{true}
◊define-meta[hide-from-sitemap]{true}


This is a copy of the course’s syllabus from the ◊link["http://shnaton.huji.ac.il/index.php/NewSyl/41582/2/2021/"]{University website}, in the structure dictated by the University (with minor changes).



◊section{Administration}

◊style{
dt {
    float: right;
    width: 30%;
    text-align: right;
    padding: .25em;
    clear: left;
}
dd {
    float: right;
    width: 60%;
    padding: .25em 0;
}
dl:after {content:"";display:table;clear:both;}
}

◊(define (row key . value)
	`(tr
		(td ,key)
		(td ,@value)
	 )
 )

◊table{
	◊row["HU Credits"]{2}
	◊row["Degree"]{1◊sup{st} degree (Bachelor)}
	◊row["Responsible department:"]{Linguistics}
	◊row["Semester"]{Fall}
	◊row["Teaching language"]{Hebrew}
	◊row["Campus"]{Mount Scopus}
	◊row["Course coordinator"]{Júda Ronén}
	◊row["Coordinator email"]{◊link["mailto:foo@digitalwords.net"]{◊code{foo@digitalwords.net}}}
	◊row["Coordinator office hours"]{Contact the teacher in order to set a meeting}
	◊row["Teaching staff"]{Júda Ronén}
	◊;◊row["Attendance requirements (%)"]{100 (given a proper reason partial attendance will be allowed, of course)}
}

Course evaluation:

◊bullet-list{
	30% — Reading.◊br{}
	Reading the texts and analyzing linguistic phenomena in them.


	70% — Project work (final assignment).◊br{}
    A choice will be given between an assignment in a standard format (a fragment for translation and analysis, as well as several questions) and a research project.
    Students who choose the latter will receive instruction and an opportunity to present the results of research in one of the last classes.
}



◊section{Course description}

◊subsection{General description}

Welsh is a Brythonic language of the Celtic branch of the Indo-European language family.
It is spoken by more than half a million speakers in Wales (Britain), who make about a fifth of the population.
Welsh has a rich literary tradition and a vibrant contemporary literary scene.
Like the other Celtic languages and unlike other languages in the area, it is generally not considered a part of the Standard Average European Sprachbund, showing areally and typologically special phonological, morphological and syntactic features.

In this course, which continues ‘Introduction to the Structure of Modern Welsh’ (#41581), we will deepen our knowledge of the language and broaden the our reach of text-types (including Colloquial Welsh). This will be done in tandem with discussing relevant general linguistic questions.



◊subsection{Course aims}

Deepening our knowledge of the linguistic structure of Welsh and the research about it, from a particular descriptive point of view as well as a general comparative one.
Students will learn to read and analyse written texts and audio recordings: belles-lettres, poetry, conversations, folktales and media.
Focus will be given not only to the linguistic, scholarly aspects but also to the practical skill of passive command of the language, without which a linguistic research cannot be done effectively.



◊subsection{Learning outcomes}

On successful completion of this module, students should be able to:
◊bullet-list{
	Understand diverse texts in Welsh (both written and audio-recorded) and analyse them.

	Take first steps towards becoming scholars of Welsh linguistics and conducting research.
}



◊subsection{Teaching arrangement and method of instruction}

The basic structure of the class revolves around reading original texts, analyzing them and discussing linguistic questions that arise from them.
Aids and instruction will be given to students who wish to keep learning beyond what is taught in class: additional texts, scholarly literature and learning aids.



◊subsection{Course content}

In addition to reading texts in Literary Welsh, this year we begin to deal with Colloquial Welsh.



◊subsection{Reading}

Required reading:
The texts in Welsh will be provided electronically.

Additional reading material:
During the course of the course additional, optional references to scholarly literature will be given.



◊subsection{Additional information}

The language of instruction is Hebrew.
One can participate in classes in English and submit the final assignment/project in English.
If needed, help will be given during office hours or by email.

Don’t hesitate to contact the teacher if you have any questions, during the course or before it, by ◊link["mailto:foo@digitalwords.net"]{email}.
I am happy to provide help as needed.

Course website: ◊link["https://digitalwords.net/ling/cymraeg/cwrs"]{◊code{https://digitalwords.net/ling/cymraeg/cwrs}}
