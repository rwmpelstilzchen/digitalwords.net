#lang pollen

◊define-meta[title]{פתרון מבדק־בית 3}
◊define-meta[publish-date]{2020-08-05}
◊define-meta[last-update]{2020-09-30}
◊define-meta[hide-from-sitemap]{true}

◊(define (external) (paragraph "מעמד הצירוף כלפי חוץ"))
◊(define (internal) (paragraph "מבנה הצירוף כלפי פנים"))


◊margin-figure["gloss.svg"]{}
לפניכן/ם הצעה לפתרון של מבדק־הבית השלישי.
במקומות מסויימים בחרתי להרחיב מעבר לנדרש כדי לתת רקע שמועיל להבנה.
אם מצאת טעות או דבר־מה שאינו ברור, אשמח לדעת כדי לתקן; אמנם עברתי על התשובות, אבל ◊link["https://en.wiktionary.org/wiki/errare_humanum_est"]{◊L{errare humanum est}}.


◊section{גלוסות}

◊div[#:class "textwidthpar latinpar glossedblock"]{
	◊numbered-list{
		◊gl["Ieithydd"]{◊heblexeme{בלשן}}
		◊gl["o’r"]{◊heblexeme{מ־}-◊grammar{def}}
		◊gl["Swistir"]{◊heblexeme{שווייץ}}
		◊gl["oedd"]{◊grammar{impf.3sg}}
		◊gl["Ferdinand de Saussure"]{◊grammar{pn}}
		(26
		◊gl["Tachwedd"]{◊heblexeme{נובמבר}}
		1857 – 22
		◊gl["Chwefror"]{◊heblexeme{פברואר}}
		1913).


		◊gl["Mae’n"]{◊heblexeme{להיות}.◊grammar{prs.3sg}-◊grammar{prep}}
		◊gl["cael"]{◊heblexeme{לקבל}}
		◊gl["ei"]{◊grammar{poss.3sg.m}}
		◊gl["barchu"]{◊grammar{len}\◊heblexeme{לכבד}}
		◊gl["fel"]{◊heblexeme{כ־}}
		◊gl["tad"]{◊heblexeme{אבא}}
		◊gl["ieithyddiaeth"]{◊heblexeme{בלשנות}}
		◊gl["gyfoes"]{◊heblexeme{בת־זמנ(נו)}}.


		◊gl["Roedd"]{◊grammar{prt}-◊heblexeme{להיות}.◊grammar{impf.3sg}}
		◊gl["ei"]{◊grammar{poss.3sg.m}}
		◊gl["syniadau"]{◊heblexeme{רעיון}.◊grammar{pl}}
		◊gl["yn"]{◊grammar{prep}}
		◊gl["sail"]{◊heblexeme{בסיס}}
		◊gl["i"]{◊heblexeme{ל־}}
		◊gl["strwythuriaeth"]{◊heblexeme{סטרוקטורליזם}},
		◊gl["theori"]{◊heblexeme{תיאוריה}}
		◊gl["a"]{◊grammar{rel}}
		◊gl["ddaeth"]{◊grammar{len}\◊heblexeme{לבוא}.◊grammar{pst.3sg}}
		◊gl["yn"]{◊grammar{prep}}
		◊gl["ganolog"]{◊grammar{len}\◊heblexeme{מרכזי}}
		◊gl["i"]{◊heblexeme{ל־}}
		◊gl["ieithyddiaeth"]{◊heblexeme{בלשנות}}
		◊gl["yr"]{◊grammar{def}}
		◊gl["20g"]{◊heblexeme{‏(ה)מאה_(ה)עשרים}},
		◊gl["ac"]{◊heblexeme{ו־}}
		◊gl["a"]{◊grammar{rel}}
		◊gl["oedd"]{◊heblexeme{להיות}.◊grammar{impf.3sg}}
		◊gl["yn"]{◊grammar{prep}}
		◊gl["ddylanwadol"]{◊grammar{len}\◊heblexeme{משפיע}}
		◊gl["iawn"]{◊grammar{intens}}
		◊gl["mewn"]{◊heblexeme{ב־}}
		◊gl["meysydd"]{◊heblexeme{תחום}.◊grammar{pl}}
		◊gl["eraill"]{◊heblexeme{אחר}.◊grammar{pl}}
		◊gl["megis"]{◊heblexeme{כמו}}
		◊gl["anthropoleg"]{◊heblexeme{אנתרופולוגיה}}
		◊gl["a"]{◊heblexeme{ו־}}
		◊gl["beirniadaeth"]{◊heblexeme{ביקורת}}
		◊gl["lenyddol"]{◊grammar{len}\◊heblexeme{ספרותי}}.


		◊gl["Ganwyd"]{◊heblexeme{להוולד}.◊grammar{pst.imprs}}
		◊gl["yng"]{◊heblexeme{ב־}}
		◊gl["Ngenefa"]{◊grammar{nas}\◊grammar{pn}}
		◊gl["a"]{◊heblexeme{ו־}}
		◊gl["roedd"]{◊grammar{prt}-◊heblexeme{להיות}.◊grammar{impf.3sg}}
		◊gl["yn"]{◊grammar{prep}}
		◊gl["ddarlithydd"]{◊grammar{len}\◊heblexeme{מרצה}}
		◊gl["ym"]{◊heblexeme{ב־}}
		◊gl["Mhrifysgol"]{◊grammar{nas}\◊heblexeme{אוניברסיטה}}
		◊gl["Genefa"]{◊heblexeme{ז׳נבה}},
		◊gl["lle"]{◊heblexeme{איפה ש־}}
		◊gl["traddododd"]{◊heblexeme{להעביר}.◊grammar{pst.3sg}}
		◊gl["gyfres"]{◊grammar{len}\◊heblexeme{סדרה}}
		◊gl["o"]{◊heblexeme{של}}
		◊gl["ddarlithoedd"]{◊grammar{len}\◊heblexeme{הרצאה}.◊grammar{pl}}
		◊gl["yn"]{◊grammar{prep}}
		◊gl["amlinellu"]{◊heblexeme{לשרטט}}
		◊gl["ei"]{◊grammar{poss.3sg.m}}
		◊gl["syniadau"]{◊heblexeme{רעיון}.◊grammar{pl}}
		◊gl["am"]{◊heblexeme{אודות}}
		◊gl["iaith"]{◊heblexeme{שפה}}
		◊gl["yn"]{◊heblexeme{ב־}}
		◊gl["ystod"]{◊heblexeme{מהלך}}
		◊gl["y"]{◊grammar{def}}
		◊gl["blynyddoedd"]{◊heblexeme{שנה}.◊grammar{pl}}
		◊gl["1906"]{}
		◊gl["tan"]{◊heblexeme{עד}}
		◊gl["1911"]{}.


		◊gl["Cyhoeddwyd"]{◊heblexeme{לפרסם}.◊grammar{pst.imprs}}
		◊gl["y"]{◊grammar{def}}
		◊gl["darlithoedd"]{◊heblexeme{הרצאה}.◊grammar{pl}}
		◊gl["hyn"]{◊grammar{dem.prox.pl}}
		◊gl["gan"]{◊heblexeme{על־ידי}}
		◊gl["ei"]{◊grammar{poss.3sg.m}}
		◊gl["gyn-fyfyrwyr"]{◊grammar{len}\◊heblexeme{לשעבר}-◊grammar{len}\◊heblexeme{תלמיד}.◊grammar{pl}}
		◊gl["ar ôl"]{◊heblexeme{אחרֵי}}
		◊gl["ei"]{◊grammar{poss.3sg.m}}
		◊gl["farwolaeth"]{◊grammar{len}\◊heblexeme{מוות}}
		◊gl["fel"]{◊heblexeme{כ־}}
		◊gl["Cwrs"]{◊heblexeme{קורס}}
		◊gl["Ieithyddiaeth"]{◊heblexeme{בלשנות}}
		◊gl["Gyffredinol"]{◊grammar{len}\◊heblexeme{כללי}}
		◊gl["(Cours"]{}
		◊gl["de"]{}
		◊gl["linguistique"]{}
		◊gl["générale),"]{}
		◊gl["llyfr"]{◊heblexeme{ספר}}
		◊gl["pwysicaf"]{◊heblexeme{חשוּב}.◊grammar{sup}}
		◊gl["ieithyddiaeth"]{◊heblexeme{בלשנות}}
		◊gl["gyffredinol"]{◊grammar{len}\◊heblexeme{כללי}}
		◊gl["hanner"]{◊heblexeme{חצי}}
		◊gl["cynta’r"]{◊heblexeme{ראשון}-◊grammar{def}}
		◊gl["20g"]{◊heblexeme{‏(ה)מאה_(ה)עשרים}}.


		◊gl["Bu"]{◊heblexeme{להיות}.◊grammar{pst.3sg}}
		◊gl["farw"]{◊grammar{len}\◊heblexeme{למות}}
		◊gl["yn"]{◊heblexeme{ב־}}
		◊gl["Vufflens-le-Château"]{◊grammar{pn}}
		◊gl["ger"]{◊heblexeme{ליד}}
		◊gl["Morges"]{◊grammar{pn}}.
	}
}



◊section{תרגום}

◊numbered-list{
	פרדינן דה־סוסיר היה בלשן משווייץ (26 בנובמבר 1857 – 22 בפברואר 1913).


	הוא נחשב (מיל׳ „מכובד”) כאבי הבלשנות בת־ימינו.


	הרעיונות שלו היוו בסיס לסטרוקטורליזם, תיאוריה שנהייתה מרכזית לבלשנות המאה העשרים ושהייתה משפיעה מאוד בתחומים אחרים כגון אנתרופולוגיה וביקורת הספרות.


	נולד בז׳נבה והיה מרצה באוניברסיטת ז׳נבה, שם העביר סדרה של הרצאות, כשהוא משרטט את רעיונותיו על שפה (בהרצאות), במהלך השנים 1906 עד 1911.


	ההרצאות פורסמו על ידי תלמידיו לשעבר אחרי מותו כ„קורס בבלשנות כללית” (◊L{Cours de linguistique générale}), הספר החשוב ביותר של הבלשנות הכללית של החצי הראשון של המאה העשרים.


	מת ב־◊L{Vufflens-le-Château}, ליד מורז׳.
}



◊section{ניתוח}

◊subsection{Ieithydd o’r Swistir oedd Ferdinand de Saussure}

◊external{}

משפט פותח בערך אנציקלופדי.
למשפטים פותחים יש מאפיינים לשוניים מיוחדים בטקסטמות שונות.
נשים לב לכך שבערך אנציקלופדי המשפט הראשון לא מניח התחלה מאפס: השם של הערך מופיע לפניו, זמין מבחינה טקסטואלית, ואפשר להתייחס אליו כאל תימה (זה בדיוק מה שקורה כאן).



◊internal{}

משפט שמני דלוקוטיבי בעל תימה שמנית.
במשבצת התימה ◊L{Fedinand de Saussure}, שם פרטי שכבר נזכר בשם של הערך;
במשבצת הרימה ◊L{Ieithydd o’r Swistir} „בלשן משווייץ”, צירוף שמני לא מיודע.

מבחינה פנימית ◊L{Ieithydd o’r Swistir} בנוי כך:
◊bullet-list{
	◊L{ieithydd} „בלשן.ית” בנוי מ־◊L{iaith} „שפה” והסיומת הגזירה ◊L{-ydd} (ב־◊L{GPC} הערך הוא ◊L{-ydd◊sup{3}}).
	ניתן לראות כאן הבדל בתנועה של ◊L{iaith} (השו׳ לדוג׳ ◊L{Rhufeinydd} „רומאי; קתולי רומי”, מ־◊L{Rhufain} „רומא”).
	הסיומת גוזרת מילים לאנשים עם מאפיין מסויים, בדרך כלל תחומי עיסוק (השו׳ ◊L{adarydd} „חוקר.ת עופות, אורניתולוג.ית”, מ־◊L{adar} „ציפור, עוף”, או ◊L{cigydd} „קצב.ית”, מ־◊L{cig} „בשר”).


	◊L{o’r Swistir} „משווייץ” ביטוי אדוורביאלי שמאפיין את ◊L{ieithydd} (השו׳ „מיפן” בשם הספר „נוֹריקוֹ-סאן, הילדה מיפן”).
	פנימית הוא בנוי ממילת־היחס ◊L{o}, צורת היידוע אחרי תנועה (◊L{’r}) ושם המקום ◊L{Swistir} „שווייץ”.
	בוולשית יש שמות של מקומות שמקבלים יידוע (כמו ◊L{Y Bala} „באלא” או ◊L{Yr Almaen} „גרמניה”), בדומה לערבית (◊Arabic{العراق} „עיראק” או ◊Arabic{اليابان} „יפן”).
}



◊subsection{theori a ddaeth yn ganolog i ieithyddiaeth yr 20g}

◊external{}

צירוף שמני המופיע כתמורה (אפוזיציה) ל־◊L{strwythuriaeth} „◊wiki-he{סטרוקטורליזם}‏◊;
◊numbered-note{
	השם העברי לפי האקדמיה הוא ◊link["https://hebrew-academy.org.il/2018/03/28/%D7%94%D7%99%D7%A0%D7%93-%D7%95%D7%A1%D6%B6%D7%A4%D6%B6%D7%9F-%D7%9E%D7%99%D7%9C%D7%99%D7%9D-%D7%97%D7%93%D7%A9%D7%95%D7%AA-%D7%95%D7%94%D7%97%D7%9C%D7%98%D7%95%D7%AA-%D7%91%D7%93%D7%A7/"]{תּוֹרַת הַמִּבְנִיּוּת}.
	אני לא נתקלתי בשימוש בשם הזה, אם כי „בלשנות סטרוקטורלית” נקראת באופן רגיל גם „בלשנות מבנית”.
}”.
התמורה ממשיכה עד סוף המשפט, אבל הקטע המסומן לניתוח לא כולל את הסיפא.



◊internal{}

הגרעין של הצירוף הוא ◊L{theori} „תיאוריה, תורה”, והרחבתו פסוקית הזיקה (פסוקית רלטיווית) שנפתחת ב־◊L{a} „ש־”.
סמן הזיקה הזה משמש כאשר היחס התחבירי בין הזוקק ותפקידו בפסוקית הוא של נושא (כמו במקרה דנן) או מושא, והוא גורר ריכוך מלא ביסוד שאחריו (כאן: ◊L{ddaeth} „בא”).
ננתח את הפסוקית:
◊bullet-list{
	הפועל הנוטה ◊L{daeth} הוא צורת העבר גוף שלישי יחיד של ◊L{dod} „לבוא”, שנטייתו חריגה.


	במקרה דנן הערכיות היא ◊L{dod yn ◊sup{◊grammar{l. len}}◊grammar{adj/N}}, בצירוף שמשמעו „להפוך ל־◊L{◊grammar{adj/N}}, להעשות ◊L{◊grammar{adj/N}}” (מובן 2 ב◊link["http://geiriadur.ac.uk/gpc/gpc.html?dod"]{ערך ב־◊L{GPC}}).
	מבנית, ל־◊L{yn + ◊sup{◊grammar{l. len}}◊grammar{adj/N}} הזה נקודות דמיון עם ◊L{yn + ◊sup{◊grammar{l. len}}◊grammar{adj/N}} שמשמש במשבצת הנשואית של המשפט עם הרימה האדוורביאלית, אם כי קבוצת ההתלפות שלהם לא זהה.

	ביחד, המשמעות כאן היא „שנהייתה מרכזית”.


	את ◊L{canolog} „מרכזי.ת” מרחיב הביטוי האדוורביאלי ◊L{i ieithyddiaeth yr 20g}, שגרעינו מילת־היחס ◊L{i} „ל־”.
	בפרפרזה: „מרכזית לְמה? —מרכזית לבלשנות המאה העשרים”.


	פנימית, ◊L{ieithyddiaeth yr 20g} בנוי במבנה סמיכות:
	◊bullet-list{
		הנסמך הוא ◊L{ieithyddiaeth} „בלשנות”.
		המילה הזאת נגזרת מ־◊L{ieithydd} „בלשן.ית”, שראינו קודם, והסיומת המופשטת (אבסטרקטית) ◊L{-(i)aeth} „־וּת”, בדומה לעברית ולאנגלית: התחום שבו עוסקות/ים בלשניות/ים.


		◊L{20g} היא צורת כתיבה מקוצרת של ◊L{ugeinfed ganrif}.
		בוולשית משמשת גם שיטת ספירה ויגסימלית (בכפולות של עשרים), באופן משולב עם בסיס עשרוני.
		כאן מופיעה ◊L{ugeinfed}, הצורה הסודרת (אורדינלית) של ◊L{ugain} „עשרים”.
		בצירוף של סודרים לפני שמות עצם ממין נקבה, שם העצם מסומן בריכוך, וכך קורה כאן עם ◊L{canrif} „מאה (שנים)”.


		מכיוון ש־◊L{ieithyddiaeth} מסתיימת בעיצור ו־◊L{ugeinfed} בתנועה, צורת היידוע היא ◊L{yr}.
	}
}

◊subsection{roedd yn ddarlithydd ym Mhrifysgol Genefa}

◊external{}

משפט עם רימה אדוורביאלית, שמאוחה אל משפט קצר קודם, ◊L{Ganwyd yng Ngenefa} „נולד בז׳נבה”.
לאחר הקטע המסומן בקו הטקסט ממשיך ומרחיב על אוניברסיטת ז׳נבה („שם הוא העביר סדרה של הרצאות וגו׳”).



◊internal{}

התימה היא גוף שלישי יחיד, שמגולמת בנטייה של ◊L{oedd}, והרימה היא ◊L{yn ddarlithydd ym Mhrifysgol Genefa}.

לפני ◊L{oedd} (◊L{◊grammar{impf.3sg}} של ◊L{bod}) מופיעה המילית ◊L{yr} בצורה קצרה שנצמדת אורתוגרפית ל־◊L{oedd}: ביחד ◊L{roedd}.
תפקידה של המילית הזאת טעון תיאור, אבל תכונה ברורה שלה היא שהיא משמשת רק בחיוב ולא בשאלות.

הרימה בנויה מהגרעין ◊L{yn} (שמסמן את קבוצת ההתחלפות האדוורביאלית של כל הצירוף ◊L{yn ddarlithydd ym Mhrifysgol Genefa}) והרחבה שלו, ◊L{ddarlithydd ym Mhrifysgol Genefa}.
ה־◊L{yn} הזה יכול להצטרף אל שמות תואר ושמות עצם, וגורר ריכוך חלקי ברכיב הראשון במשלים (במקרה דנן, ◊L{ddarlithydd}).

◊L{darlithydd} „מרצה” בנוי באופן דומה ל־◊L{ieithydd} שראינו קודם.

◊L{ym} היא הצורה של מילת־היחס הלוקטיבית ◊L{yn} „ב־” לפני עיצורים דו־שפתיים.
◊L{mhrifysgol} היא הצורה של ◊L{prifysgol} „אוניברסיטה” עם מוטציה אפית, שנגררת על ידי מילת־היחס ◊L{yn}.
הקשר בין ◊L{prifysgol} ו־◊L{Genefa} הוא של סמיכות (בדומה ל„אוניברסיטת ז׳נבה” בעברית).



◊subsection{yn amlinellu ei syniadau}

◊external{}

קונוורב עם מושא.
משמש באופן לוואי (אדיונקטיבי) שמאפיין את ◊L{traddododd} „העביר (סדרה של הרצאות)”.
בפרפרזה: „איך הוא העביר אותן? —תוך שהוא משרטט את רעיונותיו על שפה”.



◊internal{}

קונוורבים בוולשית בנויים בבסיסם ממילת יחס בתור גרעין (אצלנו ◊L{yn}) ואינפיניטיב בתור הרחבה (אצלנו ◊L{amlinellu}).
המושא של הקונוורב מסומן במבנה שייכות של סמיכות: ◊L{amlinellu | ei syniadau}.
כזכור, ◊L{bod} מיוחד בכך שבו שיוך מסמן נושא ולא מושא.

◊L{amlinellu} בנוי מהתחילית ◊L{am-} „סביב”, ◊L{llinell} „קו” וסיומת האינפיניטיב ◊L{-u}: „לצייר קו סביב”.

◊L{ei syniadau} בנוי מכינוי שייכות ושם עצם ברבים: „הרעיונות שלו”.



◊subsection{llyfr pwysicaf ieithyddiaeth gyffredinol}

◊external{}

גם כאן, בדומה ל־◊L{theori a ddaeth yn ganolog} וגו׳, מדובר בתמורה, הפעם לספר „קורס בבלשנות כללית”.
המסומן בקו מהווה חלק מהקטע הרחב יותר ◊L{llyfr pwysicaf ieithyddiaeth gyffredinol hanner cynta’r 20g}, שמהווה שרשרת ארוכה של סמיכות עם רכיב אחרון מיודע, כך שכל הצירוף מיודע: ◊L{llyfr pwysicaf {ieithyddiaeth gyffredinol {hanner cynta {’r 20g}}}} „הספר החשוב ביותר של בלשנות כללית של החצי הראשון של המאה העשרים”.



◊internal{}

כאמור, מדובר כאן בשרשרת של סמיכות, וכיוון שהרכיב האחרון בה (◊L{’r 20g}) מיודע, מעמד כולה מיודע.

נחלק לחלקים:

◊bullet-list{
	◊L{llyfr pwysicaf} „הספר החשוב ביותר”.
	תיאור של ◊L{llyfr} בעזרת ◊L{pwysicaf}, צורת דרגת ההפלגה (סופרלטיב) של ◊L{pwysig} „חשוּב.ה”.
	חלק מהמסמן של סיומת הסופרלטיבית הוא איבוד קוליות בפוצצים סופיים, ולכן מופיעה הצורה ◊L{pwysicaf} ולא ◊L{**pwysigaf}.


	◊L{ieithyddiaeth gyffredinol} „בלשנות כללית”.
	ככלל מינה הדקדוקי של הסיומת ◊L{-(i)aeth} הוא נקבה (אם כי יש מקרים של התאם בזכר למילים שגזורות עם הסיומת הזאת).
	כיוון שבוולשית (בדומה לשפות הודו־אירופיות אחרות◊;
	◊numbered-note{
		מה שהוביל ל־◊L{gender bending} נחמד ב־◊link["https://en.wiktionary.org/wiki/wifmann#Old_English"]{◊L{wifmann}} „אשה” באנגלית עתיקה (מין דקדוקי זכר) וב־◊link["https://en.wiktionary.org/wiki/Fr%C3%A4ulein#German"]{◊L{Fräulein}} ו־◊link["https://en.wiktionary.org/wiki/M%C3%A4dchen#German"]{◊L{Mädchen}} „עלמה” בגרמנית (מין דקדוקי נאוטרום).
	}) הצורן הסופי הוא שקובע את המין הדקדוקי של שם עצם מורכב, גם כאן המין של ◊L{ieithyddiaeth} „בלשנות” הוא נקבה, ולכן התואר המאייך אותה מסומן בריכוך: ◊L{gyffredinol}.
}



◊section{בונוס}

◊subsection{דלוקוטיבי עם תימה כינויית}

◊glex["Caradog Prichard" "Un Nos Ola Leuad" "pennod 11" "„אלוהים אדירים (במקור: „ישו”)! הם זמרים טובים,” אמר היוו."]{
	◊gl["Iesu"]{◊grammar{interj}},
	◊gl["canwrs"]{◊heblexeme{זַמָּר}.◊grammar{pl}}
	◊gl["da"]{◊heblexeme{טוב}}
	◊gl["ydyn"]{}
	◊gl["nhw"]{◊grammar{3.pl}},
	◊gl["medda"]{◊grammar{vd}}
	◊gl["Huw"]{◊grammar{pn}}.
}

המשפט השמני הוא החלק ◊L{canwrs da ydyn nhw} „הם זמרים◊;
◊numbered-note{
	בדוגמה זו ניתן לראות צורת ריבוי לא סטנדרטית של ◊L{canwr} „זמר”: לא ◊L{canwyr} אלא הצורה הכמו־אנגלית ◊L{canwrs}.
}
טובים”: ◊L{canwrs da} הרימה (הפרדיקט: החלק המחדש, שאותו מחילים על התימה) והגוף השלישי התימה (בסיפור בני־השיח מקשיבים לשרים, כך שהם נתונים בסיטואציה הפרגמטית).



◊subsection{דלוקוטיבי עם תימה שמנית}
◊glex["KR" "Y Lôn Wen" "Perthnasau Eraill (pennod 10)" "קיית׳רו הוא כפר ששוכן כשני מייל מקיירנרבון, […]"]{
	◊gl["Pentref"]{◊heblexeme{כְּפָר}}
	◊gl["rhyw"]{◊heblexeme{איזה}}
	◊gl["ddwy"]{◊grammar{len}\◊heblexeme{שני}}
	◊gl["filltir"]{◊grammar{len}\◊heblexeme{מייל}}
	◊gl["o"]{◊heblexeme{מ־}}
	◊gl["Gaernarfon"]{◊grammar{len}\◊grammar{pn}}
	◊gl["yw"]{}
	◊gl["Caeathro"]{◊grammar{pn}},
	[…]
}

המחברת הזכירה את שם היישוב ◊L{Caeathro} במשפט שלפני כן, והוא מהווה את התימה במשפט שנותן מידע עליו וממקם אותו ביחס לעיר הגדולה יותר ◊L{Caernarfon}.



◊subsection{קופולרי}

◊glex["KR" "Y Lôn Wen" "Darluniau (pennod 1)" "השיעור הבא הוא (שיעור) תפירה, […]"]{
	◊gl["Y"]{◊grammar{def}}
	◊gl["wers"]{◊grammar{len}\◊heblexeme{שיעור}}
	◊gl["nesaf"]{◊heblexeme{הבא}}
	◊gl["yw"]{}
	◊gl["gwnïo"]{◊heblexeme{תפירה}}
	[…]
}

בנרטיב זמן הווה, אחרי שהמחברת מספרת שיעור גיאוגרפיה ומה שהתרחש בו, היא עוברת לספר על מה שקרה בשיעור שלאחריו, תפירה.
◊L{y wers nesaf} „השיעור הבא„ תימה, ו־◊L{gwnïo} „תפירה” רימה.



◊subsection{אינטרלוקוטיבי}

◊glex["Dafydd Parri" "Bwrw Hiraeth" "Y Ci Coch (pennod 7)" "אתה זָר?"]{
	◊gl["Dyn"]{◊heblexeme{איש}}
	◊gl["diarth"]{◊heblexeme{זָר}}
	◊gl["ydych"]{}
	◊gl["chi"]{◊grammar{2pl}}?
}

הדובר פוגש באדם ומדבר איתו.
הנמען תימטי, ו־◊L{dyn diarth} „איש זר” רימטי.



◊subsection{דו־איברי א׳ (דומה לחג״ם העברי)}

◊glex["Islwyn Ffowc Elis" "Cysgod y Cryman" "pennod 21" "נכון שאמא שלו לא היתה עדיין מחוץ לכלל סכנה, […]"]{
	◊gl["Gwir"]{◊heblexeme{אמת}}
	◊gl["nad"]{◊grammar{nmlz.neg}}
	◊gl["oedd"]{◊heblexeme{להיות}.◊grammar{impf.3sg}}
	◊gl["ei"]{◊grammar{poss.3sg.m}}
	◊gl["fam"]{◊grammar{len}\◊heblexeme{אמא}}
	◊gl["allan"]{◊heblexeme{חוץ}}
	◊gl["o"]{◊heblexeme{מ־}}
	◊gl["berygl"]{◊grammar{len}\◊heblexeme{סכנה}}
	◊gl["eto"]{◊heblexeme{עדיין}},
	[…]
}

לדגם שני חלקים: את הראשון, הרימטי, ממלא ◊L{gwir} „אמת”; את השני, התימטי, ממלאת הפסוקית שנפתחת ב־◊L{nad} „שלא” (רכיב שכולל גם את הנומינליזציה וגם את השלילה ביחד).
לפי שישה הלוי, את המשבצת התמטית בדגם הדו־איברי הזה כאשר ◊L{gwir} במשבצת הרימטית יכולות למלא פסוקיות שמתחילות ב־◊L{mai} (בחיוב) וב־◊L{nad} (בשלילה), ולא מתועדים אינפיניטיבים.



◊subsection{דו־איברי ב׳ (קווליפיקציה)}

◊glex["Fflur Dafydd" "Y Llyfrgell" "pennod 18" "מוטב מאוחר מאשר מאוחר יותר."]{
	◊gl["Gwell"]{◊heblexeme{טוב}.◊grammar{comp}}
	◊gl["hwyr"]{◊heblexeme{מאוחר}}
	◊gl["na"]{◊heblexeme{מאשר}}
	◊gl["hwyrach"]{◊heblexeme{מאוחר}.◊grammar{comp}}.
}

ביטוי גנומי.
נשים לב ש־◊L{gwell} יכול להופיע גם בדגם הראשון (עם אינפיניטיב או ◊L{hynny} במשבצת השניה, לפי שישה הלוי).
קבוצת ההתחלפות שלו וההצטרפות שלו שם שונה, ולכן ערכו המבני שונה.



◊subsection{דו־איברי ג׳ (דומה ל־◊L{naʿt sababī} הערבי)}

◊glex["John Griffith Williams" "Pigau’r Sêr" "Yr Eira (pennod 1)" "אדוורד ג׳ונס הוא קצר־מילים."]{
	◊gl["Cwta"]{◊heblexeme{קָצָר}}
	◊gl["iawn"]{◊grammar{intens}}
	◊gl["ei"]{◊grammar{poss.3sg.m}}
	◊gl["eiriau"]{◊grammar{len}\◊heblexeme{מילה}.◊grammar{pl}}
	◊gl["ydi"]{}
	◊gl["Edward Jones"]{◊grammar{pn}}.
}

הסבר שמופיע מיד לאחר תשובה קצרה, חד־הברתית („◊L{Do.}”) מצד אדוורד ג׳ונס.
החלק ◊L{cwta iawn ei eiriau} „מקצר מאוד במילים, קצר־מילים מאוד” הוא דוגמה למבנה הנדון, שבנוי מתואר, כינוי שייכות ושם עצם בלתי־נתיק: „מי שה־◊L{N} שלה/ו הוא ◊L{◊grammar{adj}}”.
המבנה דומה בצורתו ותפקידו ל„קשה־עורף” העברי, ובמידה רבה יותר ל־◊L{naʿt sababī} הערבי, שכולל גם כינוי שייכות בדומה למבנה הוולשי.



◊subsection{דו־איברי ד׳ (משפט מבוקע ומבנים קרובים)}

◊glex["Kate Roberts" "O Gors y Bryniau" "Henaint (pennod 9)" "זה לי שניתנה המלאכה לומר את הבשורה בל׳יין וון."]{
	◊gl["I"]{◊heblexeme{ל־}}
	◊gl["mi"]{◊grammar{1sg}}
	◊gl["y"]{◊grammar{rel}}
	◊gl["rhoddwyd"]{◊heblexeme{לתת}.◊grammar{pst.imprs}}
	◊gl["y"]{◊grammar{def}}
	◊gl["gorchwyl"]{◊heblexeme{עבודה}}
	◊gl["o"]{◊heblexeme{של}}
	◊gl["ddweyd"]{◊grammar{len}\◊heblexeme{לומר}}
	◊gl["y"]{◊grammar{def}}
	◊gl["newydd"]{◊heblexeme{חדשה}}
	◊gl["yn"]{◊heblexeme{ב־}}
	◊gl["Llain Wen"]{◊grammar{pn}}.
}

המבנה כאן הוא של פוקוס על ביטוי אדוורביאלי, ◊L{i mi} „לי” (ולא לאדם אחר), כשהחלק התימטי הוא הפסוקית שמסומנת ב־◊L{y} „ש־”.



◊subsection{דו־איברי ה׳ (דגם של רגש (אַפקטיבי) וקריאה)}

◊glex["Caradog Prichard" "Un Nos Ola Leuad" "pennod 2" "אה! הכלומניק הזה!"]{
	◊gl["Ia"]{◊grammar{interj}},
	◊gl["yr"]{◊grammar{def}}
	◊gl["hen"]{}
	◊gl["drychfil"]{◊grammar{len}\◊heblexeme{חרק}}
	◊gl["iddo"]{◊heblexeme{ל־}.◊grammar{3sg.m}}
	◊gl["fo"]{◊grammar{3sg.m}}.
}

בהעדר מקבילה עברית ישירה, לקחתי חירות תרגומית בתרגום למעלה; מילולית זה „החרק הזקן לו”, צירוף בלתי אידיומטי בעברית.
הגוף השלישי הוא התימה, ומחדשים עליו באופן אַפקטיבי שהוא „חרק זקן”.
תווית היידוע היא חלק מהמבנה עם כינויי גנאי כאלה.



◊subsection{דו־איבריים ו׳ (שונות)}

◊glex["Dafydd Parri" "Bwrw Hiraeth" "Yr Ymwelydd (pennod 3)" "מוזר מאוד."]{
	◊gl["Rhyfedd"]{◊heblexeme{מוזר}}
	◊gl["iawn"]{◊grammar{intens}}.
}

יש כמה דגמים שמאוגדים תחת הסעיף הזה אצל שישה הלוי.
בחרתי כאן בדוגמה למבנה עם רימה בלבד, ותימה אנפורית מאופסת.
בטקסט האמירה הזאת מתייחסת לאמירה הקודמת של בן־השיח בדיאלוג.
