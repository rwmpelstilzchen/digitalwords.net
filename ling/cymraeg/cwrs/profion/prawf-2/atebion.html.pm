#lang pollen

◊define-meta[title]{פתרון מבדק־בית 2}
◊define-meta[publish-date]{2020-08-05}
◊define-meta[last-update]{2020-09-21}
◊define-meta[hide-from-sitemap]{true}

◊(define (external) (paragraph "מעמד הצירוף כלפי חוץ"))
◊(define (internal) (paragraph "מבנה הצירוף כלפי פנים"))


לפניכן/ם הצעה לפתרון של מבדק־הבית השני.
במקומות מסויימים בחרתי להרחיב מעט מעבר לנדרש כדי לתת רקע שמועיל להבנה.
אם מצאת טעות או דבר־מה שאינו ברור, אשמח לדעת כדי לתקן; אמנם עברתי על התשובות, אבל ◊link["https://en.wiktionary.org/wiki/errare_humanum_est"]{◊L{errare humanum est}}.



◊section{טקסט א׳: אליס בארץ הפלאות}

◊margin-figure["cheshire.webp"]{החתול צ׳שר, ◊link["https://commons.wikimedia.org/wiki/File:Arthur_Rackham_Cheshire_Cat.jpeg?uselang=cy"]{ציור} מאת ◊wiki-en["Arthur Rackham"]{ארתור רקהאם} (◊L{1907})}

◊subsection{גלוסות}

גלוסות מופיעות כחלק מהמקראה.
אין טעם שאחזור עליהן כאן שוב.



◊subsection{תרגום}

◊numbered-list{
	„הדרך ההיא,” אמר החתול תוך הנפת בכפתו הימנית, „שם חי כובען; והדרך ההיא,” תוך הנפת השניה, „שם חי ארנביב”


	לכי לבקר את האחד או האחר: שניהם מטורפים.”


	„אבל אין לי שום רצון ללכת בין אנשים מטורפים,” אמרה אליס.


	„הו, את לא יכולה להמנע מכך,” אמר החתול, „כולנו מטורפים כאן.


	אני מטורף.


	את מטורפת.”


	„איך אתה יודע שאני מטורפת?” שאלה אליס.


	„את חייבת להיות,” אמר החתול, „או שלא היית באה לכאן.”
}


◊subsection{ניתוח}

◊subsubsection{◊L{Y ffordd acw}}

◊external{}

ביטוי שמני שנמצא במעמד טופיקלי (ייחוד); ה־◊L{’na} (צורה מקוצצת של ◊L{yna} „שָׁם”) שבהמשך מוסב עליו.
נשים לב לקטיעה הפרוזודית בציטוט הדיבור הישיר: אחרי הביטוי הטופיקלי נקטע הציטוט ומופיע ציין הדיבור עם הרחבה קונוורביאלית (◊L{gan chwifio}), שלאחריו ממשיך הציטוט לחלק המרכזי של המשפט.



◊internal{}

המבנה הוא אחד מהמבנים של כינויי הרמז צמודי־שם: מערכת הרמז השניה שלמדנו עליה בשיעור, שבנויה ◊L{y NP {yma / yna / acw/draw}} (כלומר: תווית יידוע, לאחריה ביטוי שמני (שם עצם או שם עצם + שם תואר) ולבסוף קבוצת התחלפות מצומצמת של ביטויים דאיקטיים).
הערך המבני של של הביטויים הדאיקטיים שונה מערכם בסביבות אחרות.
מערכת זו מנוגדת למערכת עם ◊L{hon(no)/hwn(nw)/hyn(ny)/rhain/rheiny}, ונראה שהיא כוללת שלוש דרגות: ◊L{yma} קרוב (פרוסימלי, ◊grammar{prox}), ◊L{yna} רחוק (דיסטלי, ◊grammar{dist}) ו־◊L{acw} שככל הנראה מסמן דרגת מרחק גבוהה יותר.
בדרגה הרחוקה יותר קיים יסוד לשוני נוסף, ◊L{draw}, שמתחלף עם ◊L{acw}.

מבחינת תדירות ההופעה, נראה ש־◊L{y NP yma} הנפוץ ביותר, ◊L{y NP yna} פחות (כמחצית מהמופעים של ◊L{y NP yma}, בבדיקה קצרה שערכתי), ו־◊L{y NP acw/draw} נדירים יותר.



◊subsubsection{◊L{gan chwifio}}

◊external{}

ביטוי קונוורביאלי שמאייך את ◊L{ebe’r Gath} „אמר החתול”: הדיבור נעשה תוך פעולה נוספת, במקרה דנן הנפת הכפה הימנית.

המושא של הפועל ◊L{chwifio} מובע בצורת סמיכות◊;
◊numbered-note{
	בפרפרזה עברית: „עם ניפוף כפתו הימנית” או „עם ניפוף (של) כפתו הימנית”.
}
לאחריו, כשהאינפיניטיב הוא הנסמך: ◊L{chwifio|’i phawen dde}.


◊internal{}

קונוורב, הבנוי מצירוף ◊L{◊grammar{prep}+◊grammar{inf}}: מילת־היחס ◊L{gan} „עִם, אצל, מאת, …” והאינפיניטיב ◊L{chwifio} „להניף, לנופף”.
מילת היחס גוררת ריכוך, אך ◊L{chw-} לא מושפע ממוטציות.

קבוצת ההתחלפות של מילת־היחס בקונוורבים שונה במעמד נשואי ובמעמד לוואי, כמו כאן.
בנוסף, לא כל המעמדים הלוואיים זהים, ונראה שיש קורלציה מסויימת◊;
◊numbered-note{
	אך לא מוחלטת; ר׳ להשוואה ◊L{gan chwythu} שראינו ב־◊L{Gofid}, שם הקונוורב מאייך את ◊L{chwythai}.
}
בין קונוורבים עם ◊L{gan} והרחבה של צייני דיבור (◊wiki-en["Verbum dicendi"]{◊L{verba dicendi}}) ופעלים שקשורים לדיבור.

לקריאה נוספת ר׳ ◊link["https://arielshishahalevy.huji.ac.il/publications/2010"]{שישה הלוי (2010)}.


◊subsubsection{◊L{mae’r ddau yn wallgof}}

◊external{}

משפט בדיאלוג.
מבחינת התוכן הוא מסביר את הקודם לו: לא משנה את מי מהשניים תלך אליס לבקר, שניהם מטורפים.



◊internal{}

משפט בדגם ההשאה האדוורביאלי.
נתבונן בחלקיו:
◊bullet-list{
	הנושא הוא ◊L{’r ddau} „השניים” (חוזר אל הכובען והארנביב).
	הוא בנוי מתווית היידוע (כאן בצורתה שאחרי תנועה, ◊L{’r}, באופן שנשען על ◊L{mae} שלפניה) שלאחריה ◊L{dau} „שניים (זכר)” שמשמש כשם עצם המתייחס לשני פרטים (בדומה ל„השניים” בעברית).
	מבחינה מבנית אפשר להבחין בין ◊L{dau₁} שהוא שם מספר מונה (עם שם עצם נמנה אחריו) ובין ◊L{dau₂} שהוא שם עצם בפני עצמו.
	כפי שמציין ◊L{Thorne} בסעיף 49 בספר, גם ◊L{dwy} הנקבי וגם ◊L{dau} הזכרי מתרככים אחרי תווית היידוע (גם כשם מספר מונה, ◊L{y ddau afal} „שני התפוחים”, וגם כשם עצם, ◊L{y ddau} „השניים” / ◊L{y ddwy} „השתיים”).


	הנשוא הוא ◊L{yn wallgof} „מטורף” (מילולית, „במטורף”), צירוף שקבוצת ההתחלפות שלו היא אדוורביאלית, כאן במסגרת המשבצת הנשואית בדגם (ר׳ החלק על דגמי ההשאה במצגת).
	מילת היחס ◊L{yn} היא שמסמנת את המעמד של הצירוף, ומהווה את הגרעין שלו (ר׳ ◊link["https://archive.org/details/barri_1975a/"]{ברי (1975)}).
	מבחינת המוטציות, ה־◊L{yn} שמצטרפת כמסמן של נשוא בדגם ומצטרפת אל שם או תואר גוררת ריכוך חלקי במשלים שלו: ◊L{wallgof}, כשהצורה המילונית היא ◊L{gwallgof}.
	היא שונה הן ממילת־היחס המרחבית ◊L{yn} „ב־, בתוך” שמצטרפת אל שם עצם מיודע וגוררת מוטציה אפית, הן מ־◊L{yn} שמצטרפת אל אינפיניטיב ליצירת קונוורב והן מ־◊L{yn} שמצטרפת לשם תואר בלבד, מחוץ למשבצת הנשואית, וגוררת גם היא ריכוך חלקי.


	החלק שמהווה את העוגן של המבנה הוא ◊L{mae}, שהוא צורת הווה גוף שלישי יחיד של ◊L{bod} „להיות”.
	משמשת צורת היחיד עם נושא מרובה (◊L{y ddau}) מכיוון שבוולשית הן כאן והן בדגם הפעלי רק כינוי גוף שלישי רבים מצטרף עם נטיית הגוף השלישי ברבים, וכל צורה אחרת מצטרפת עם הצורה שנקראת צורת „יחיד”.
}

◊;מבחינת הניגוד עם המשפט השמני, אם אכן ההבחנה „אינהרנטי:אינצידנטלי” אכן

◊subsubsection{◊L{… does arna’ i ddim eisiau mynd …}}

◊external{}

חלק ממשפט בדיאלוג (חתכתי את ההתחלה והסוף כדי שנתמקד במבנה המסויים הזה), שמגיב אל דבריו של החתול וחוזר על לקסמות שאמר (◊L{mynd/ewch} ו־◊L{gwallgof}).



◊internal{}

בוולשית קיימים מספר ביטויים שמתייחסים אל תחושות ומצבים שמובעים במבנה קיום, עם ◊L{ar} „על” לציון מי שיש עליה/ו את התחושה/מצב; דיברנו על זה כשסקרנו את דגם הקיום.
◊L{eisiau} „רצון, צורך” הוא אחד הנפוצים במבנה הזה.
ראינו אותו ב־◊L{Yr oedd ar y Frân Ddu eisiau diod} „העורבת השחורה רצתה לשתות” (מיל׳: „היה על העורבת השחורה רצון לשתות”).
זה המבנה שיש לנו כאן, בצורה שלילית.
ננתח אותו למרכיביו:

◊;{
◊bullet-list{
	◊L{
		◊gl["Mae"]{◊heblexeme{להיות}.◊grammar{prs.3sg}}
		◊gl["arnaf"]{◊grammar{prep.1sg}}
		◊gl["hiraeth"]{◊heblexeme{ערגה}}
	} —
	אני מתגעגע.ת (מיל׳: יש עלי ערגה).


	◊L{
		◊gl["Mae"]{◊heblexeme{להיות}.◊grammar{prs.3sg}}
		◊gl["arnynt"]{◊grammar{prep.3pl}}
		◊gl["ei"]{◊grammar{poss.3sg.f}}
		◊gl["hofn"]{◊heblexeme{פַּחַד}}
	} —
	הן/ם מפחדות/ים ממנה (◊L{ofn} „פַּחַד”).

}
יש  פחד ()
}

◊bullet-list{
	◊L{does} בנוי מצורן השלילה ◊L{d-} (צורה מקוצרת של ◊L{nid} שמצטרפת אל חלק מהצורות של ◊L{bod} שמתחילות בתנועה) ו־◊L{oes} „יש”; ביחד, „אין”.
	את ◊L{oes} אפשר לנתח כצורה תלויה של ◊L{mae} שמופיעה במבני קיום בשלילה („אין…”, כמו כאן), בשאלות („האם יש…”)◊;
	◊numbered-note{
		לדוג׳, ◊L{Oes gynnoch chi siwgwr coch?} „יש לכן/ם סוכר חום?” (◊L{KR, Gobaith, Dychwelyd (pennod 10)}).
	}
	וברישא של תנאי („אם יש…”)◊;
	◊numbered-note{
		לדוג׳, ◊L{Os oes gennych chi ddiddordeb,} „אם יש לכן/ם עניין,” (◊L{Islwyn Ffowc Elis, Wythnos yng Nghymru Fydd, pennod 5}).
	}.


	◊L{arna’ i} „עלי”, ביטוי אדוורביאלי עם ◊L{ar} שמציין מי חווה את התחושה או המצב.
	◊L{arna’} היא צורה דיבורית של נטיית מילת־היחס בגוף ראשון יחיד (◊L{arnaf} בשפה הכתובה◊;
	◊numbered-note{
		קיימת תופעה נרחבת של נפילת ◊L{-f}.
		אפשר לראות את זה גם בנטייה בגוף ראשון יחיד (כמו כאן וכמו בפעלים בהווה), גם בסופרלטיב (◊L{-af} בשפה הכתובה) וגם לפעמים בלקסמות (יש מי שהוגים ◊L{haf} „קיץ” כ־◊L{hâ}).
		האפוסטרוף מסמן, כמובן, את נפילת ה־◊L{f}.
	}),
	ו־◊L{i} כינוי גוף ראשון יחיד (כמו שראינו, תדיר למצוא חזרה כינויית אחרי צורות נטיה סינתטיות, במיוחד בשפה המדוברת יותר ובייצוגה בכתב).


	◊L{ddim eisiau mynd} „שום רצון ללכת”.
	זה החלק שמציין את מה שקיים.
	כפי שראינו, כאשר מופיע ביטוי אדוורביאלי בין הצורה של ◊L{bod} לבין החלק הזה, הוא תמיד מסומן בריכוך (עיינו בפרדיגמה בחלק הרלוונטי במצגת וכן בדוגמאות).
	במקרה דנן מופיע הביטוי האדוורביאלי ◊L{arna’ i}, ואכן הרכיב הראשון כאן, ◊L{dim} „שום, כלום”, מסומן בריכוך.
	היחסים הפנימיים בין הרכיבים בנויים כסמיכות◊;
	◊numbered-note{
		נוכל לעשות לזה פרפרזה „שום־רצון־ללכת” או „שום של רצון של ללכת”, אם זה עוזר להבין.
	}.

	נשים לב לכך שניתוח חליפי הגורס ש־◊L{ddim} מרוכך כי הוא משמש באופן אדוורביאלי (השו׳ הסימון ב„ב־” ב„בכלל”; ר׳ סעיף 81 בדקדוק של ת׳ורן) לא תופס.
	אם נתבונן במשפט הבא נוכל לראות שהרכיב שמסומן כלא קיים, ◊L{teisen} „עוגה”, לא מרוכך; את הריכוך קיבל כבר ◊L{(d)dim}, כחלק מהמבנה („שום גבינה”).

	◊glex-inlist["Kate Roberts" "Y Lôn Wen" "Darluniau (pennod 7)" "סבתא אומרת שאין לה עוגה לתה, רק גבינה."]{
		◊gl["Mae"]{◊heblexeme{להיות}.◊grammar{prs.3sg}}
		◊gl["nain"]{◊heblexeme{סבתא}}
		◊gl["yn"]{◊grammar{prep}}
		◊gl["dweud"]{◊heblexeme{להגיד}}
		◊gl["nad"]{◊grammar{nmlz.neg}}
		◊gl["oes"]{◊heblexeme{להיות}.◊grammar{prs.3sg.dep}}
		◊gl["ganddi"]{◊grammar{prep.3sg.f}}
		◊gl["ddim"]{◊grammar{len\neg}}
		◊gl["teisen"]{◊heblexeme{עוגה}}
		◊gl["i"]{◊grammar{prep}}
		◊gl["de"]{◊heblexeme{תה}},
		◊gl["dim"]{◊grammar{neg}}
		◊gl["ond"]{◊heblexeme{מלבד, אלא}}
		◊gl["caws"]{◊heblexeme{גבינה}}.
	}
}


◊subsubsection{◊L{mod i’n wallgof}}

◊external{}

משלים את ◊L{Sut gwyddoch chi} „איך אתה יודע” בפסוקית תוכן (◊L{◊em{that}-form}).



◊internal{}

בחרתי לסמן את הקטע הזה לניתוח, בגלל שהוא כולל מאפיין „טריקי”.
צורה מלאה יותר לכתוב אותו תהיה ◊L{fy mod i’n wallgof} „שאני משוגעת” (מיל׳ „היותי במשוגעת”), אלא שכאן ה־◊L{fy} נשמט והמוטציה האפית שהוא גורר (◊L{m-}) נשארה, בדומה למה שראינו ב־◊L{nhad} „אבא שלי” מול ◊L{fy nhad} „אבא שלי”.
ניתן לראות אחרי ה־◊L{mod} את כינוי הגוף הראשון היחיד ◊L{i}, במבנה שקראנו לו „מבנה הד”.

מבחינת המבנה הפנימי, זהו דגם ההשאה האדוורביאלי, כאשר ◊L{bod} בצורת האינפיניטיב ולא בצורה פיניטית, מה שמסמן את כל הביטוי כשמני מבחינת קבוצת ההתחלפות ומאפשר לו לשמש, כאמור, כמשלים של ◊L{gwybod} (כלומר, במקבילה העברית: כפסוקית הפותחת ב„ש־” ב„יודע ◊em{שאני מטורפת}”).

החלק ◊L{’n wallgof} בנוי בדיוק כמו המקבילה לו ב־◊L{mae’r ddau yn wallgof}, רק ש־◊L{yn} מופיע בצורה שאחרי תנועה.



◊section{טקסט ב׳: אטלס החיות}

◊margin-figure["lego.svg"]{
	מידות של חלקי לגו (◊link["https://en.wikipedia.org/wiki/File:Lego_dimensions.svg"]{מקור}).
	כפי שניתן לראות אכן שלוש פלטות זהות בגובהן ללבנה רגילה: ◊L{3×3.2mm = 9.6mm}…
}

◊subsection{גלוסות}

◊div[#:class "latinpar glossedblock textwidthpar"]{
	◊numbered-list{
		◊gl["Mae"]{◊heblexeme{להיות}.◊grammar{prs.3sg}}
		◊gl["platiau’n"]{◊heblexeme{פְּלָטָה}.◊grammar{pl}-◊grammar{prep}}
		◊gl["deneuach"]{◊grammar{len}\◊heblexeme{דק}.◊grammar{comp}}
		◊gl["na"]{◊heblexeme{מאשר}}
		◊gl["briciau"]{◊heblexeme{לְבֵנָה}.◊grammar{pl}}.


		◊gl["Mae"]{◊heblexeme{להיות}.◊grammar{prs.3sg}}
		◊gl["3"]{}
		◊gl["phlât"]{◊grammar{asp}\◊heblexeme{פְּלָטָה}}
		◊gl["ar"]{◊heblexeme{על}}
		◊gl["ben"]{◊grammar{len}\◊heblexeme{ראש}}
		◊gl["ei"]{◊grammar{poss.3sg}}
		◊gl["gilydd"]{◊grammar{len}\◊grammar{recp}}
		◊gl["yr"]{◊grammar{def}}
		◊gl["un"]{◊heblexeme{אותו (אחד)}}
		◊gl["trwch"]{◊heblexeme{עובי}}
		◊gl["â"]{◊heblexeme{עִם}}
		◊gl["bricsen"]{◊heblexeme{לְבֵנָה}(◊grammar{f}).◊grammar{sgv}}
		◊gl["gyffredin"]{◊grammar{len}\◊heblexeme{רגיל}}.


		(◊gl["Gwiwer"]{◊heblexeme{סנאי}(◊grammar{f})}
		◊gl["fawr"]{◊grammar{len}\◊heblexeme{גדול}}:)
		◊gl["Mae"]{◊heblexeme{להיות}.◊grammar{prs.3sg}}
		◊gl["cynffon"]{◊heblexeme{זנב}}
		◊gl["y"]{◊grammar{def}}
		◊gl["wiwer"]{◊grammar{len}\◊heblexeme{סנאי}(◊grammar{f})}
		◊gl["fawr"]{◊grammar{len}\◊heblexeme{גדול}}
		◊gl["hon"]{◊grammar{dem.prox.sg.f}}
		◊gl["yn"]{◊grammar{prep}}
		◊gl["hirach"]{◊heblexeme{ארוך}.◊grammar{comp}}
		◊gl["na’i"]{◊heblexeme{מאשר}-◊grammar{poss.3sg.f}}
		◊gl["phen"]{◊grammar{asp}\◊heblexeme{ראש}}
		◊gl["a’i"]{◊heblexeme{ו־}-◊grammar{poss.3sg.f}}
		◊gl["chorff"]{◊grammar{asp}\◊heblexeme{גוף}}
		◊gl["gyda’i"]{◊heblexeme{עִם}-◊grammar{poss.3sg}}
		◊gl["gilydd"]{◊grammar{len}\◊grammar{recp}}.


		◊gl["Mae’r"]{◊heblexeme{להיות}.◊grammar{prs.3sg}-◊grammar{def}}
		◊gl["gynffon"]{◊grammar{len}\◊heblexeme{זנב}(◊grammar{f})}
		◊gl["hir"]{◊heblexeme{ארוך}}
		◊gl["yn"]{◊grammar{prep}}
		◊gl["helpu’r"]{◊heblexeme{לעזור}-◊grammar{def}}
		◊gl["wiwer"]{◊grammar{len}\◊heblexeme{סנאי}(◊grammar{f})}
		◊gl["i"]{◊heblexeme{ל־}}
		◊gl["gadw’i"]{◊heblexeme{לשמור}-◊grammar{poss.3sg.f}}
		◊gl["balans"]{◊heblexeme{שיווי־משקל}},
		◊gl["pan"]{◊heblexeme{כאשר}}
		◊gl["fydd"]{◊heblexeme{להיות}.◊grammar{prs.hab.3sg}}
		◊gl["yn"]{◊grammar{prep}}
		◊gl["codi"]{◊heblexeme{לקום}}
		◊gl["ar"]{◊heblexeme{על}}
		◊gl["ei"]{◊grammar{poss.3sg.f}}
		◊gl["thraed"]{◊heblexeme{כף־רגל}\◊grammar{pl}}
		◊gl["ôl"]{◊heblexeme{אחוֹר}}
		◊gl["i"]{◊heblexeme{ל־}}
		◊gl["fwyta"]{◊grammar{len}\◊heblexeme{לאכול}}
		◊gl["aeron"]{◊heblexeme{פרי־יער}.◊grammar{coll}}
		◊gl["neu"]{◊heblexeme{או}}
		◊gl["ffrwythau"]{◊heblexeme{פרי}.◊grammar{pl}}.


		(◊gl["Clipio"]{◊heblexeme{להצמיד}}
		◊gl["ffliper"]{◊heblexeme{סנפיר}}:)
		◊gl["Gwna"]{◊heblexeme{לעשות}.◊grammar{imp.2sg}}
		◊gl["ffliperi"]{◊heblexeme{סנפיר}.◊grammar{pl}}
		◊gl["i"]{◊heblexeme{ל־}}
		◊gl["greadur"]{◊grammar{len}\◊heblexeme{יצור}}
		◊gl["môr"]{◊heblexeme{ים}},
		◊gl["fel"]{◊heblexeme{כמו}}
		◊gl["y"]{◊grammar{def}}
		◊gl["manatî"]{◊heblexeme{פרת־ים}(◊grammar{m})}
		◊gl["hwn"]{◊grammar{dem.prox.sg.m}},
		◊gl["drwy"]{◊heblexeme{דֶּרֶךְ־}}
		◊gl["osod"]{◊grammar{len}\◊heblexeme{להניח}}
		◊gl["teils"]{◊heblexeme{אריח}.◊grammar{pl}}
		◊gl["llyfn"]{◊heblexeme{שטוח}}
		◊gl["ar"]{◊heblexeme{על}}
		◊gl["blatiau"]{◊grammar{len}\◊heblexeme{פלטה}.◊grammar{pl}}
		◊gl["â"]{◊heblexeme{עִם}}
		◊gl["chlipiau"]{◊grammar{asp}\◊heblexeme{קליפס}.◊grammar{pl}}.
	}
}



◊subsection{תרגום}

◊numbered-list{
	פלטות דקות יותר מִלְּבֵנִים.


	שלוש פלטות אחת על גבי השניה הן באותו העובי כמו לְבֵנָה רגילה.


	(סנאי גדול◊;
	◊numbered-note{
		הוא נקרא בעברית בשמו המלא „◊wiki-he{סנאי רטופה הודי}”.
	}:)
	זנבו של הסנאי הגדול הזה ארוך יותר מראשו וגופו ביחד.


	הזנב הארוך עוזר לסנאי לשמור על שיווי משקלו, כאשר הוא נעמד על רגליו האחוריות כדי לאכול פירות־יער (◊L{berries}) ופירות (◊L{fruits}).


	(להצמיד סנפיר:) צרי/צור סנפירים ליצור ים, כמו פרת־הים הזאת, על ידי הנחת אריחים שטוחים על פלטות עם קליפסים.
}


◊subsection{ניתוח}

◊subsubsection{◊L{deneuach}}

◊external{}

מהווה את המשלים של ◊L{yn} (שמושלם בשם עצם או תואר וגורר ריכוך) במסגרת משפט בדגם ההשאה האדוורביאלי.
צורת דרגת היתרון (קומפרטיב) מורחבת על ידי ◊L{na briciau} „מאשר לְבֵנִים”.



◊internal{}

צורת הבסיס היא ◊L{tenau} „דק”, וכאן מופיעה צורת דרגת היתרון ◊L{teneuach}, עם סיומת ◊L{-ach} ושינוי בתנועה.
הריכוך ב־◊L{t} נגרם על ידי ◊L{yn}, שמסמן את ◊L{teneuach} כאן כנשואי במסגרת דגם ההשאה האדוורביאלי.



◊subsubsection{◊L{ar ben ei gilydd}}

◊external{}

צירוף שקבוצת ההתחלפות של כולו היא אדוורביאלית, כשהגרעין הוא מילת היחס ◊L{ar} „על”.
הצירוף אדיונקטיבי: הוא אינו מוצרך, ומוסיף ומדייק בפרטים◊;
◊numbered-note{
	נוכל לומר ◊L{Mae 3 phlât yr un trwch â bricsen gyffredin} „שלוש פלטות הן באותו עובי כמו לבנה רגילה”, מבלי לפרט איך הן מונחות (אחת על גבי השניה).
}.

מבחינת יחסים פוריים (◊L{phoric}), הרכיב ◊L{ei} חוזר אל הפלטות.



◊internal{}

הצירוף הוא צירוף יחס שגרעינו ◊L{ar} ומשלימו ◊L{pen ei gilydd}.

נתבונן במשלים. היחס בין ◊L{pen} ו־◊L{ei gilydd} הוא של סמיכות (מיל׳ „ראש◊;
◊numbered-note{
	נשים לב שבוולשית הביטוי משתמש ב„ראש” — באופן מושאל, מורחב ו־◊L{grammaticalized} („מדוקדק”…? 🙂) — בעוד שבעברית משמש איבר אחר בצורת הביטוי הבלתי־מאופיינת: גב.
	באנגלית, לשם השוואה, צורת הביטוי הבלתי־מאופיינת לא משתמשת במטאפורה כזאת: ◊L{one upon the other}.
}
רעו”, אם כי ל־◊L{cilydd} אין כבר עצמאות לקסיקלית בוולשית מודרנית; ר׳ סעיף ◊L{255} אצל ◊L{Thorne}).
הן הביטוי כולו והן הרכיב ההדדי (רציפרוקלי) ◊L{ei gilydd} (שמשמש גם בסביבות אחרות) עברו דרגות של גרמטיקליזציה.

עכשיו נתבונן בסומך ◊L{ei gilydd} ורכיביו.
לכאורה, אם הכינוי ◊L{ei} חוזר אל הפלטות (ברבים), היינו אמורות/ים לצפות לצורת הרבים ◊L{eu}, אלא שמדובר כאן בביטוי קפוא.
כמו שדיברנו בשיעור, ל־◊L{gilydd} התנהגות חריגה גם בגוף ראשון (◊L{ein gilydd}) ובגוף שני (◊L{eich gilydd}) מבחינת המוטציה.

כל המאפיינים החריגים האלה מעידים על גרמטיקליזציה גבוהה של המבנה בוולשית מודרנית: לא מדובר באוסף של רכיבים עם קיום לקסיקלי עצמאי שמתחברים ביחד, אלא על מבנה שלא מתפרק כבר באופן נקי לרכיבים.



◊subsubsection{◊L{cynffon y wiwer fawr hon}}

◊external{}

צירוף שמני שמהווה נושא במשפט בעל נשוא אדוורביאלי.



◊internal{}

היחס בין ◊L{cynffon} „זנב” ו־◊L{y wiwer fawr hon} „הסנאי הגדול הזה” הוא של סמיכות: „זְנַב הסנאי הגדול הזה”.
הביטוי כולו מיודע; הרכיב האחרון (במקרה זה, השני ◊L{y wiwer fawr hon}) מסומן ביידוע.

הביטוי ◊L{y wiwer fawr hon} בנוי במבנה הראשון של כינויי הרמז צמודי השם שראינו, בדומה ל־◊L{y gath hon} „החתול הזה” שבמצגת: ◊L{◊grammar{def} NP ◊grammar{dem}}.
במקרה דנן הצירוף השמני הוא שם+תואר.
מינו של שם העצם ◊L{gwiwer} „סנאי” הוא נקבה, ולכן אנחנו רואים ריכוך (חלקי ברמת העקרון; כאן עם ◊L{g-} אין הבדל) בשם העצם וריכוך (מלא) בתואר.
בנוסף, כינוי הרמז נמצא בהתאם עם שם העצם: ◊L{hon} ולא ◊L{hwn} או ◊L{hyn}.



◊subsubsection{◊L{yn helpu’r wiwer i gadw’i balans}}

◊external{}

צירוף שממלא את המשבצת הנשואית במשפט בעל נשוא אדוורביאלי, וגרעינו ◊L{yn}.



◊internal{}

נחלק לרכיבים וננתח:

◊bullet-list{
	◊L{yn helpu}: קונוורב שבנוי ממילת־היחס ◊L{yn} והאינפיניטיב ◊L{helpu} „לעזור”.
	ה־◊L{yn} שמצטרף עם קונוורבים לא גורר מוטציה, ובמקרה דנן (עם ◊L{helpu}) בכל מקרה ◊L{h-} לא מושפע ממוטציות.


	◊L{’r wiwer}: המושא◊;
	◊numbered-note{
		מבחינת הערכיות, בעברית „לעזור” מקבל מושא עקיף („לעזור ל־”), אך ◊L{helpu} מקבל מושא ישיר.
	}
	של המבנה הפעלי הפריפרסטי, מסומן ביחס של שייכות (סמיכות) עם האינפיניטיב.
	היידוע בצורתו אחרי תנועה, ◊L{’r}.
	העיצור הראשון ב־◊L{gwiwer} עובר ריכוך (חלקי) אחרי היידוע, שכן הוא שם עצם ממין נקבה.


	◊L{i gadw’i balans}: מבנה קונוורביאלי ◊L{i+◊grammar{inf}} שמציין מטרה או תכלית: „במה הזנב עוזר? —לשמור על שיווי המשקל”.
	מילת־היחס ◊L{i} גוררת ריכוך במשלים שלה; הצורה המילונית היא ◊L{cadw}.
	כמו קודם, גם כאן המושא של האינפיניטיב מסומן כמבנה שייכות (סמיכות), כאשר הסומך הוא ◊L{’i balans}, צירוף של כינוי השייכות בגוף שלישי יחיד נקבה (מתאים ל־◊L{gwiwer}) ושם העצם ◊L{balans}.
	הצורה ◊L{’i} היא צורה של ◊L{ei} שיכולה לבוא אחרי תנועה (אצלנו ◊L{-w} ב־◊L{cadw}), והמין של כינוי השייכות מסומן על ידי המוטציה שהוא גורר: בנקבה מוטציה חוככת (שמשאירה אצלנו את ◊L{b-} על כנו) ובזכר ריכוך (◊L{i gadw’i falans} „לשמור על שיווי־המשקל שלו (מתייחס לזכר)”).
}



◊subsubsection{◊L{Gwna}}

◊external{}

פועל נוטה בדגם הפעלי, בצורת ציווי גוף שני יחיד.
המושא שלו הוא ◊L{ffliperi}, שלוּ היה מתחיל בעיצור שמשתנה בריכוך היה עובר ריכוך.



◊internal{}

לפועל ◊L{gwneud} „לעשות” נטייה בלתי־רגולרית, שלה קווים דומים עם נטייתם של ◊L{mynd} „ללכת” ו־◊L{dod} „לבוא”.
כפי שניתן לראות מעיון בטבלאות שבמצגת, הייסוד המשותף בכל צורות הנטייה הוא ◊L{gwn-}, שאליו מתווספות סיומות שונות; במקרה דנן, ◊L{-a} לציון ציווי גוף שני יחיד (צורת הרבים היא ◊L{gwnewch}).
הבחירה בצורת ה־◊L{ti} במסגרת ספר כזה לא מפתיעה: הפניה היא ישירה, בגובה העיניים.

נשים לב ש־◊L{gwna} כציווי גוף שני יחיד הומונימי עם ◊L{gwna} של ההווה בגוף שלישי יחיד.
כאן, כמובן, מדובר בציווי — הוראה איך ליצור סנפירים לחיות ים — ולא באדם שמספר על עצמו בגוף ראשון שהוא עושה דבר זה או אחר.




◊section{בונוס}

◊subsection{זיהוי מבנים בטקסט}

תשובות לדוגמה עבור הסעיף הזה ניתנו במסגרת דף השאלות של המבדק.



◊subsection{מוטציה שאינה במגע}

אפרט יותר מחמישה מבנים, כך שתוכלו ללמוד מזה על מבנים שלא חשבתן/ם עליהם.



◊subsubsection{מושא של פועל נוטה}

מושא ישיר של פועל נוטה יוצא בדגם ההשאה הפעלי מסומן בריכוך.
בין הפועל והמושא יכול להפריד מרחק סינטגמטי רב, ועדיין המושא יקבל ריכוך.
סדר הרכיבים הבסיסי במשפט פעלי וולשי הוא ◊L{VSO}.
אם מיד לפני המושא מופיע רכיב מתנה מוטציה שמהווה חיץ (תווית יידוע או כינוי שייכות), לא יסומן הריכוך.

◊glex["Islwyn Ffowc Elis" "Cysgod y Cryman" "pennod 7" "אדוורד וון ראה אור אדום."]{
	◊gl["Gwelodd"]{◊heblexeme{לראות}.◊grammar{pst.3sg}}
	◊gl["Edward Vaughan"]{◊grammar{pn}}
	◊gl["olau"]{◊u{◊grammar{len}}\◊heblexeme{אור}}
	◊gl["coch"]{◊heblexeme{אדום}}.
}

העיצור הראשון במושא (אור אדום, ◊L{golau coch}) מקבל ריכוך.



◊subsubsection{מבנה „◊L{◊em{i} cum infinitivo}”}

כזכור, בוולשית קיים דגם תחבירי ◊L{i + ◊grammar{pro/◊sup{◊grammar{len}}N} + ◊sup{◊u{◊grammar{len}}}◊grammar{inf}}.
דגם זה בעל התחלפות שמנית, והוא מסמן נושא לאינפיניטיב◊;
◊numbered-note{
	מושא, כזכור, מסומן בשיוך של האינפיניטיב, אם בכינוי שייכות עבור כינויים או בסמיכות עבור שם עצם.
}
.
מבחינת המרה (◊L{conversion}), הדגם מעמיד את הנקסוס בין הנושא (◊grammar{pro/◊sup{◊grammar{len}}N}) והפועל (◊sup{◊grammar{len}}◊grammar{inf}) במעמד שמני (◊L{◊em{that}-form}).

◊glex["Dafydd Parri" "Bwrw Hiraeth" "Y Dathlu (pennod 1)" "הבערתי סיגריה נוספת לפני שקולו של הג׳יפ נעלם בחשיכה."]{
	◊gl["Taniais"]{◊heblexeme{להבעיר}.◊grammar{pst.1sg}}
	◊gl["sigarét"]{◊heblexeme{סיגריה}}
	◊gl["arall"]{◊heblexeme{אחר}}
	◊gl["cyn"]{◊heblexeme{לִפְנֵי}}
	◊gl["i"]{◊heblexeme{ל־}}
	{◊gl["sŵn"]{◊heblexeme{קול}}
	◊gl["y"]{◊grammar{def}}
	◊gl["jîp"]{◊heblexeme{ג׳יפ}}}
	◊gl["ddiflannu"]{◊u{◊grammar{len}}\◊heblexeme{להעלם}}
	◊gl["yn"]{◊heblexeme{ב־}}
	◊gl["y"]{◊grammar{def}}
	◊gl["tywyllwch"]{◊heblexeme{חשיכה}}
	…
}

בדוגמה זו ◊L{i sŵn y jîp ddiflannu} ממלא את המשבצת המשלימה את מילת היחס ◊L{cyn} „לִפְנֵי”.



◊subsubsection{סימון שלילה במוטציה מעורבת, ללא ◊L{ni(d)}}

במשפט השני בהארי פוטר ראינו מקרה שבו משמשת המוטציה המעורבת כציין שלילה, ללא ◊L{ni(d)} לפניה.
היסטורית, ניתן לראות כאן תהליך נפוץ בוולשית ובשפות אחרות:
בשלב הראשון שני יסודות לשוניים נמצאים אחד לצד השני;
בשלב השני אחד גורר שינוי בשני;
בשלישי היסוד שגרם לשינוי נופל, והסימון של הפונקציה המקורית שלו נשאר על השינוי ביסוד השני.
בשיעור השוונו את זה עם ה◊wiki-en["Germanic umlaut"]{האומלאוט הגרמאני} בצורות הריבוי (כמו ◊L{feet} מול ◊L{foot} באנגלית).

◊glex["John Griffith Williams" "Pigau’r Sêr" "Yr Eira (pennod 1)" "האשה לא אמרה אף מילה לאבא שלי, …"]{
	◊gl["Ddwedodd"]{◊u{◊grammar{mix}}\◊heblexeme{להגיד}.◊grammar{pst.3sg}}
	◊gl["y"]{◊grammar{def}}
	◊gl["ddynes"]{◊heblexeme{אשה}}
	◊gl["ddim"]{◊heblexeme{שום}}
	◊gl["gair"]{◊heblexeme{מילה}}
	◊gl["wrth"]{◊heblexeme{עם}}
	◊gl["Nhad"]{◊grammar{nas}\◊heblexeme{אבא}},
	…
}

מלבד הסימון ב־◊L{ddim}, המוטציה ב־◊L{Ddwedodd} מסמנת את השלילה (הצורה ה„מלאה יותר” היא ◊L{Ni ddwedodd}).
כיוון שמדובר במוטציה מעורבת, פועל שמתחיל ב־◊L{p-} יעבור מוטציה ל־◊L{ph-}, וכך ◊L{t-} ל־ ◊L{th-},‏ ◊L{c-} ל־◊L{ch-} ושאר העיצורים לפי השינויים של הריכוך.



◊subsubsection{סימון שאלה בריכוך, ללא ◊L{a} השאלה}

בדומה, בדיבור ובכתיבה לא פורמלית ניתן להשמיט את ציין השאלה ◊L{a} (שגורר ריכוך) ולהשאיר את הריכוך.
ר׳ סעיף ◊L{91} אצל ◊L{Thorne}.


◊glex["Caradog Prichard" "Un Nos Ola Leuad" "pennod 1" "תבוא מחר החוצה לשחק, היוו?"]{
	◊gl["Ddoi"]{◊u{◊grammar{len}}\◊heblexeme{לבוא}.◊grammar{prs.2sg}}
	◊gl["di"]{◊grammar{2sg}}
	◊gl["allan"]{◊heblexeme{החוצה}}
	◊gl["i"]{◊heblexeme{ל־}}
	◊gl["chwara"]{◊heblexeme{לשחק}}
	◊gl["fory"]{◊heblexeme{מחר}},
	◊gl["Huw"]{◊grammar{pn}}?
}




◊subsubsection{שיוך במוטציה אפית בלבד, ללא ◊L{fy}}

מקרה נוסף שבו נשמט רכיב שגורם למוטציה ונשארה המוטציה בלבד כסמן הוא שיוך לגוף ראשון ללא כינוי השייכות ◊L{fy} אלא רק במוטציה שהוא מסמן.
דיברנו על זה בכמה הזדמנויות בשיעור.
המופע הנפוץ ביותר של התופעה הוא ◊L{Nhad} „אבא שלי” (השו׳ ◊L{tad} „אבא”).
כזכור, בתחילת השיעור מתאריך ◊L{2020/05/17} ראינו שהמבנה הזה יכול לשמש כחוצץ מפני מוטציה אחרת גם במילים שבהם לא רואים את השפעת המוטציה האפית: ◊L{i fam Begw} „לאמא של בגו” או ◊L{i fam} „לאמא (במובן ◊L{to a mother})” מול ◊L{i Mam/mam} „לאמא שלי”.

אצלנו במבדק נתקלנו ב־◊L{mod i} „היותי”.

◊glex["Kate Roberts" "O Gors y Bryniau" "Henaint (pennod 9)" "„מה שעשיתי היה לבוא אל הדלת כדי להציץ מסביבי לפני שיחשיך,” היא אמרה."]{
	“◊gl["Wedi"]{◊heblexeme{אחרי־}}
	◊gl["dwad"]{◊heblexeme{לבוא}}
	◊gl["i’r"]{◊heblexeme{ל־}-◊grammar{def}}
	◊gl["drws"]{◊heblexeme{דלת}}
	◊gl["i"]{◊heblexeme{ל־}}
	◊gl["spio"]{◊heblexeme{להציץ}}
	◊gl["o"]{◊heblexeme{מ־}}
	◊gl["nghwmpas"]{◊u{◊grammar{nas}}\◊heblexeme{סביב}}
	◊gl["cyn"]{◊heblexeme{לפני}}
	◊gl["iddi"]{◊heblexeme{ל־}.◊grammar{3sg.f}}
	◊gl["dwllu’r"]{◊grammar{len}\להחשיך-◊grammar{def}}
	◊gl["oeddwn"]{◊heblexeme{להיות}.◊grammar{impf.1sg}}
	◊gl["i"]{◊grammar{1sg}},”
	◊gl["ebe"]{◊grammar{vd}}
	◊gl["hi"]{◊grammar{3sg.f}}.
}

נשים לב לתופעה מעניינת בדוגמה זו: אמנם ב־◊L{o nghwmpas} „מסביבי” המוטציה האפית מסמנת את הגוף הראשון, אבל הצורה עם כינוי השייכות הסגמנטלי היא לא ◊L{**o fy nghwmpas} אלא ◊L{o’m cwmpas} (כזכור, אחרי מילת־היחס ◊L{o} באים כינויי ככלל השייכות התלויים◊;
◊numbered-note{
	קיימות דוגמאות של ◊L{o ◊grammar{poss} ◊grammar{N}}, אבל הן מעטות ונדירות, ואני משער שנעדרות לגמרי בחלק מהאידיולקטים.
},
כאשר כינוי הגוף הראשון ◊L{’m} לא גורר מוטציה אפית).
מה אנחנו למדות/ים מכך? שגם אם אמנם המקור של הסימון במוטציה אפית בלבד לסימון של שייכות בגוף ראשון מגיע היסטורית מהשמטה של ◊L{fy} (שגורר מוטציה אפית), הרי שכאן לא מדובר בהשמטה כזאת בצורה פשוטה (ממחיקת ◊L{’m} ב־◊L{o’m cwmpas} היינו מקבלות/ים ◊L{**o cwmpas}) אלא משימוש מורחב במוטציה האפית כסמן שייכות עצמאי מבחינה סינכרונית, שיכול להופיע גם במקומות שבהם מופיע ◊L{’m} (ללא מוטציה אפית).



◊subsubsection[#:label "bonws-2::redup"]{חזרה על יסוד עם מוטציה}

שפות רבות משתמשות באופנים שונים ולמטרות שונות בחזרה על יסודות לשוניים.
יש שפות שבהן רדופליקציה היא מיכניזם מרכזי, ויש כאלה שפחות.
בדומה לעברית, בוולשית חזרה על תואר מסמנת אינטנסיביות („גדול גדול”, הווה אומר „גדול מאוד”).
הפעם השניה בחזרה מהדהדת את התואר כמות שהוא.
כך נקבל ◊L{dyn mawr, mawr} „איש גדול גדול” (◊L{John Griffith Williams, ◊em{Pigau’r Sêr}, Yr Eira (pennod 1)}) ו־◊L{ceg fawr, fawr} „פה גדול גדול” (שם, בהמשך הפסקה).


הטקסט שקראנו לחגיגות סיום הסמסטר הראשון, ◊L{Alys Fach} נכתב לפעוטות.
בשביל הריתמוס והלשון הילדותית יש שם הרבה חזרות ביחס לאורך הטקסט, שתיים מהן עם מוטציה במופע הראשון ולכן עם חזרה על המוטציה:

◊glex["Luned Whelan (cyfieithydd)" "Alys Fach" "t. 1" "היא נופלת למאורת הארנב, למטה, למטה למטה."]{
	◊gl["Mae’n"]{◊heblexeme{להיות}.◊grammar{prs.3sg}-◊grammar{prep}}
	◊gl["syrthio"]{◊heblexeme{ליפול}}
	◊gl["i"]{◊heblexeme{ל־}}
	◊gl["ffau’r"]{◊heblexeme{מאורה}-◊grammar{def}}
	◊gl["gwningen"]{◊grammar{len}\◊heblexeme{ארנב}}…
	◊gl["i"]{◊heblexeme{ל־}}
	◊gl["lawr"]{◊grammar{len}\◊heblexeme{רצפה}},
	◊gl["lawr"]{◊grammar{len}\◊heblexeme{רצפה}},
	◊gl["lawr"]{◊grammar{len}\◊heblexeme{רצפה}}.
}

◊glex["Luned Whelan (cyfieithydd)" "Alys Fach" "t. 3" "אליס שותה את המשקה והופכת לקטנה קטנה."]{
	◊gl["Mae"]{◊heblexeme{להיות}.◊grammar{prs.3sg}}
	◊gl["Alys"]{◊grammar{pn}}
	◊gl["yn"]{◊grammar{prep}}
	◊gl["yfed"]{◊heblexeme{לשתות}}
	◊gl["y"]{◊grammar{def}}
	◊gl["ddiod"]{◊grammar{len}\◊heblexeme{משקה}},
	◊gl["a"]{◊heblexeme{ו־}}
	◊gl["mynd"]{◊heblexeme{ללכת}}
	◊gl["yn"]{◊grammar{prep}}
	◊gl["fach"]{◊grammar{len}\◊heblexeme{קטן}},
	◊gl["fach"]{◊grammar{len}\◊heblexeme{קטן}}.
}



◊subsubsection{תמורה לשם פרטי}

כפי שראינו בשיעור עם השם ◊L{Taliesin Ben Beirdd}, תמורה לשם פרטי מסומנת בריכוך.
ר׳ סעיף ◊L{79} אצל ◊L{Thorne} לדוגמאות נוספות.

◊glex["golygyddion Wicipedia" "Taliesin" "" "השם המסורתי לבארד האגדי, […], הוא טליאסין ראש הבארדים, […]."]{
	◊gl["Yr"]{◊grammar{def}}
	◊gl["enw"]{◊heblexeme{שֵׁם}}
	◊gl["traddodiadol"]{◊heblexeme{מסורתי}}
	◊gl["ar"]{◊heblexeme{על}}
	◊gl["y"]{◊grammar{def}}
	◊gl["bardd"]{◊heblexeme{בארד}}
	◊gl["chwedlonol"]{◊heblexeme{אגדי}},
	[…],
	◊gl["yw"]{}
	◊gl["Taliesin"]{◊grammar{pn}}
	◊gl["Ben"]{◊grammar{◊u{len}}\◊heblexeme{ראש}}
	◊gl["Beirdd"]{◊heblexeme{בארד}\◊grammar{pl}},
	[…].
}



◊subsubsection{תואר צמוד לשם פרטי מסויים}

מבנה נוסף, דומה, הוא ◊L{◊grammar{pn} + ◊sup{◊grammar{len}}◊grammar{adj}}, שמקביל במידה מסויימת למבנה שב„שמעון הצדיק” או שב„אלפרד הגדול”: שם תואר שמצטרף אל שם פרטי של אדם מסויים.
ר׳ סעיף ◊L{50} אצל ◊L{Thorne}.

הדוגמה הבאה לקוחה מהערך בוויקיפדיה הוולשית על ◊wiki["cy" "Hywel Dda"]{◊L{Hywel Dda}} „היוול הטוב”, מלך וולשי:

◊glex["golygyddion Wicipedia" "Hywel Dda" "" "היוול „הטוב” בן קדל׳ היה המלך הראשון של ממלכת דהייברת׳ בדרום־מערב וויילס […]."]{
	◊gl["Brenin"]{◊heblexeme{מֵלֵךְ}}
	◊gl["cyntaf"]{◊heblexeme{ראשון}}
	◊gl["teyrnas"]{◊heblexeme{מלכות}}
	◊gl["Deheubarth"]{◊grammar{pn}}
	◊gl["yn"]{◊heblexeme{ב־}}
	◊gl["ne-orllewin"]{◊grammar{nas}\◊heblexeme{דרום־מערב}}
	◊gl["Cymru"]{◊heblexeme{וויילס}}
	◊gl["oedd"]{}
	◊gl["Hywel"]{◊grammar{pn}}
	◊gl["'Dda'"]{◊grammar{◊u{len}}\◊heblexeme{טוב}}
	◊gl["ap"]{◊heblexeme{בן־}}
	◊gl["Cadell"]{◊grammar{pn}}
	[…].
}



◊subsubsection{סימון לוקטיבי במוטציה אפית}

כשלמדנו על מערכת המוטציות הצגתי דוגמה לשימוש נדיר במוטציה האפית לבדה לסימון מיקום.
במסגרת הודעה על אירוע, המילה ◊L{darlithfa} „אולם הרצאות” סומנה במוטציה אפית במשמעות „באולם הרצאות” (◊L{narlithfa Wallace} „באולם ההרצאות על שם וולס”).
צילום של המודעה מופיע ב◊xref["ling/cymraeg/arwyddion"]{ארכיון השלטים}.



◊subsubsection{סימון של אדוורביאליות על ידי ריכוך}

כפי שראינו בטקסט שקראנו עם ◊L{bob amser} „בכל הזמן”, וכן בדוגמאות מהשיעור של ◊L{2020/03/29}, קיימים מקרים בהם אדוורביאליות מסומנת על ידי ריכוך.
אין סיבה לחזור על הדוגמאות כאן, כשהן מובאות במצגת עם גלוסות והפניה למקור.



◊subsubsection{סימון של הפתעה על ידי ריכוך דרגת ההשוואה}

לא דיברנו על זה בכיתה, אבל קיים שימוש של צורה מרוככת של תואר בדרגת ההשוואה (אקווטיב) לסימון הפתעה או קריאה אַפקטיבית.
◊L{Thorne} כותב על זה בסעיף ◊L{53}, ונותן דוגמאות כגון ◊L{Fyrred yw bywyd!} „הו, כמה קצרים הם החיים!” (צורת השוואה מרוככת של ◊L{byr} „קצר”).



◊subsubsection{מקרים של ריכוך אחרי צירוף אדוורביאלי}

כפי שראינו במשפטי קיום ושייכות, כאשר לפני היסוד שמסומן כקיים (או לא קיים, בשלילה) מופיע ביטוי אדוורביאלי, אותו יסוד מסומן בריכוך.
כך עם ◊L{Roedd gan y Dursleys ◊u{f}ab bychan […]} בטקסט של הארי פוטר ו־◊L{ “Ond does arna’ i ◊u{dd}im eisiau mynd} בטקסט של אליס בארץ הפלאות.

תופעה דומה ראינו ב־◊L{Gofid} במשפטים ◊L{6} ו־◊L{17}, בריכוך ◊L{gael} ו־◊L{golli}.

אמנם לא ראינו לכך דוגמאות בטקסטים, אבל תופעה כזאת קיימת גם במשפטים עם פועל אימפרסונלי, כמו שראינו בדוגמה ◊L{Gwelwyd ar y bryn dŷ} „בית נראה על הגבעה” מול ◊L{Gwelwyd tŷ ar y bryn} בשיעור.
