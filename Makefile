workflow:
	#raco pollen reset
	raco pollen render -f index.html.pm
	raco pollen start
upload:
	#raco pollen reset
	raco pollen render
	raco pollen render -f index.html.pm
	#raco pollen render -f feed.xml.pp
	raco pollen publish . /tmp/ndw/
	rsync -avPC --exclude '.*' /tmp/ndw/ digitalwords@digitalwords.net:digitalwords.net/
	#rsync -aC --exclude '.*' /tmp/ndw/ digitalwords@digitalwords.net:digitalwords.net/
	rsync -avPC /tmp/ndw/.htaccess digitalwords@digitalwords.net:digitalwords.net/
	rm -r /tmp/ndw
