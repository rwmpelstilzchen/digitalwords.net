#lang pollen
◊;◊(require pollen/pagetree pollen/template sugar/coerce)

◊(define-meta hide-nav #t)

◊;◊p[#:class "subtitle"]{כותרת משנה}

◊p[#:id "sitetitle"]{מילים דיגיטליות ◊img[#:src "nib.webp" #:id "sitelogo"]{}}
◊;◊p[#:id "sitetitle"]{מילים דיגיטליות◊margin-figure["nib.webp"]{}}

◊hr{}

שלום, ברוכות הבאים וברוכים הבאות.
באתר הזה תוכלו לקרוא דברים שהיו מעניינים אותי לקרוא לו הייתי אני אתן/ם.
אם יש לנו תחומי עניין משותפים — יופי, אני מקווה שיהיה לכן/ם מעניין; אם מה שאני כותב לא נמצא בתחומי העניין שלכן/ם — תציצו, אולי תמצאו משהו חדש להסתקרן לגביו, ואם לא, אני מאחל לכן/ם המשך גלישה נעימה באתרים אחרים.

אפשר להתעדכן כשמתפרסמים דפים חדשים בעזרת ◊xref["feed.xml"]{הפֿיד} (◊link["https://youtu.be/6HNUqDL-pI8"]{הסבר}).

בשלב זה חלק הארי של התוכן נמצא ב◊xref["old-blog"]{ארכיון הבלוג}.

◊p[#:style "text-align: left"]{
	יום נפלא,◊br{}
	◊link["https://me.digitalwords.net/"]{יודה}
}

◊;◊margin-note{◊img[#:src "images/u13001_JSeshFont.svg" #:style "width: 50%"]}
◊;◊margin-note{◊img[#:src "images/u13001_NotoSansEgyptianHieroglyphs-Regular.svg" #:style "width: 50%"]}

◊style{
	/* An ugly workaround. It is needed because unnecessary <p>-tags are added. */
	.maintoc p {
		margin-top: 0;
		margin-bottom: 0;
	}
}

◊div[#:class "maintoc"]{◊toc{}}
