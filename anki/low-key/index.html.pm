#lang pollen

◊define-meta[title]{Low-key Anki with custom scheduling}
◊define-meta[publish-date]{2022-05-03}
◊define-meta[last-update]{2022-05-06}
◊define-meta[language]{eng}
◊define-meta[toc]{#t}
◊(define-meta tldr ◊@{
	How (and why) to make Anki have a constant ease factor and use only two buttons (◊em{fail} and ◊em{pass}).
	This is implemented with the custom scheduling option of the v3 scheduler.
	The method I propose is reversible, so if you want to return to vanilla Anki, you can do it without losing your ease data.
})


◊section[#:label "meta"]{Meta}

The fundamental idea for what I propose here is based on ◊link["https://refold.la/roadmap/stage-1/a/anki-setup#Low-key-Anki"]{Refold’s}, ◊link["https://web.archive.org/web/20210118163849/https://massimmersionapproach.com/table-of-contents/anki/"]{MIA’s} and ◊link["https://tatsumoto.neocities.org/blog/setting-up-anki.html#reviews"]{Tatsumoto’s} notion of ◊em{low-key Anki}.
Neither vanilla Anki nor low-key Anki are perfect; before adopting low-key Anki you might wish to read ◊link["https://forums.ankiweb.net/t/low-key-anki-with-v3-scheduler-s-custom-scheduling/19707/2"]{Damien Elmes’s objection} to it.
Unless you flatten your ease factors using ◊link["https://ankiweb.net/shared/info/819023663"]{RefoldEase}, the method proposed here is reversible, so you can see for yourself what works the best for you 🙂

My implementation, using the custom scheduling ability, is released under ◊wiki-en["Creative_Commons_license#Zero_/_public_domain"]{CC0}/◊wiki-en["Public domain"]{PD}.
◊link["https://forums.ankiweb.net/t/low-key-anki-with-v3-scheduler-s-custom-scheduling/19707/"]{To the best of my knowledge}, it should not have any negative effect on your Anki database.
Nevertheless, it is provided ‘as is’, and if you choose to use it I am not liable in any way.
It is ◊em{always} a good idea to make backups.◊;
◊numbered-note{
	If you don’t have an up-to-date backup of your entire system, ◊em{now} is a great time to stop reading and to make a backup…
	IMHO, ◊link["https://www.borgbackup.org/"]{Borg} is wonderful and ◊link["https://en.wikipedia.org/wiki/Borg#%22Resistance_is_futile%22"]{resistance is futile}.
}



◊section[#:label "background"]{Background}

◊subsection[#:label "problem"]{The problem}

◊margin-figure["high-key.webp"]{What choosing the right feedback in Anki (let alone SuperMemo and Mnemosyne) feels like… ◊figure-source-eng{https://commons.wikimedia.org/wiki/File:200801191443_Japanische_Tastatur.jpeg}}
The fundamental premise of SRS is that the learner should be prompted on cards just before they forget them.
For the algorithm to estimate when that happens, it needs input from the learner, who should assess how well they answered the card.◊;
◊numbered-note{
	This might be rendered unnecessary by mind-reading computers, but these are not available at the moment…
}
This is done by grading one’s answer: ◊link["https://docs.ankiweb.net/studying.html#review-cards"]{Anki} presents four options◊;
◊numbered-note{
	These are
	‘again’ if failed to answer correctly,
	‘hard’ if answered correctly and it was too hard,
	‘good’ if answered correctly and felt the timing was just right, and
	‘easy’ if answered correctly and it was too easy.
},
while ◊link["https://www.help.supermemo.org/wiki/Learn#Grades"]{SuperMemo} and ◊link["https://mnemosyne-proj.org/help/getting-started.php"]{Mnemosyne} present a whopping six-grade scale (0–5).
The constant need to self-evaluate poses two problems.

One is that we as human beings are not as good as we like to think in assessing how well we know things: there is an extensive body of research in psychology supporting the fact that people are not particularly accurate in their subjective judgement of their own knowledge.
The four- or six-grade scales are not well-defined, and individuals interpret them differently according to personal and cultural differences of what counts as ‘success’ or the meaning of ‘hard’ and ‘easy’.
These differences may result in ◊em{very} different workloads in Anki, which do not necessarily correspond with the individual’s mastery of the material.
In extreme (but not so uncommon) cases, this can give rise to the dreaded ◊link["https://youtu.be/1XaJjbCSXT0?t=665"]{◊em{ease hell}}: pressing ◊em{again} or ◊em{hard} (in Anki’s scale) too many times, not sufficiently balanced by ◊em{easy}, lowers the ease factor◊;
◊numbered-note{
	An ◊em{ease factor} is a number Anki’s algorithm stores for every card.
	When one chooses ◊em{again} or ◊em{hard} it decreases, when one chooses ◊em{good} it stays the same and when one chooses ◊em{easy} it increases.
	Broadly speaking, the new interval of a card after rating a card ◊em{good} (the default option) is calculated by multiplying the current interval by the ease factor; if the latter decreases too much (down to 1.30 even), the interval grows very slowly.
}
of the card to a degree it is being shown too frequently, in a manner that is harmful for one’s progress.
◊link["https://faqs.ankiweb.net/what-spaced-repetition-algorithm.html"]{Here} you can read more about the details of Anki’s algorithm, which is not very complex.

The other problem also has to do with the psychology of ◊wiki-en["Decision-making"]{decision making}: ◊wiki-en{decision fatigue}.
The quality of our decisions deteriorates in long sessions of decision making, which are mentally tiring.
When reviewing SRS cards, one has not only to actively recall information, but also to constantly decide what grade should they give to the retrieval of said information.
When one reviews hundreds of cards, this technical issue of providing a feedback to the system results in fatigue.
I have not used SuperMemo or Mnemosyne, but I can vouch choosing between Anki’s four options (◊em{again}, ◊em{hard}, ◊em{good} and ◊em{easy}) is demanding, especially when I do it hundred of times a day, every day.



◊subsection[#:label "solution"]{The solution}

◊margin-figure["low-key.webp"]{A literal ‘low-key’ keyboard… ◊figure-source-eng{https://hackaday.com/2021/01/23/two-key-keyboard-build-log-starts-small-but-thinks-big/}}
The solution proposed here sacrifices the supposed ability of the algorithm to estimate the best interval for individual cards in favour of having a simpler, more friendly system, which consists of two complementary and interconnected parts:

◊bullet-list{
	The ease factor is constant: it does not decrease or increase.


	Only two options are being used: ◊em{pass} and ◊em{fail}.
}

This decreases significantly the decision fatigue — as a binary choice is much easier than choosing between four or six minute options — and makes it much clearer what the correct feedback should be.
Basically, think of two buttons, one red (🟥, ❌, 🙅) and one green (🟩, ✅, 🙆): it’s as simple as it gets, clear cut.

Having a constant ease factor means the algorithm cannot adjust the ease factor of a particular card according to one’s performance on that card.
Therefore, a global ease factor for a deck should be picked.
The default ease factor in Anki is 2.5◊;
◊numbered-note{
	That means that each time ◊em{good} is chosen the interval grows by 2.5; for example:
	1 day,
	2.5~ days,
	6.3~ days,
	15.6~ days,
	39.1~ days,
	97.7~ days,
	244.1~ days,
	610.4~ days,
	1525.9~ days,
	etc.
	You see how fast it grows: ◊wiki-en["Exponential growth"]{exponentially}.
	This factor is resemblant, but not identical, to the notion of ◊wiki-en["Basic reproduction number"]{basic reproduction number} (R₀) we are all familiar with from COVID-19 epidemiology…
},
and for many it might be a good rule of thumb to use it.
Depending on how difficult what you learn is to you, you might want to set a higher or lower factor.
For example, here are my average ease factors for four languages I learn, as well as ◊wiki-en["Remembering the Kanji and Remembering the Hanzi"]{RTK} Kanji cards, before I began to use ◊em{low-key Anki}:

◊table[#:class "booktab" #:id "cyflwyniad"]{
	◊tr{
		◊th{Deck}
		◊th{Reading◊br{}comprehension}
		◊th{Listening◊br{}comprehension}
		◊th{Production}
		◊th{Global}
	}
	◊tr{
		◊td{Welsh}
		◊td{3.20}
		◊td{3.17}
		◊td{2.97}
		◊td{3.09}
	}
	◊tr{
		◊td{Norwegian}
		◊td{3.05}
		◊td{2.98}
		◊td{2.80}
		◊td{2.95}
	}
	◊tr{
		◊td{Finnish}
		◊td{2.72}
		◊td{2.70}
		◊td{2.31}
		◊td{2.68}
	}
	◊tr{
		◊td{Japanese}
		◊td{2.77}
		◊td{2.73}
		◊td{not enough cards}
		◊td{2.76}
	}
	◊tr{
		◊td{Kanji (◊wiki-en["Remembering the Kanji and Remembering the Hanzi"]{RTK})}
		◊td{–}
		◊td{–}
		◊td{2.41}
		◊td{2.41}
	}
}

If you already use Anki◊;
◊numbered-note{
	If you read this, you probably are…
},
use the statistics tool in order to get an idea about what ease factors fit the best for your needs (which, as you can see, may vary according the the topic or language).
If you don’t use Anki, or if you begin to learn a new topic, 2.5 seems like a relatively low factor for language learning, and 2.75 might be a better choice for that (unless it is a particularly difficult language for you).



◊subsubsection[#:label "other solutions"]{Other existing solutions}

This is ◊em{low-key Anki}.
It is only one possible modification of ◊link["https://faqs.ankiweb.net/what-spaced-repetition-algorithm.html"]{Anki’s SM-2} algorithm to allow binary feedback.
It is the simplest modification, but it sacrifices individual ease factors.
Two other solutions are:◊;
◊numbered-note{
	H/T ◊link["https://www.reddit.com/r/Anki/comments/ui687z/"]{Reddit}.
}

◊bullet-list{
	The add-on ◊link["https://ankiweb.net/shared/info/957961234"]{Straight Reward}, in which a consecutive streak of correct answers award you with an increase of the ease factor.
	If you use only two ◊em{again} and ◊em{good}, with the default values it takes four ◊em{good} feedbacks in a row to recover (and overshoot) from one ◊em{again}.
	This add-on is actively maintained and supports the v3 scheduler.


	Another add-on is ◊link["https://ankiweb.net/shared/info/1672712021"]{Auto Ease Factor}, which presents a ◊link["https://eshapard.github.io/anki/thoughts-on-a-new-algorithm-for-anki.html"]{radical modification of the algorithm}.
	The way it works is less straightforward, and at the moment (2022/5) it is not maintained any longer and does not support the v3 scheduler.
	For these reasons I would suggest to avoid using it.
}



◊section[#:label "implementation"]{Implementation}

If one avoids the ◊em{hard} and ◊em{easy} buttons on vanilla Anki, and uses only ◊em{again} (our ◊em{fail}) and ◊em{good} (our ◊em{pass}), one gets a very similar system to the one describe above, but there is a problem:
hitting ◊em{again} on review cards decreases their ease factor, and since we don’t use ◊em{easy}, the ease factor is bound to get lower and lower.

Fortunately, the v3 scheduler◊;
◊numbered-note{
	Supported on ◊link["https://apps.ankiweb.net/"]{Anki Desktop} (2.1.45+) and ◊link["https://apps.apple.com/us/app/ankimobile-flashcards/id373493387"]{AnkiMobile} (2.0.75+), but not on ◊link["https://apps.apple.com/us/app/ankimobile-flashcards/id373493387"]{AnkiDroid} at the moment (2022/5).
	You can still use the proposed method on your desktop and the vanilla Anki algorithm (not avoiding ◊em{easy}) on your Android device.
}
allows users to easily change the way the algorithm works.
If you haven’t already, turn the v3 scheduler on: ◊em{Tools} > ◊em{Preferences} > ◊em{Scheduling} > Check ◊em{V3 scheduler}.
Then, paste the following code in the ◊link["https://docs.ankiweb.net/deck-options.html"]{custom scheduling} textbox in the ◊link["https://docs.ankiweb.net/deck-options.html"]{deck options} window (any deck; this is affect all decks):

◊highlight['javascript]{
	/*
	 * New card:
	 * 	Fail: enter the learning sequence.
	 * 	Pass: skip the learning sequence and use the ‘easy interval’ setting.
	 */
	if (states.current.normal?.new ||
		states.current.filtered?.rescheduling.originalState.new) {
		states.hard = states.easy;
		states.good = states.easy;
	}

	/*
	 * (Re)learning card:
	 * 	Fail: reset the (re)learning sequence.
	 * 	Pass: progress in the (re)learning sequence.
	 */
	else if (states.current.normal?.learning ||
		states.current.normal?.relearning ||
		states.current.filtered?.rescheduling.originalState.learning ||
		states.current.filtered?.rescheduling.originalState.relearning) {
		states.hard = states.good;
		states.easy = states.good;
	}

	/*
	 * Review card:
	 * 	Fail: enter the relearning sequence with no penalty.
	 * 	Pass: multiply the interval by the constant ease factor.
	*/
	// Code for normal review
	else if (states.current.normal?.review) {
		states.hard = states.good;
		states.easy = states.good;
		states.again.normal.relearning.review.easeFactor =
			states.current.normal.review.easeFactor;
	}
	// Code for filtered decks
	else if (states.current.filtered?.rescheduling.originalState.review) {
		states.hard = states.good;
		states.easy = states.good;
		states.again.filtered.rescheduling.originalState.relearning.review.easeFactor =
			states.current.filtered.rescheduling.originalState.review.easeFactor;
	}
}

Using this custom scheduling, ◊em{hard}, ◊em{good} and ◊em{easy} all behave the same (our ◊em{pass}), making no difference in scheduling.
I suggest using answering only ◊em{good} (avoiding ◊em{hard} and ◊em{easy}, even though their effect is identical), because in general ◊em{pass} behaves in most cases like the vanilla ◊em{good}.



◊subsection[#:label "explanation"]{Explanation}

Let’s see have a look on how the scheduler behaves in each of the following situations:◊;
◊numbered-note{
	Even if you are not familiar with JavaScript, you can try to read the code above.
	It is quite simple and straightforward.
}

◊bullet-list{
	New cards.

	◊bullet-list{
		◊em{Fail}.
		If you don’t know the answer to a new card, it enters the learning sequence.
		This is the normal behaviour of Anki.
		Choose ◊link["https://docs.ankiweb.net/deck-options.html#learning-steps"]{learning steps} to your liking◊;
		◊numbered-note{
			I for one don’t like the default short first step (◊link["https://youtu.be/1XaJjbCSXT0?t=1121"]{here}’s why), so my first step is ◊tt{4h} (four hours), which means it won’t appear in the same session.
		}.


		◊em{Pass}.
		If you know the answer to a new card, it suggests you are already familiar with it, having previous knowledge, so the learning sequence is skipped.◊;
		◊numbered-note{
			In order to avoid other cards of the same note helping too much without having being ingrained in your memory well enough, I suggest using ◊link["https://ankiweb.net/shared/info/930365039"]{this add-on} with a fairly large number (at least ◊t{21} days, so previous information has time to consolidate).
		}
		Thus, it makes sense not to show it too soon: the value of the ◊link["https://docs.ankiweb.net/deck-options.html#easy-interval"]{easy interval} is taken◊;
		◊numbered-note{
			Mine is ◊tt{14} days, but choose whatever fits your needs the best.
		}.
	}


	(Re)learning cards.

	This behaves exactly the same as vanilla Anki◊;
	◊;◊numbered-note{
	◊;	By the way, I stray from the default values regarding relearning (lapses) as well.
	◊;	If I forgot a card I want to take some time relearning it properly before it returns as a normal review card.
	◊;	I set
	◊;	◊link["https://docs.ankiweb.net/deck-options.html#relearning-steps"]{relearning steps} of ◊tt{4h 3d 7d},
	◊;	a ◊link["https://docs.ankiweb.net/deck-options.html#minimum-interval"]{minimum interval} of ◊tt{14} days, and
	◊;	a ◊link["https://docs.ankiweb.net/deck-options.html#new-interval"]{new interval} of ◊tt{20} (which is possible too high).
	◊;}
	,
	except for the fact that ◊em{hard} and ◊em{easy} are now synonyms of ◊em{good}:

	◊bullet-list{
		◊em{Fail} resets the (re)learning sequence.


		◊em{Pass} progresses the card one step in the (re)learning sequence.
	}


	Review cards.

	◊bullet-list{
		◊em{Fail}.
		Enters the relearning sequences as usual, but with no penalty of reducing the ease factor (which is constant now).


		◊em{Pass}.
		Just like ◊em{good}: the new interval is the current interval times the ease factor.◊;
		◊numbered-note{
			If you changed your ◊link["https://docs.ankiweb.net/deck-options.html#interval-modifier"]{interval modifier}, now it is a good time to return it back to ◊tt{1}.
		}
	}
}

That’s it.
Now you are all set to use ◊em{low-key Anki} with the modern v3 scheduler.



◊section[#:label "other issues"]{Other issues}

◊subsection[#:label "mouse-free"]{Mouse-free Anki-ing}

If you want to use the keyboard more easily, I suggest installing ◊link["https://ankiweb.net/shared/info/24411424"]{this add-on} and using ◊kbd{↵} (enter/return) for showing the answer and ◊em{pass}, and ◊kbd{'} for ◊em{fail}◊;
◊numbered-note{
	That is, set ◊tt{"reviewer choice 1": "'"} in the configuration window of the add-on.
}:
this way you have the two side by side.
If you have audio on your cards, you might want to change the default key (◊kbd{R}) to ◊kbd{]}◊;
◊numbered-note{
	Set ◊tt{"reviewer replay audio 1": "]"}.
},
so all three are easily accessible using one hand.

If you have a game controller you can use it to input keys using special software◊;
◊numbered-note{
	Such as ◊link["https://github.com/AntiMicroX/antimicroX"]{AntiMicroX} for Linux, ◊link["https://github.com/fyhuang/enjoy2"]{Enjoy2} for macOS and ◊link["https://github.com/AntiMicro/antimicro"]{AntiMicro} for Windows.
}, allowing you to lean back and review comfortably.
◊margin-figure["mid-key.webp"]{◊link["https://stackoverflow.blog/2021/03/31/the-key-copy-paste/"]{The Key} ◊figure-source-eng{https://drop.com/buy/stack-overflow-the-key-macropad}}
This is also a great use for ◊link["https://stackoverflow.blog/2021/03/31/the-key-copy-paste/"]{◊em{The Key}} keyboard…



◊subsection[#:label "ui"]{User interface}

If you have the next review times shown above the answer buttons◊;
◊numbered-note{
	◊em{Tools} > ◊em{Preferences} > ◊em{Scheduling} > ◊em{Show next review time above answer buttons}.
},
the correct times will be shown in Anki’s built-in answer bar, but not in ◊link["https://github.com/mobedoor/No-Distractions-Full-Screen"]{NDFS}’s answer bar (the cards are nevertheless scheduled correctly).
Anyway, in my humble opinion having the next review times shown is distracting and not helpful in any way; in fact, I use NDFS without an answer bar at all◊;
◊numbered-note{
	◊em{View} > ◊em{ND Full Screen} > ◊em{Answer Button Visibility} > ◊em{Hidden (Reveals on Mouseover)}.
}.

Another add-on you might want to consider is ◊link["https://ankiweb.net/shared/info/1715096333"]{this one}◊;
◊numbered-note{
	H/T ◊link["https://tatsumoto.neocities.org/blog/setting-up-anki.html#reviews"]{Tatsumoto}.
},
which allows you to grade a card from the front side (in case the answer is so obvious you don’t need to check it) and show only two buttons.



◊subsection[#:label "migrating from vanilla"]{Migrating from vanilla Anki}

With the proposed implementation, the ease of old existing cards is retained.
This way, you can test whether you like ◊em{low-key Anki} or not without loosing your old data.

But there is a problem: if you have existing cards with low ease values they will remain this way (an eternal, inescapable ‘ease hell’…), since the ◊em{easy} option has been removed.
This can be solved in two ways.

One approach is to make all of the cards in a deck have the same ease.
The ◊link["https://ankiweb.net/shared/info/819023663"]{RefoldEase} add-on provides this functionality, but take note this quite an extreme sudden change to your deck, which is ◊em{not} reversible in case you want to return to vanilla scheduling.

Another, milder approach, which can be used if you are unsure if you want to stick with ◊em{low-key Anki} or not is the following, which does not flatten the whole deck at once but gradually and only if necessary:

◊bullet-list{
	Each time you pass a review card with an ease value lower than the target ease (say, ◊tt{2.5}), it increases it a bit (say, by ◊tt{0.2}).


	If the value of the said card is too high, the original penalty for ◊em{fail} is retained.
}

To achieve this, change the final part of the code with this:

◊highlight['javascript]{
	/*
	 * Review card:
	 * 	Fail: enter the relearning sequence with no penalty.
	 * 	Pass: multiply the interval by the constant ease factor.
	*/
	// Code for normal review
	else if (states.current.normal?.review) {
		if (states.current.normal.review.easeFactor < 2.5) {
			states.good.normal.review.easeFactor =
				Math.min(states.current.normal.review.easeFactor + 0.2, 2.5); 
		}
		states.hard = states.good;
		states.easy = states.good;
		if (states.current.normal.review.easeFactor <= 2.5) {
			states.again.normal.relearning.review.easeFactor =
				states.current.normal.review.easeFactor;
		}
	}
	// Code for filtered decks
	else if (states.current.filtered?.rescheduling.originalState.review) {
		if (states.current.filtered.rescheduling.originalState.review.easeFactor < 2.5) {
			states.good.normal.review.easeFactor =
				Math.min(states.current.filtered.rescheduling.originalState.review.easeFactor + 0.2, 2.5); 
		}
		states.hard = states.good;
		states.easy = states.good;
		if (states.current.normal.review.easeFactor <= 2.5) {
			states.again.filtered.rescheduling.originalState.relearning.review.easeFactor =
				states.current.filtered.rescheduling.originalState.review.easeFactor;
		}
	}
}

This way the change is not sudden and binding, but gradual.
You also get to benefit from the supposedly more accurate values calculated by vanilla ease factor mechanism, but you are not stuck with values which might prove too low or too high.
Unfortunately, at the moment I don’t know how can one have different target values for different decks.◊;
◊numbered-note{
	I guess the current deck is stored in some variable, but I have not troubled myself with finding out what exactly is its name or how to access it…
	◊link["https://me.digitalwords.net/"]{Contact me} if you can help with this.
}


◊subsection[#:label "migrating from low-key"]{Migrating from a low-key workaround}

Before custom scheduling was introduced, one way to achieve the ◊em{low-key Anki} functionality was a workaround suggested by ◊link["https://refold.la/roadmap/stage-1/a/anki-setup#Low-key-Anki"]{Refold}, ◊link["https://web.archive.org/web/20210118163849/https://massimmersionapproach.com/table-of-contents/anki/"]{MIA} and ◊link["https://tatsumoto.neocities.org/blog/setting-up-anki.html#reviews"]{Tatsumoto}: setting the starting ease to ◊tt{1.31} and the interval modifier to ◊tt{1.92} (1.31 ⋅ 1.92 ≈ 2.5).
Because Anki has a lower ◊tt{1.30} limit for the ease factor◊;
◊numbered-note{
	You cannot set the starting ease to ◊tt{1.30} due of a bug.
},
this provided a way to have a constant ease of ◊tt{2.5} ◊em{de facto}.
It was great for the time before the v3 scheduler, but now we have better options.

If you want to migrate from this workaround, change the interval modifier back to ◊tt{1.00} and the ease factor to whatever you want (as discussed above).
The latter can be altered using the ◊link["https://ankiweb.net/shared/info/819023663"]{RefoldEase} add-on.
