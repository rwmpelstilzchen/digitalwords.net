#lang pollen

◊define-meta[title]{Generating Anki decks with audio from the Tatoeba Project}
◊define-meta[publish-date]{2019-12-20}
◊define-meta[last-update]{2019-12-21}
◊define-meta[language]{eng}
◊define-meta[toc]{#t}
◊(define-meta tldr
	(string-append
		(p "If you look for the precompiled decks, " (strong (a ((href "#sec:precompiled:download") (class "xref")) "here")) " is a list.")
		(p "A Hebrew version of this page is available " (a ((href "index.heb.html") (class "xref")) "here") ".")
	)
 )



◊section{Background}

The most effective ◊link["http://ankisrs.net/"]{Anki} decks I know for language acquisition share three features:

◊bullet-list{
	They include sentences, as opposed to vocabulary items without co-text.


	All of these sentences have audio recorded by native speakers.


	They are incrementally sorted, meaning that ideally in each sentence you learn no more than one new word or grammatical feature.
	This idea is also called ‘the ◊link["https://github.com/kaegi/MorphMan/wiki/I-plus-1"]{◊em{i+1}} principle’.
}

The ◊link["https://tatoeba.org/"]{Tatoeba Project} has millions of sentences, many of which were recorded by native speakers.
As far as I am aware, there are no pre-made tools for importing Tatoeba sentences with audio to Anki, so I made such a tool and created decks for all of the languages that have recordings, incrementally sorted using the ◊link["https://ankiweb.net/shared/info/900801631"]{MorphMan} addon.
In order to save you the trouble of creating the decks on your own, I’ve already ◊strong{◊link["#sec:precompiled:download"]{compiled decks}} for all of the languages that have audio sentences in Tatoeba.

Tatoeba is an amazing public and open collaborative project, but as all such projects it is not free of mistakes and low-quality entries.
The volunteers do their best to minimize them, but they still exists.
Having an audio recording can be seen as a filter for better quality sentences: if someone took their time to record a sentence, it is presumably of good quality.



◊section{Deck generation process}

I made the decks on a Linux machine, but I guess you could do the same on macOS or Windows (with ◊wiki-en{Cygwin} or something similar).
The tools we will use are:
◊wiki-en["Unix shell"]{sh},
◊wiki-en{Wget},
◊wiki-en["tar (computing)"]{tar},
◊wiki-en{bzip2},
◊wiki-en{sed},
◊wiki-en{uniq},
◊wiki-en{SQLite} and
◊wiki-en["Python (programming language)"]{Python}.

If for some reason the pre-made decks do not suit your needs, don’t hesitate to ◊link["https://me.digitalwords.net/"]{contact me} if you need help with generating a deck the do.



◊subsection{Downloading the data}

Tatoeba exports its data as ◊link["https://tatoeba.org/eng/downloads"]{downloadable CSV files}.
The files we need are:
◊code{◊link["https://downloads.tatoeba.org/exports/sentences.tar.bz2"]{sentences.csv}},
◊code{◊link["https://downloads.tatoeba.org/exports/links.tar.bz2"]{links.csv}},
◊code{◊link["https://downloads.tatoeba.org/exports/tags.tar.bz2"]{tags.csv}} and
◊code{◊link["https://downloads.tatoeba.org/exports/sentences_with_audio.tar.bz2"]{sentences_with_audio.csv}}:

We can download, unpack and prepare them for later use by running this script:

◊filebox-highlight["download_and_prepare_csv.sh" 'sh]{
	#!/usr/bin/sh

	mkdir -p csv
	pushd csv

	# Download
	wget https://downloads.tatoeba.org/exports/sentences.tar.bz2
	wget https://downloads.tatoeba.org/exports/links.tar.bz2
	wget https://downloads.tatoeba.org/exports/tags.tar.bz2
	wget https://downloads.tatoeba.org/exports/sentences_with_audio.tar.bz2

	# Decompress and untar
	for f in *.tar.bz2; do
		tar jxf $f
	done

	# Prepare
	sed 's/"/""/g;s/[^\t]*/"&"/g' sentences.csv > sentences.escaped_quotes.csv
	sed 's/"/""/g;s/[^\t]*/"&"/g' tags.csv > tags.escaped_quotes.csv
	uniq sentences_with_audio.csv > sentences_with_audio.uniq.csv

	# Remove compressed files
	rm -i {sentences, links, tags, sentences_with_audio}.tar.bz

	popd
}



◊subsection{Making a local database}

Now we want to make a database so we will be able to run queries.
I chose ◊wiki-en{SQLite} because it is local, fast, convenient and easy to setup.
By running this SQL file (◊code{sqlite3 -init create_db.sql}) we create a database with the tables we need and import the data:

◊filebox-highlight["create_db.sql" 'sql]{
	.open tatoeba.sqlite3

	-- Tatoeba’s database has many deleted entries in `sentences` which are referenced from `sentences_with_audio` and `links`; expect tons of warning messages
	PRAGMA foreign_keys = ON;

	CREATE TABLE sentences (
		sentence_id INTEGER PRIMARY KEY,
		lang TEXT,
		text TEXT
	);
	CREATE TABLE sentences_with_audio (
		sentence_id INTEGER PRIMARY KEY,
		username TEXT,
		license TEXT,
		attribution_url TEXT,
		FOREIGN KEY (sentence_id) REFERENCES sentences(sentence_id)
	);
	CREATE TABLE links (
		sentence_id INTEGER,
		translation_id INTEGER,
		FOREIGN KEY (sentence_id) REFERENCES sentences(sentence_id),
		FOREIGN KEY (translation_id) REFERENCES sentences(sentence_id)
	);
	CREATE TABLE tags (
		sentence_id INTEGER,
		tag_name TEXT,
		FOREIGN KEY (sentence_id) REFERENCES sentences(sentence_id)
	);

	CREATE INDEX links_index ON links(sentence_id, translation_id);
	CREATE INDEX tags_index ON tags(sentence_id, tag_name);

	.separator "\t"
	.import csv/sentences.escaped_quotes.csv sentences
	.import csv/sentences_with_audio.uniq.csv sentences_with_audio
	.import csv/links.csv links
	.import csv/tags.escaped_quotes.csv tags
}

The CSV files has many references to deleted sentences, so many warnings will be shown.



◊subsection{Writing the main query}

Now we want to write a query that will output an Anki-importable file with this data in each row:

◊bullet-list{
	Tatoeba sentence ID.


	The text of the sentence in the target language.


	Reference to the audio file.


	Tatoeba tags (such as ◊code{colloquial}).


	Translations of the sentence to languages we already know.
}

For this purpose I wrote this patchy Python script:

◊filebox-highlight["query.py" 'python]{
	#!/usr/bin/env python3

	import argparse
	import csv
	import os
	import sqlite3

	output_dir = 'output'
	native_langs = []


	def native_lang_columns():
		def native_lang_column(lang):
			return f"""
			"<ul class=""translations""><li>" ||
			(
					SELECT group_concat(sentences.text, "</li><li>")
					FROM links JOIN sentences
					ON
							links.translation_id = sentences.sentence_id
					WHERE
							links.sentence_id = target_sentences.sentence_id
							AND
							sentences.lang = '{lang}'
							)
			|| "</li></ul>"
			"""
		result = ""
		for lang in native_langs[:-1]:
			result += native_lang_column(lang) + ", "
		result += native_lang_column(native_langs[-1])
		return result


	def main():
		parser = argparse.ArgumentParser(
			description="Make a CSV files of sentences from the Tatoeba Project that have audio, along with their translations into selected languages")
		parser.add_argument("-t", "--target", type=str,
							help="target language",
							required=True)
		parser.add_argument("-n", "--native", type=str,
							help="native languages (space-delimited, within quotes)",
							required=True)
		parser.add_argument("-d", "--database", type=str,
							help="database file",
							default = "tatoeba.sqlite3")
		args = parser.parse_args()
		global native_langs
		native_langs = args.native.split(" ")

		conn = sqlite3.connect(args.database)
		c = conn.cursor()

		query = f"""
		SELECT
				target_sentences.sentence_id,
				target_sentences.text,
				"[sound:tatoeba_" || "{args.target}" || "_" || target_sentences.sentence_id || ".mp3]",
				"<ul class=""tags""><li>" ||
				(
					SELECT group_concat(tag_name, "</li><li>")
					FROM tags
					WHERE tags.sentence_id = target_sentences.sentence_id
				)
				|| "</li></ul>",
				{native_lang_columns()}
		FROM
				sentences AS target_sentences
		WHERE
				target_sentences.lang = "{args.target}" AND
				target_sentences.sentence_id IN (SELECT sentence_id FROM sentences_with_audio)
		;
		"""
		if not os.path.exists('output'):
			os.makedirs('output')
		with open(f'{os.path.join(output_dir, args.target)} → {args.native}.csv', 'w', newline='') as csvfile:
			out = csv.writer(csvfile, delimiter='\t',
					quotechar='|', quoting=csv.QUOTE_MINIMAL)
			for row in c.execute(query):
				out.writerow(row)

		conn.close()


	if __name__ == '__main__':
		main()
}

In order to produce, for example, a deck of Finnish sentences with audio along with translations into English, Russian, Spanish, Italian and Japanese (whenever available), we use it like this (with ◊wiki-en["Wikipedia:WikiProject_Languages/List_of_ISO_639-3_language_codes_(2019)"]{ISO 639-3 codes}):

◊highlight['sh]{
	./query.py -t fin -n "eng rus spa ita jpn"
}

Running the script will produce a CSV file in the ◊code{output} subdirectory.



◊subsection{Downloading the audio files}

Each audio file is available from this URL: ◊code{https://audio.tatoeba.org/sentences/◊textsc{sentence_id}}.
In order to make a list of all of the files to be downloaded we use this Python script:

◊filebox-highlight["audio_urls.py" 'python]{
	#!/usr/bin/env python3

	import argparse
	import csv
	import sqlite3


	def main():
		parser = argparse.ArgumentParser(
			description="Make a list of URLs of audio files for a specific language from the Tatoeba Project")
		parser.add_argument("-t", "--target", type=str,
							help="target language",
							required=True)
		parser.add_argument("-d", "--database", type=str,
							help="database file",
							default = "tatoeba.sqlite3")
		args = parser.parse_args()

		conn = sqlite3.connect(args.database)
		c = conn.cursor()

		query = f"""
	SELECT
		sentence_id
	FROM
		sentences
	WHERE
		lang = '{args.target}' AND
		sentence_id IN (SELECT sentence_id FROM sentences_with_audio)
		"""

		for row in c.execute(query):
			print("https://audio.tatoeba.org/sentences/" + args.target + "/" + str(row[0]) + ".mp3")


	if __name__ == '__main__':
		main()
}

This shell script downloads the files and renames them properly:

◊filebox-highlight["download_and_prepare_audio" 'sh]{
	#!/usr/bin/sh

	mkdir -p output/audio

	# Source: https://stackoverflow.com/a/11850469
	./audio_urls.py -t $1 -d $2 | xargs -n 1 -P 2 wget --directory-prefix=output/audio/ --continue

	for f in output/audio/*; do
		mv "$f" "$(echo $f | sed 's/^output\/audio\//output\/audio\/tatoeba_'$1'_/g')";
	done
}



◊subsection{Creating a proper note type}

Now that we have the data exported to an importable CSV files we want to create a proper note type for it ◊em{(Tools → Manage Note Types → Add)}.
We need the following fields:
◊code{sentence_id},
◊code{◊textsc{target}} (e.g. ◊code{fin}),
◊code{audio},
◊code{tags}, and
a field for each of the languages we are familiar with.
In addition, we need these fields for ◊link["https://ankiweb.net/shared/info/900801631"]{MorphMan}:
◊code{MorphMan_FocusMorph},
◊code{MorphMan_Index},
◊code{MorphMan_Unmatures},
◊code{MorphMan_UnmatureMorphCount},
◊code{MorphMan_Unknowns},
◊code{MorphMan_UnknownMorphCount},
◊code{MorphMan_UnknownFreq}.
Delete the default ◊code{Front} and ◊code{Back} fields and close the window.

Now, pressing the ◊em{Cards} button will open a window similar to this:

◊figure-en["cards.png"]{Card editing window.}

We create three cards: Reading, Listening and Production.
For Reading in our Finnish exemple we write

◊highlight['html]{
	<p>fin</p>
}

in the front template and

◊highlight['html]{
	{{FrontSide}}

	<hr id="answer">

	<p id="tags">{{tags}}</p>

	<p>{{eng}}</p>
	<p>{{rus}}</p>
	<p>{{spa}}</p>
	<p>{{ita}}</p>
	<p>{{jpn}}</p>

	<p>{{audio}}</p>

	<p id="tatoeba"><a href="https://tatoeba.org/eng/sentences/show/{{sentence_id}}"><img src="_tatoeba.svg" /></a></p>
}

in the back template.

The shared styling should be something like this:
◊highlight['css]{
	.card {
		font-family: sans;
		font-size: 20px;
		text-align: center;
		color: #111;
		background-color: #fffff8;
	}
	.card.night_mode {
		background-color: #2E3440;
		color: #D8DEE9;
	}

	#lookup a, #tatoeba {
		color: inherit;
		text-decoration: inherit;
	}

	#tatoeba img {
		width: 1em;
		height: auto;
	}

	#tags {
		font-size: small;
	}

	.hebrew, .arabic {
		direction: rtl;
	}

	.translations, .tags {
		list-style-type: none;
		margin: 0;
		padding: 0;
	}
	.translations li, .tags li {
		display: inline;
	}
	.translations li:after, .tags li:after {
		content: " · "
	}
	.translations li:last-child:after, .tags li:last-child:after {
		content: ""
	}
}

Listening’s front template:

◊highlight['html]{
	<p>{{audio}}</p>
}

Listening’s back template:

◊highlight['html]{
	{{FrontSide}}

	<hr id="answer">

	<p id="lookup">{{fin}}</p>

	<p id="tags">{{tags}}</p>

	<p>{{eng}}</p>
	<p>{{rus}}</p>
	<p>{{spa}}</p>
	<p>{{ita}}</p>
	<p>{{jpn}}</p>

	<p id="tatoeba"><a href="https://tatoeba.org/eng/sentences/show/{{sentence_id}}"><img src="_tatoeba.svg" /></a></p>
}

Production’s front template:

◊highlight['html]{
	<p>{{eng}}</p>
	<p>{{rus}}</p>
	<p>{{spa}}</p>
	<p>{{ita}}</p>
	<p>{{jpn}}</p>
}

Production’s back template:

◊highlight['html]{
	{{FrontSide}}

	<hr id="answer">

	<p id="lookup">{{fin}}</p>

	<p id="tags">{{tags}}</p>

	<p>{{audio}}</p>

	<p id="tatoeba"><a href="https://tatoeba.org/eng/sentences/show/{{sentence_id}}"><img src="_tatoeba.svg" /></a></p>
}



◊subsection{Importing into Anki}

Import the CSV file (◊em{File → Import}).
Check ◊em{Allow HTML in fields}.
The fields in the CSV file and our note type should match.

Copy the MP3 files to your media collection directory.
Its location depends on your operating system; read more ◊link["https://apps.ankiweb.net/docs/manual.html#file-locations"]{here}. Download ◊link["https://tatoeba.org/img/tatoeba.svg"]{Tatoeba’s logo} to the same directory, renaming it ◊code{_tatoeba.svg}.

Now we have a working deck and it’s time to check it using the preview option of the Browse window.



◊subsection{Sorting the cards}

One last thing you might want to do is to sort the cards so new words occur incrementally.
◊link["https://ankiweb.net/shared/info/900801631"]{MorphMan} is an Anki addon that does just this.
Read about it in the ◊link["https://github.com/kaegi/MorphMan/wiki"]{wiki} and/or watch YouTube ◊link["https://www.youtube.com/results?search_query=MorphMan+Anki"]{videos} about it.

That’s it.
Enjoy learning whatever language you want to learn :-)

If you benefit from Tatoeba please consider ◊link["https://tatoeba.org/eng/users/register"]{joining} the project and contribute sentences, translations and audio recordings (see ◊link["https://en.wiki.tatoeba.org/articles/show/quick-start"]{this guide}), or ◊link["https://tatoeba.org/eng/donate"]{donating} money.



◊section[#:label "sec:precompiled"]{Precompiled decks}

I made decks for all of the language in Tatoeba that have audio recordings (except English and Spanish, which have too many sentences) so people will not have to go through the pain (and pleasure…) of making them.

In order to obtain a list of the relevant languages we can run this query:

◊filebox-highlight["languages_with_audio.sql" 'sql]{
	.open tatoeba.sqlite3

	SELECT lang, COUNT (sentences.sentence_id) AS audio_sentences_no
	FROM sentences_with_audio JOIN sentences
	ON sentences_with_audio.sentence_id = sentences.sentence_id
	GROUP BY lang
	ORDER BY audio_sentences_no DESC;
}

I chose to include translation for the five languages that share most co-translated sentences with the target language.
In order to check what these languages are we can run this query:

◊filebox-highlight["statistics_for_translated_audio_sentences.sql" 'sql]{
	.open tatoeba.sqlite3

	SELECT lang, COUNT (sentences.sentence_id) AS counter
	FROM sentences JOIN links
	ON sentences.sentence_id = links.translation_id
	WHERE
		links.sentence_id IN
		(
			SELECT sentence_id
			FROM sentences
			WHERE
				lang = 'fin'
				AND
				sentence_id in (SELECT sentence_id FROM sentences_with_audio)
		)
	GROUP BY lang
	ORDER BY counter DESC
	LIMIT 5;
}



◊subsection[#:label "sec:precompiled:download"]{Downloading the decks}

The result of the whole process was uploaded to ◊link["https://ankiweb.net/shared/decks/"]{AnkiWeb}.
The decks have names following this format, conforming to the 60 character limit:

◊highlight['txt]{
	All LANGUAGE sentences with recorded audio from Tatoeba
}

If you find the decks useful, please leave positive feedback on AnkiWeb: it will make me happy and will others find the decks.

For backup purposes you can download the decks from this website’s server; the link is designated by ⭳.
I prefer you download the decks from AnkiWeb (◊img[#:src "ankiweb.png" #:style "width: 1em; height: auto;"]{}), as decks that don’t get enough downloads are removed from AnkiWeb.

A temporary note: one can share only 10 decks a week on AnkiWeb.
This is the reason not all decks have AnkiWeb links.
I hope soon I will be able to complete sharing all decks there.



◊(define (deck iso target native_a native_a_no native_b native_b_no native_c native_c_no native_d native_d_no native_e native_e_no sentences date ankiweb)
	`(tr
		(td (span ((class "no_of_sentences")) ,sentences))
		(td (b ,target))
		(td ,(if (equal? native_a "") "" "→"))
		(td ,native_a " " (span ((class "no_of_sentences")) ,native_a_no))
		(td ,native_b " " (span ((class "no_of_sentences")) ,native_b_no))
		(td ,native_c " " (span ((class "no_of_sentences")) ,native_c_no))
		(td ,native_d " " (span ((class "no_of_sentences")) ,native_d_no))
		(td ,native_e " " (span ((class "no_of_sentences")) ,native_e_no))
		(td ,date)
		(td ,(if (equal? ankiweb "") "" `(a ((href  ,(string-append "https://ankiweb.net/shared/info/" ankiweb)) (class "barelink")) (img ((src "ankiweb.png"))))))
		(td (a ((href  ,(string-append "https://media.digitalwords.net/anki/tatoeba-audio/" iso ".apkg")) (class "barelink")) "⭳"))
	 )
 )

◊style{
	.decktable {
		font-size: 1.2rem;
		direction: ltr;
	}
	.decktable td {
		vertical-align: top;
	}
	.no_of_sentences {
		color: ◊|col-mouse|;
	}
	.decktable img {
		width: 1em;
		height: auto;
		max-width: none;
	}
}
◊table[#:class "decktable fullwidth"]{
	◊;◊deck["" "" "English" "()" "" "()" "" "()" "" "()" "" "()" "" "2019/10/12" ""]
	◊deck["deu-1" "German 1/2" "English" "(18733)" "Esperanto" "(9873)" "French" "(8322)" "Russian" "(7717)" "Spanish" "(6917)" "19582" "2019/10/12" "139315945"]
	◊deck["deu-2" "German 2/2" "" "" "" "" "" "" "" "" "" "" "" "" "1961742728"]
	◊deck["por" "Portuguese" "English" "(10163)" "Spanish" "(2282)" "Esperanto" "(1704)" "French" "(1080)" "Russian" "(653)" "11019" "2019/10/12" "1859535970"]
	◊deck["fra" "French" "English" "(7956)" "Esperanto" "(6280)" "Russian" "(5051)" "German" "(2921)" "Ukrainian" "(2204)" "8181" "2019/10/12" "1373615545"]
	◊deck["hun" "Hungarian" "English" "(6048)" "German" "(1263)" "Esperanto" "(643)" "Italian" "(529)" "French" "(350)" "6720" "2019/10/12" "1691262801"]
	◊deck["rus" "Russian" "English" "(3294)" "Japanese" "(1874)" "French" "(1662)" "German" "(1268)" "Ukrainian" "(1246)" "4690" "2019/10/12" "604511069"]
	◊deck["ber" "Berber" "English" "(4494)" "Spanish" "(320)" "French" "(315)" "Kabyle" "(60)" "Arabic" "(46)" "4598" "2019/10/12" "342441168"]
	◊deck["epo" "Esperanto" "English" "(3901)" "French" "(1340)" "German" "(1172)" "Dutch" "(748)" "Spanish" "(570)" "4601" "2019/10/12" "1832773631"]
	◊deck["fin" "Finnish" "English" "(4017)" "Russian" "(1174)" "Spanish" "(1087)" "Italian" "(766)" "Japanese" "(286)" "4057" "2019/10/12" "702922008"]
	◊deck["wuu" "Wu Chinese" "Mandarin" "(2489)" "French" "(633)" "English" "(427)" "Spanish" "(34)" "Yue Chinese" "(32)" "2491" "2019/10/12" "949774152"]
	◊deck["nld" "Dutch" "Esperanto" "(1935)" "English" "(1698)" "Ukrainian" "(1460)" "German" "(1321)" "Spanish" "(1231)" "1961" "2019/10/12" "742594829"]
	◊deck["cmn" "Mandarin Chinese" "French" "(1317)" "German" "(1280)" "English" "(1260)" "Wu Chinese" "(700)" "Spanish" "(605)" "1678" "2019/10/12" "110314713"]
	◊deck["jpn" "Japanese" "English" "(1278)" "Russian" "(1249)" "Finnish" "(1050)" "German" "(1025)" "French" "(658)" "1283" "2019/10/12" "1823020002"]
	◊deck["heb" "Hebrew" "English" "(1086)" "Esperanto" "(125)" "Polish" "(120)" "Russian" "(89)" "French" "(83)" "1086" "2019/10/12" "2086474061"]
	◊deck["lat" "Latin" "English" "(974)" "Portugeuse" "(430)" "Spanish" "(375)" "French" "(293)" "Esperanto" "(199)" "1067" "2019/10/12" "1699554124"]
	◊deck["dtp" "Central Dusun" "English" "(401)" "Japanese" "(45)" "Coastal Kadazan" "(31)" "" "" "" "" "480" "2019/10/12" "392451841"]
	◊deck["mar" "Marathi" "English" "(376)" "Hindi" "(142)" "" "" "" "" "" "" "376" "2019/10/12" "708121003"]
	◊deck["ukr" "Ukrainian" "English" "(363)" "French" "(28)" "German" "(20)" "Italian" "(17)" "Spanish" "(15)" "363" "2019/10/12" "1520102967"]
	◊deck["pol" "Polish" "English" "(222)" "Dutch" "(98)" "German" "(30)" "Ukrainian" "(27)" "Russian" "(21)" "224" "2019/10/12" "1086608121"]
	◊deck["tha" "Thai" "English" "(87)" "Esperanto" "(39)" "German" "(38)" "French" "(38)" "Russian" "(32)" "134" "2019/10/12" "1315022992"]
	◊deck["cat" "Catalan" "English" "(111)" "Spanish" "(41)" "Ukrainian" "(31)" "French" "(19)" "Esperanto" "(17)" "112" "2019/10/12" ""]
	◊deck["cbk" "Chavacano" "English" "(53)" "" "" "" "" "" "" "" "" "60" "2019/10/12" ""]
	◊deck["ron" "Romanian" "English" "(51)" "Esperanto" "(51)" "Dutch" "(50)" "German" "(34)" "Spanish" "(34)" "53" "2019/10/12" ""]
	◊deck["tur" "Turkish" "English" "(37)" "German" "(13)" "Esperanto" "(8)" "Spanish" "(5)" "Swedish" "(3)" "37" "2019/10/12" ""]
	◊deck["nst" "Naga (Tangshang)" "English" "(28)" "" "" "" "" "" "" "" "" "28" "2019/10/12" ""]
}
