#lang pollen

◊define-meta[title]{הפקת חפיסות עם שמע מפרוייקט טטואבה}
◊define-meta[publish-date]{2019-12-20}
◊define-meta[last-update]{2019-12-21}
◊define-meta[toc]{#t}
◊(define-meta tldr
	(string-append
        (p "אם אתן מחפשות להוריד חפיסות מוכנות של משפטים עם הקלטות אודיו, " (strong (a ((href "#sec:precompiled:download") (class "xref")) "כאן")) " יש רשימה.")
        (p ((style "direction: ltr")) "An English version of this page is available " (a ((href "index.eng.html") (class "xref")) "here") ".")
    )
)
◊; ◊strong{◊a[#:href '#sec:precompiled:download']{כאן}} יש רשימה.
	◊;◊div[#:style "direction: ltr"]{An English version of this page is available <a href="index.eng.html">here</a>.}


◊section{רקע}

◊margin-figure["tatoeba.svg"]{}
בדף הזה אתאר שיטה ליצירה אוטומטית של חפיסות מהסוג שלדעתי הוא הכי אפקטיבי: הלימוד בעזרתן הוא המהיר ביותר, הקל ביותר ומקנה את ההבנה השלמה ביותר.
העקרון פשוט, ובנוי מכמה חלקים:

◊bullet-list{
	בכל שלב בלימוד נקרא לסך הידע על השפה i: כל מה שאתן יודעות עד אותה הנקודה.
	העקרון הוא שכשלומדות דבר חדש, תמיד טוב לבנות על הקיים ולהוסיף באופן הדרגתי: לא לקפוץ מהר מדי גבוה מדי וגם לא להתפרש לרוחב בלי לנצל את העוגנים שאפשר להעזר בהם בידע הקיים.
	במילים אחרות, אנחנו רוצות ללמוד בכל שלב ◊link["https://github.com/kaegi/MorphMan/wiki/I-plus-1"]{◊L{i+1}}, כלומר להוסיף משהו חדש ◊em{אחד} על הידע הקיים ולהתבסס עליו.


	הנדבך השני הוא לימוד של משפטים, ולא של מילים מנותקות מהקשר.
	אנחנו רגילות מבית־הספר ללמוד מילים בנפרד; זאת דרך קשה ולא אפקטיבית ללימוד אוצר מילים.
	נחשוב, לדוגמה, על מקרה כמו של ◊L{get} באנגלית: יש לה ◊link["https://en.wiktionary.org/wiki/get#Verb"]{31 פירושים בוויקימילון}, ולימוד שלהם באופן ערטילאי נדון לכשלון.
	לעומת זאת, אם אנחנו לומדות משפטים קצרים וממוקדים, גם קל יותר לזכור את המילים החדשות וגם אפשר להבין אותן ואת אופני השימוש האידיומטיים בהן באופן טוב יותר.
	הופעה של אותה המילה במשפטים שונים תאיר פנים שונות שלה ושל השימוש הטבעי בה, גם מבחינת מבנים לשוניים וגם מבחינת דקויות משמעות.
	המשפטים צריכים להיות קצרים מספיק כדי שיהיה קל לזכור אותם באופן לא מעורפל, והם צריכים גם להיות עצמאיים מבחינה פרגמטית (כלומר, שאפשר להבין אותם במנותק מהקשר רחב יותר בשיחה).


	החלק השלישי בעקרון המנחה הוא ששפה, עבור רובנו, נטועה לא רק בחוש הראיה — שפה כתובה — אלא גם ובעיקר בחוש השמיעה.
	ללמוד שפה בלי לשמוע אותה ובלי לגלגל אותה על הלשון זה סוג של „◊L{People talking without speaking}” מ־◊L{◊wiki-he{The Sound of Silence}}.◊;
	◊numbered-note{
		גם בשפות מתות יש, לדעתי, ערך בשמיעה ובתרגול של אופן ההיגוי המשוחזר.
	}
	הכי טוב זה לעשות שימוש פעיל בשפה ביחד עם דוברות ילידיות, בין אם בחילופי־שפות ובין אם במפגש מסוג אחר (מתווך־מחשב או לא), אבל לא תמיד אנחנו לומדות שפות שיש לנו דרך לדבר בהן עם דוברות ילידיות ולא תמיד זה מתאים לנו או להן.
	כעזר מלאכותי אפשר להשתמש בהקלטות של דוברות ילידיות של המשפטים שדיברנו עליהם קודם.
	זה מאפשר לנו גם לדעת איך להגות נכון ובמקצב ובאינטונציה טבעיים◊;
	◊numbered-note{
		בהקשר הזה כדאי להכיר טכניקה מועילה מאוד בשם ◊L{◊wiki-en["Speech shadowing"]{shadowing}}.
		אפשר ללמוד עליה גם מ◊link["https://www.youtube.com/results?search_query=shadowing"]{סרטונים ביוטיוב}.
	}
	עד כמה שניתן וגם להתאמן על הבנת הנשמע (כשבחלק הקדמי של הכרטיס יש את השמע ובחלק האחורי את המשפט הכתוב בשפה הנלמדת ואת פשרו בשפה/ות שאנחנו יודעות).
	יש מי שמשתמשות ב◊wiki-he["סינתזת דיבור"]{שמע מסונתז}; אמנם התחום הזה ראה שיפור כביר בשנים האחרונות, אבל אני עדיין ממליץ לכן להשתמש בשמע מסונתז רק אם אתן רוצות להשמע כמו רובוט…
}

אם נסכם את שלושת החלקים נקבל: לימוד של משפטים קצרים ועצמאיים־פרגמטית עם שמע טבעי שמסודרים באופן הדרגתי.
נכון הגיוני?

עכשיו עולה השאלה מאיפה אנחנו משיגות משפטים כאלה (רצוי עם תרגום לשפה שאנחנו כבר מכירות היטב), ולא רק זה אלא גם שמע מוקלט שלהם, ולא רק זה אלא גם שהם יהיו מסודרים בסדר הטוב ביותר, שמציג לנו בכל פעם כמה שפחות מידע חדש ובונה על הקיים.
כאן בדיוק נכנס הדף הזה: את המשפטים נוריד באופן אוטומטי מפרוייקט מופלא בשם ◊L{◊link["https://tatoeba.org/"]{Tatoeba}} ואת הסידור נעשה בעזרת תוסף לאנקי בשם ◊L{◊link["https://ankiweb.net/shared/info/900801631"]{MorphMan}}.
כדי לחסוך לכן את העבודה שבלעשות את התהליך מחדש, יצרתי ◊strong{◊link["#sec:precompiled:download"]{חפיסות מוכנות}} עבור כל השפות שיש להן משפטים מוקלטים בטטואבה.

פרוייקט טטואבה הוא מיזם שיתופי־ציבורי ליצירה של מאגר רב־לשוני של משפטים לדוגמה שמתורגמים בין שפות◊;
◊numbered-note{
	גם באופן ישיר (לדוגמה, משפט ביפנית שמתורגם ישירות לעברית על ידי דוברת עברית שיודעת יפנית) וגם באופן עקיף (לדוגמה, אם התרגום העברי של המשפט היפני תורגם ל◊wiki-he{מאורית} על ידי דוברת מאורית שיודעת עברית, יש גם קשר עקיף בין המשפט היפני והמאורי, למרות שהוא בתיווך עברי).
}.
לחלק מהמשפטים יש גם ◊link["https://tatoeba.org/eng/audio/index"]{הקלטות באתר}; נכון לזמן כתיבת שורות אלה יש 7,853,301 משפטים בסך הכל, מתוכם 637,768 גם מוקלטים.
לא רק שזה מיזם שיתופי וציבורי, הוא גם פתוח וחופשי: כל הטקסט וחלק מהשמע משוחרר תחת ◊wiki-he["תוכן חופשי"]{רשיונות חופשיים} ואפשר ◊link["https://tatoeba.org/eng/downloads"]{להוריד} את קבצי מסד הנתונים באופן נוח.

לא כל המשפטים באותה הרמה.
כמו בכל מיזם שיתופי כזה, תמיד יהיו טעויות ורשומות באיכות נמוכה יותר◊;
◊numbered-note{
	הבעיה היא גם בטעויות של דוברים ילידיים (קורה!), אבל בעיקר במי שלומדות שפה כלשהי ובוחרות להתאמן בה בטטואבה, למרות הבקשות המפורשות באתר להמנע מכך.
	אם אתן רוצות להתאמן בכתיבה ולקבל משוב מדוברות ילידיות, ◊link["https://lang-8.com/"]{◊L{Lang-8}} הוא האתר בשבילכן.
};
כמו ברוב המיזמים האלה, המתנדבות תמיד פועלות כדי לשפר את האיכות ולנפות החוצה רשומות לא מתאימות.
לצרכינו עצם קיומה של הקלטה למשפט כבר עושה סינון טוב: אם דוברת של השפה בחרה לטרוח ולהקליט אותו, כנראה שהוא נכון וטבעי.
למרות זאת כדאי להיות זהירות, במיוחד בתרגומים של המשפטים לשפות אחרות.



◊section{תהליך ההפקה}

בסעיף הזה אתאר איך ליצור חפיסות בעצמכן מהנתונים שבטטואבה.
התהליך דורש ידע טכני במחשבים שאין לכולן.
את המערכת בניתי בגנו/לינוקס, אבל אין סיבה עקרונית שזה לא יעבוד גם ב־◊L{macOS} או ב„חלונות” בעזרת ◊L{◊wiki-he{Cygwin}} או מערכת דומה.
הכלים שבהם נעשה שימוש הם:
◊L{◊wiki-en["Unix shell"]{sh}},‏
◊L{◊wiki-en{Wget}},‏
◊L{◊wiki-en["tar (computing)"]{tar}},‏
◊L{◊wiki-en{bzip2}},‏
◊L{◊wiki-he{sed}},‏
◊L{◊wiki-en{uniq}},‏
◊L{◊wiki-he{SQLite}},‏
ו־◊L{◊wiki-he{Python}}.
זאת דוגמה יפה, לדעתי, לאופי ה◊wiki-en["Modular design"]{מודולרי} של הכלים האלה: כל אחד מצידו עושה משהו אחד, עושה אותו טוב, ומאפשר שיתוף פעולה עם כלים אחרים, כמו חלקים של לגו שמתחברים אחד לשני.

אם זה מעניין אתכן ועדיין אין לכן את הידע תוכלו להשתמש בזה כשער לעבור דרכו וללמוד, אבל ברור לי שלא לכולן זה מתאים.
אם מסיבה כלשהי ◊link["#sec:precompiled:download"]{החפיסות המוכנות} לא עונות על צרכיך ויש לך צורך בעזרה, ◊link["https://me.digitalwords.net/"]{כתבי לי} ואנסה לעזור במה שצריך, בין אם בהנחיה או בבניה של חפיסה ושליחה שלה.
אחרי שכבר בניתי את המערכת ואני מכיר אותה זה לוקח לי רגע להכין חפיסות.



◊subsection{הורדה של הנתונים}

השלב הראשון הוא ◊link["https://tatoeba.org/eng/downloads"]{להוריד} את קבצי מסד הנתונים של טטואבה.
הם מיוצאים באופן אוטומטי כקבצי ◊L{◊wiki-he{CSV}}.
הקבצים שאנחנו צריכות הם:

◊bullet-list{
	◊code{◊link["https://downloads.tatoeba.org/exports/sentences.tar.bz2"]{sentences.csv}}:
	כל המשפטים (הצמתים ב◊wiki-he["תורת הגרפים"]{גרף}).


	◊code{◊link["https://downloads.tatoeba.org/exports/links.tar.bz2"]{links.csv}}:
	הקשרים בין המשפטים (הקשתות בגרף, לא מכוונות ומופיעות פעמים, כ־◊L{A→B} וכ־◊L{B→A}): איזה משפט מקביל לאיזה משפט.


	◊code{◊link["https://downloads.tatoeba.org/exports/tags.tar.bz2"]{tags.csv}}:
	משפטים יכולים להיות מסומנים בתגים (לדוגמה, ◊code{colloquial} בשביל לסמן משלב דיבורי).


	◊code{◊link["https://downloads.tatoeba.org/exports/sentences_with_audio.tar.bz2"]{sentences_with_audio.csv}}:
	רשימה של המשפטים שיש להם קבצי שמע, עם מטא־נתונים על המקליטות והרשיונות.
}

אנחנו רוצות להוריד אותם, לפרוס את הדחיסה, ולעשות באופן אוטומטי כמה תיקונים כדי שנוכל להשתמש בהם בהמשך, כך:

◊filebox-highlight["download_and_prepare_csv.sh" 'sh]{
	#!/usr/bin/sh

	mkdir -p csv
	pushd csv

	# Download
	wget https://downloads.tatoeba.org/exports/sentences.tar.bz2
	wget https://downloads.tatoeba.org/exports/links.tar.bz2
	wget https://downloads.tatoeba.org/exports/tags.tar.bz2
	wget https://downloads.tatoeba.org/exports/sentences_with_audio.tar.bz2

	# Decompress and untar
	for f in *.tar.bz2; do
		tar jxf $f
	done

	# Prepare
	sed 's/"/""/g;s/[^\t]*/"&"/g' sentences.csv > sentences.escaped_quotes.csv
	sed 's/"/""/g;s/[^\t]*/"&"/g' tags.csv > tags.escaped_quotes.csv
	uniq sentences_with_audio.csv > sentences_with_audio.uniq.csv

	# Remove compressed files
	rm -i {sentences, links, tags, sentences_with_audio}.tar.bz

	popd
}

כדי להריץ זה נפקוד ◊code{sh download_and_prepare_csv.sh} בספריה אליה הורדנו הקובץ ה־◊L{sh}.



◊subsection{יצירת מסד נתונים מקומי}

עכשיו נוכל לייבא את הנתונים מקבצי ה־◊L{CSV} למסד נתונים, אליו נוכל לשלוח שאילתות ◊L{◊wiki-he{SQL}}.
נשתמש ב־◊L{◊wiki-he{SQLite}} בגלל שהוא מקומי, מהיר, נוח ולא דורש התעסקות.
קובץ ה־◊L{SQL} הזה יוצר קובץ מסד נתונים, מוסיף לו טבלאות ומייבא אליו את הנתונים:

◊filebox-highlight["create_db.sql" 'sql]{
	.open tatoeba.sqlite3

	-- Tatoeba’s database has many deleted entries in `sentences` which are referenced from `sentences_with_audio` and `links`; expect tons of warning messages
	PRAGMA foreign_keys = ON;

	CREATE TABLE sentences (
		sentence_id INTEGER PRIMARY KEY,
		lang TEXT,
		text TEXT
	);
	CREATE TABLE sentences_with_audio (
		sentence_id INTEGER PRIMARY KEY,
		username TEXT,
		license TEXT,
		attribution_url TEXT,
		FOREIGN KEY (sentence_id) REFERENCES sentences(sentence_id)
	);
	CREATE TABLE links (
		sentence_id INTEGER,
		translation_id INTEGER,
		FOREIGN KEY (sentence_id) REFERENCES sentences(sentence_id),
		FOREIGN KEY (translation_id) REFERENCES sentences(sentence_id)
	);
	CREATE TABLE tags (
		sentence_id INTEGER,
		tag_name TEXT,
		FOREIGN KEY (sentence_id) REFERENCES sentences(sentence_id)
	);

	CREATE INDEX links_index ON links(sentence_id, translation_id);
	CREATE INDEX tags_index ON tags(sentence_id, tag_name);

	.separator "\t"
	.import csv/sentences.escaped_quotes.csv sentences
	.import csv/sentences_with_audio.uniq.csv sentences_with_audio
	.import csv/links.csv links
	.import csv/tags.escaped_quotes.csv tags
}

כדי להריץ אותו נפקוד ◊code{sqlite3 -init create_db.sql}.
צפו להמון אזהרות; מסד הנתונים של טטואבה כולל המון הפניות למשפטים שנמחקו כבר.



◊subsection{יצירת שאילתה מתאימה על הנתונים}

עכשיו כשיש לנו מסד נתונים עם כל המידע הדרוש נוכל להשתמש בו כדי לקבל את המידע שאנחנו רוצות.
רגע, מה המידע שאנחנו רוצות?
קובץ שבכל שורה שלו יש את הנתונים הבאים:

◊bullet-list{
	מספר המשפט במערכת של טטואבה.
	בעזרת המספר הזה אפשר לקשר מתוך הכרטיס באנקי אל העמוד המתאים בטטואבה באופן אוטומטי.


	הטקסט של המשפט בשפה שאנחנו לומדות.


	הפניה לקובץ של ההקלטה.


	התגים של המשפט.


	תרגומים של המשפט לשפות שאנחנו מכירות.
	בכוונה בחרתי שלא לכלול תרגום עקיף (כמו בדוגמה של יפנית ← עברית ← מאורית למעלה), גם כי זה מעט מורכב יותר למימוש ובעיקר בגלל שמשפטים בדרגת קרבה רחוקה יותר יכולים להיות שונים תחבירית ולקסיקלית באופן מטעה, כמו סוג של ◊wiki-he{טלפון שבור}.
}

לצורך זה כתבתי את הסקריפט הזה בפייתון.
אולי הוא לא הכי קצר ואלגנטי שאפשר◊;
◊numbered-note{
	אם יש לכן הצעות לשיפור ופִייתוּן, ◊link["https://me.digitalwords.net/"]{אשמח לשמוע}.
},
אבל הוא עובד טוב.

◊filebox-highlight["query.py" 'python]{
	#!/usr/bin/env python3

	import argparse
	import csv
	import os
	import sqlite3

	output_dir = 'output'
	native_langs = []


	def native_lang_columns():
		def native_lang_column(lang):
			return f"""
			"<ul class=""translations""><li>" ||
			(
					SELECT group_concat(sentences.text, "</li><li>")
					FROM links JOIN sentences
					ON
							links.translation_id = sentences.sentence_id
					WHERE
							links.sentence_id = target_sentences.sentence_id
							AND
							sentences.lang = '{lang}'
							)
			|| "</li></ul>"
			"""
		result = ""
		for lang in native_langs[:-1]:
			result += native_lang_column(lang) + ", "
		result += native_lang_column(native_langs[-1])
		return result


	def main():
		parser = argparse.ArgumentParser(
			description="Make a CSV files of sentences from the Tatoeba Project that have audio, along with their translations into selected languages")
		parser.add_argument("-t", "--target", type=str,
							help="target language",
							required=True)
		parser.add_argument("-n", "--native", type=str,
							help="native languages (space-delimited, within quotes)",
							required=True)
		parser.add_argument("-d", "--database", type=str,
							help="database file",
							default = "tatoeba.sqlite3")
		args = parser.parse_args()
		global native_langs
		native_langs = args.native.split(" ")

		conn = sqlite3.connect(args.database)
		c = conn.cursor()

		query = f"""
		SELECT
				target_sentences.sentence_id,
				target_sentences.text,
				"[sound:tatoeba_" || "{args.target}" || "_" || target_sentences.sentence_id || ".mp3]",
				"<ul class=""tags""><li>" ||
				(
					SELECT group_concat(tag_name, "</li><li>")
					FROM tags
					WHERE tags.sentence_id = target_sentences.sentence_id
				)
				|| "</li></ul>",
				{native_lang_columns()}
		FROM
				sentences AS target_sentences
		WHERE
				target_sentences.lang = "{args.target}" AND
				target_sentences.sentence_id IN (SELECT sentence_id FROM sentences_with_audio)
		;
		"""
		if not os.path.exists('output'):
			os.makedirs('output')
		with open(f'{os.path.join(output_dir, args.target)} → {args.native}.csv', 'w', newline='') as csvfile:
			out = csv.writer(csvfile, delimiter='\t',
					quotechar='|', quoting=csv.QUOTE_MINIMAL)
			for row in c.execute(query):
				out.writerow(row)

		conn.close()


	if __name__ == '__main__':
		main()
}

השימוש בסקריפט פשוט: ◊code{python query.py -t ◊textsc{target} -n ◊textsc{native}}, כש־◊code{◊textsc{target}} הוא קוד ◊L{◊wiki-en["Wikipedia:WikiProject_Languages/List_of_ISO_639-3_language_codes_(2019)"]{ISO 639-3}} שמצייג השפה שאנחנו רוצות ללמוד ו־◊code{◊textsc{native}} רשימה של השפות שאנחנו יודעות (תחומות במירכאות ישרות ומופרדות ברווח).
כך, לדוגמה, יצרתי לי את החפיסה שאני משתמש בה ללימוד פינית, עם תרגומים לשפות בהן אני שולט באופן החופשי ביותר (אנגלית, עברית, וולשית, נורווגית ואספרנטו):

◊highlight['sh]{
python query.py -t fin -n "eng heb cym nob epo"
}

אחרי שהסקריפט ירוץ נקבל בתוך תת־הספריה ◊L{output} קובץ ◊L{CSV} עם המשפטים שלנו.
אפשר כבר להתחיל לראות את הסוף…



◊subsection{הורדת קבצי הקול}

לפני שנרוץ לייבא את הנתונים האלה לאנקי, נרצה להוריד את קבצי הקול.
אין מאגר אחד להורדה, כך שנצטרך להוריד קובץ־קובץ.
למזלנו, יש לנו כלים אוטומטיים שיכולים לעשות את זה בשבילנו.

כתבתי את הסקריפט הבא, שמייצר רשימה של קבצי הקול, כל קובץ בשורה נפרדת:

◊filebox-highlight["audio_urls.py" 'python]{
	#!/usr/bin/env python3

	import argparse
	import csv
	import sqlite3


	def main():
		parser = argparse.ArgumentParser(
			description="Make a list of URLs of audio files for a specific language from the Tatoeba Project")
		parser.add_argument("-t", "--target", type=str,
							help="target language",
							required=True)
		parser.add_argument("-d", "--database", type=str,
							help="database file",
							default = "tatoeba.sqlite3")
		args = parser.parse_args()

		conn = sqlite3.connect(args.database)
		c = conn.cursor()

		query = f"""
	SELECT
		sentence_id
	FROM
		sentences
	WHERE
		lang = '{args.target}' AND
		sentence_id IN (SELECT sentence_id FROM sentences_with_audio)
		"""

		for row in c.execute(query):
			print("https://audio.tatoeba.org/sentences/" + args.target + "/" + str(row[0]) + ".mp3")


	if __name__ == '__main__':
		main()
}

נרצה לא רק להוריד את הקבצים בצורה מסודרת, אוטומטית ומהירה, אלא גם לתת להם קידומת ◊code{tatoeba_◊textsc{target}_} כדי שלא יהיה בלגאן באוסף קבצי המדיה של אנקי. כדי לעשות את זה נשתמש בסקריפט:

◊filebox-highlight["download_and_prepare_audio" 'sh]{
	#!/usr/bin/sh

	mkdir -p output/audio

	# Source: https://stackoverflow.com/a/11850469
	./audio_urls.py -t $1 -d $2 | xargs -n 1 -P 2 wget --directory-prefix=output/audio/ --continue

	for f in output/audio/*; do
		mv "$f" "$(echo $f | sed 's/^output\/audio\//output\/audio\/tatoeba_'$1'_/g')";
	done
}

ההורדה היא של שני קבצים במקביל, כך שבזמן שמחכים לבקשה של קובץ אחד השני ממשיך לרדת ולא מחכים בצוואר בקבוק.
למה רק שניים? כי ◊wiki-he["תפוקה שולית פוחתת"]{התפוקה פוחתת} ככל שמוסיפים עוד הורדות במקביל, כי חבל סתם להעמיס על השרתים של טטואבה וכי הורדה של יותר מדי קבצים במקביל יכולה להסתיים בזה שההורדה של חלקם תשתבש (מנסיון…).



◊subsection{יצירת סוג רשומה מתאים}

עכשיו נרצה שתהיה לנו תבנית של רשומות כדי שנוכל לייבא את הקבצים אל אנקי.
כדי ליצור תבנית כזאת נבחר ◊L{Tools → Manage Note Types} מהתפריט ושם נלחץ על ◊L{Add}, נוסיף תבנית בסיסית ונקרא לה בשם.
נחפש אותה ברשימת התבניות ונלחץ על ◊L{Fields}.
נוסיף את השדות שאנחנו צריכות (אפשר לבחור גם שמות אחרים, כמובן, וסדר אחר):

◊bullet-list{
	◊code{sentence_id}, בשביל מספר המשפט במאגר של טטואבה.


	שדה בשביל השפה שאנחנו לומדות.
	לי נוח פשוט לקרוא לשדה בשם מ־◊L{◊link["https://en.wikipedia.org/wiki/Wikipedia:WikiProject_Languages/List_of_ISO_639-3_language_codes_(2019)"]{ISO 639-3}}.


	◊code{audio}, שיפנה לקובץ השמע.


	◊code{tags}, בשביל התגים מטטואבה.
	יש לאנקי שדה פנימי של תגים, שמאפשר לחפש לפיהם.
	השדה כאן מיועד להצגה על גבי הכרטיסים.


	שדה עבור כל אחת מהשפות שאנחנו יודעות כבר.


	אם אתן רוצות להשאיר את השדות עם המשפטים בלי שינוי ולכתוב הערות שלכן בשדות נפרדים, אפשר ליצור שדות שמיועדים להערות.


	שדות בשביל התוסף ◊link["https://ankiweb.net/shared/info/900801631"]{◊L{MorphMan}} שמסדר את הכרטיסים בסדר ◊link["https://github.com/kaegi/MorphMan/wiki/I-plus-1"]{◊L{i+1}}, כדי שהוא יוכל לשמור נתונים על כל רשומה:
	◊code{MorphMan_FocusMorph},
	◊code{MorphMan_Index},
	◊code{MorphMan_Unmatures},
	◊code{MorphMan_UnmatureMorphCount},
	◊code{MorphMan_Unknowns},
	◊code{MorphMan_UnknownMorphCount},
	◊code{MorphMan_UnknownFreq}.
	על השמות האלה נשמור כמות שהם.
}

אחרי שהוספנו את השדות החדשים נוכל למחוק את ה־◊code{Front} וה־◊code{Back} שמגיעים ביחד עם התבנית הבסיסית ולסגור את החלון של השדות.

נלחץ על ◊L{Cards} ויפתח לנו חלון כזה:

◊figure["cards.png"]{חלון עריכת הכרטיסים.}

בראש החלון נוכל לבחור בין סוגי הכרטיסים שנרצה ליצור מכל רשומה.
אני ממליץ על שלושה כיוונים לפי הסדר הזה:

◊bullet-list{
	הבנת הנקרא, כלומר: בצד הקדמי (השאלה) יופיע המשפט הכתוב שאנחנו לומדות ובאחורי (התשובה) השמע והתרגומים.


	הבנת הנשמע, כלומר: בצד הקדמי תושמע ההקלטה ובאחורי המלל הכתוב והתרגומים.


	הפקה אקטיבית, כלומר: בצד הקדמי יופיעו התרגומים ובאחורי המשפט בשפה שאנחנו לומדות, כתוב ומושמע.
}

הסדר הזה הוא, מנסיוני, הסדר הכי טוב ללמוד בו.

נתחיל בסידור הכיוון הראשון.
בצד שמאל למעלה (◊L{Front Template}) נכתוב

◊highlight['html]{
	<p>{{TARGET}}</p>
}

כשאת ◊code{◊textsc{target}} נחליף בשם של השדה של הטקסט בשפה הנלמדת.
אם תרצו לחפש מילים במילון בלחיצת כפתור קראו את ◊xref["anki/lookup"]{הדף הזה} וכתבו במקום השורה הזו את זו:

◊highlight['html]{
	<p id="lookup">{{TARGET}}</p>
}

למטה (◊L{Back Template}) נוסיף את השדה של התגים, של השמע ואת השדות של השפות שאנחנו יודעות, כך לדוגמה:

◊highlight['html]{
	<p id="tags">{{tags}}</p>

	<p class="hebrew">{{heb}}</p>
	<p>{{eng}}</p>
	<p>{{nob}}</p>
	<p>{{cym}}</p>
	<p>{{epo}}</p>

	<p>{{audio}}</p>

	<p id="tatoeba"><a href="https://tatoeba.org/eng/sentences/show/{{sentence_id}}"><img src="_tatoeba.svg" /></a></p>
}

השורה האחרונה מקשרת לדף של המשפט בטטואבה.

התגים והעברית מוגדרים בעיצוב מיוחד: את התגים נרצה להציג פחות בולטים ואת העברית בכיוון ימין←שמאל ולא שמאל←ימין.
נגדיר את העיצוב בחלק האמצעי (◊L{Styling}) כך לדוגמה, ב־◊L{◊wiki-he{CSS}}:

◊highlight['css]{
	#tags {
	    font-size: small;
	}

	.hebrew {
	    direction: rtl;
	}
}

בנוסף, אם נרצה שהתרגומים השונים לאותה השפה יופיעו בצורה יפה, אחד לצד השני, ולא אחד מתחת לשני ברשימה עם ◊wiki-he["תבליט (פיסוק)"]{תבליטים} (•), נוכל להוסיף את המוג׳ו הבא לחלון העיצוב:

◊highlight['css]{
	.translations {
	    list-style-type: none;
	    margin: 0;
	    padding: 0;
	}

	.translations li {
	    display: inline;
	}
	.translations li:after {
	    content: " · "
	}
	.translations li:last-child:after {
	    content: ""
	}
}

עכשיו נעבור ליצירת סוג הכרטיסים הבא על ידי לחיצה על הכפתור ◊L{Options} שבפינה הימנית העליונה ובחירה ב־◊L{Add Card Type}.
בדומה, הפעם נציב בחלק הקדמי את השדה של השמע ובאחורי את המלל הכתוב, התגים והשפות האחרות.
לבסוף ניצור את הכיוון של ההפקה האקטיבית.



◊subsection{ייבוא אל אנקי}

עכשיו אנחנו בשלות לשלב הכמעט־אחרון: אחרי שיש לנו גם קובץ ◊L{CSV} מוכן ותבנית עבורו וגם קבצי שמע שהורדנו נוכל לייבא אותם אל אנקי.

נתחיל בקובץ ה־◊L{CSV}.
מהחלון הראשי של אנקי נבחר ◊L{File → Import} וניגש את המקום שבו שמרנו את הקובץ.
נבחר בסוג הרשומות שיצרנו, ובחפיסה המתאימה (נוכל להוסיף אחת אם אין כזו עדיין).
חשוב ש־◊L{Allow HTML in fields}.
עכשיו נמפה את השדות בקובץ לשדות המתאימים בסוג הרשומות שלנו.
הראשון הוא מספר המשפט, השני הטקסט בשפה הנלמדת, השלישי השמע, הרביעי התגים, והחמישי ואילך הם של השפות שנרצה לראות את התרגומים בהן, לפי אותו סדר שהזנו לסקריפט לפני עידן ועידנים.
נייבא בעזרת לחיצה על ◊L{Import}.

עכשיו נעתיק את כל קבצי ה־◊L{MP3} לספריית המדיה של אנקי.
המיקום שלה תלוי במערכת ההפעלה בה אתן משתמשות.
פירוט עבור לינוקס, מקינטוש וחלונות נמצא ב◊link["https://apps.ankiweb.net/docs/manual.html#file-locations"]{סעיף המתאים במדריך}.

נוריד גם את ◊link["https://tatoeba.org/img/tatoeba.svg"]{הלוגו של טטואבה} לאותה הספריה ונשנה את שם הקובץ ל־◊code{_tatoeba.svg}.

זהו.
עכשיו אפשר לבדוק בעזרת האפשרות ◊L{Browse} ולראות שהכל עובד כראוי בלחיצה על ◊L{Preview}.
הסדר של הכרטיסים הוא זה שהופיע במסד הנתונים, ועכשיו נרצה לשנות אותו.



◊subsection{סידור הכרטיסים}

כדי לעשות את זה נעזר בתוסף לאנקי בשם ◊L{◊link["https://ankiweb.net/shared/info/900801631"]{MorphMan}}.
אני לא רואה טעם לכתוב כאן מדריך עבור ◊L{MorphMan}; יש הסברים ברורים ב◊link["https://github.com/kaegi/MorphMan/wiki"]{וויקי} וב◊link["https://www.youtube.com/results?search_query=MorphMan+Anki"]{סרטונים ביוטיוב}.

בגדול מה שתצטרכו לעשות הוא:

◊bullet-list{
	להתקין את התוסף בעזרת ◊L{Tools → Add-ons}, לחיצה על ◊L{Get Add-ons} והקלדת המספר של התוסף — 900801631 — בחלון שיפתח.
	כדי להשתמש בתוסף תצטרכו לסגור ולפתוח מחדש את אנקי.


	להוסיף ולהפעיל את סוג הכרטיסים שלנו במסך ההגדרות של התוסף (◊L{Tools → MorphMan Preferences}) ואז להורות על חישוב מחדש של סדר הכרטיסים (◊L{Tools → MorphMan Recalc}).
}

זהו זה!
עכשיו הכל מוכן והגיע הזמן להתחיל ללמוד בשצף, פינית, לטינית, ◊wiki-he["שפות ברבריות"]{ברברית}, יפנית או מה שבא לכן!

אתן מפיקות תועלת מטטואבה?
שווה לשקול לעזור לפרוייקט החמוד והמעולה הזה:
דרך אחת היא ◊link["https://tatoeba.org/eng/users/register"]{להצטרף} ולתרום משפטים, תרגומים והקלטות (ר׳ ◊link["https://en.wiki.tatoeba.org/articles/show/quick-start"]{מדריך});
דרך אחרת היא ◊link["https://tatoeba.org/eng/donate"]{לתרום} תרומה כספית.



◊section[#:label "sec:precompiled"]{אחרית דבר: חפיסות מוכנות}

התהליך שאני מתאר כאן אמנם עושה את העבודה בצורה טובה ואוטומטית, אבל דורש ידע ומצריך גם עבודה ידנית.
זאת לא משימה שעושים באופן תכוף מספיק כדי להצדיק כתיבה של כלי אחת מהוקצה שיעשה את הכל באופן אוטומטי לגמרי, בלי טרחה ידנית.

כדי שכמה שיותר לומדות של שפות שונות — כולל שפות שאישית אני לא לומד — יוכלו להנות מהתוצרים של התהליך הפקתי חפיסות מוכנות עבור כל השפות שיש עבורן הקלטות במאגר.



◊subsection{רשימת השפות שיש להן הקלטות}

בשלב ראשון נרצה לדעת לאיזה שפות יש בכלל הקלטות וכמה.
אפשר ◊link["https://tatoeba.org/eng/audio/index/"]{להסתכל ולבדוק באתר} כמו בני־תמותה פשוטים, אבל כשיש לנו ◊L{SQL} בידיים כל דבר נראה כמו שאילתה:

◊filebox-highlight["languages_with_audio.sql" 'sql]{
	.open tatoeba.sqlite3

	SELECT lang, COUNT (sentences.sentence_id) AS audio_sentences_no
	FROM sentences_with_audio JOIN sentences
	ON sentences_with_audio.sentence_id = sentences.sentence_id
	GROUP BY lang
	ORDER BY audio_sentences_no DESC;
}

מבט חטוף בתוצאה המתקבלת מגלה תמונה מעניינת ומאוד לא מאוזנת.
טטואבה הוא מיזם שיתופי, מה שאומר שאם יש מי שיטרחו בשביל שפה מסויימת יהיו בה חומרים, ואם אין אז לא.
בדיוק מהסיבה הזאת ל◊wiki-he["שפות ברבריות"]{שפות בֶּרְבֶּרְיוֹת} יש יצוג עצום (במקום השלישי, אחרי אנגלית וספרדית, נמצאת ◊L{◊link["https://tatoeba.org/eng/audio/index/kab"]{Kabyle}} וכמה מקומות מתחתיה ◊L{◊link["https://tatoeba.org/eng/audio/index/ber"]{Berber}}), בעוד שאין הקלטה אפילו למשפט בודד באיטלקית למרות שזו השפה השניה מבחינת מספר המשפטים הכתובים…

מסקנה שניה היא שאין טעם לנסות להפיק חפיסות עבור אנגלית וספרדית, כי היו יוצאות חפיסות כבדות מדי◊;
◊numbered-note{
	כ־6 ג׳יגה ו־1 ג׳יגה בהתאמה, לפי ההערכה שלי שמתבססת על המשקל של משפט ממוצע בפינית.
}
שדורשות זמן בלתי סביר כדי ללמוד אותן (במקרה של אנגלית, יותר מתקופת חיים אחת…), ובכל מקרה יש כבר די והותר חומרים ללימוד השפות האימפריאליסטיות האלה בנמצא.



◊subsection{בחירת השפות לתרגום}

יש צמדים של שפת מקור ושפת תרגום שהם יותר נפוצים וכאלה שפחות.
זה קשור לסיבות חברתיות־גיאוגרפיות־היסטוריות, אבל גם זה גם יכול להיות תוצאה של מישהי נמרצת מאוד שטרחה ותרגמה הרבה משפטים משפה אחת לאחרת.
בפועל, לא משנה מה הסיבה, נרצה שהחלק של התרגומים בחפיסות יהיה כמה שיותר שלם.
לכן, עבור כל שפה צירפתי תרגומים בחמשת השפות שיש הכי הרבה משפטים קוֹ־מתורגמים איתן.
כדי לדעת מה הן כתבתי את השאילתה הזאת:

◊filebox-highlight["statistics_for_translated_audio_sentences.sql" 'sql]{
.open tatoeba.sqlite3

SELECT lang, COUNT (sentences.sentence_id) AS counter
FROM sentences JOIN links
ON sentences.sentence_id = links.translation_id
WHERE
	links.sentence_id IN
	(
		SELECT sentence_id
		FROM sentences
		WHERE
			lang = TARGET
			AND
			sentence_id in (SELECT sentence_id FROM sentences_with_audio)
	)
GROUP BY lang
ORDER BY counter DESC
LIMIT 5;
}



◊subsection[#:label "sec:precompiled:download"]{הורדת החפיסות}

את התוצאה של כל התהליך העלתי ל◊link["https://ankiweb.net/shared/decks/"]{מאגר החפיסות המוכנות של ◊L{AnkiWeb}}‎◊;
◊numbered-note{
	◊link["https://ankiweb.net/shared/byauthor/139315945"]{קישור לכלל החפיסות שהעלתי}.
}
בחרתי בפורמט אחיד לשם של החפיסות, במגבלת 60 התווים הנתונה:

◊highlight['txt]{
	All LANGUAGE sentences with recorded audio from Tatoeba
}

להלן רשימת החפיסות.
אם החפיסות שימושיות עבורכן, בבקשה השאירו פידבק חיובי ב־◊L{AnkiWeb}: זה גם יהיה נחמד עבורי, וגם יעזור לאחרות למצוא את החפיסות.

לצרכי גיבוי מופיעה גם אופציה להורדה מהשרת של „מילים דיגיטליות”, ומסומנת ב־⭳.
אני מעדיף שתורידו מ־◊L{AnkiWeb} (סימון: ◊img[#:src "ankiweb.png" #:style "width: 1em; height: auto;"]{}); חפיסות שלא זוכות למספיק הורדות מוסרות מהמאגר, ולא הייתי רוצה להכשיל את עצמי.

הערה זמנית: ניתן לשתף ב־◊L{AnkiWeb} רק 10 חפיסות בשבוע.
לכן לחלק מהחפיסות אין עדיין קישורים.
בקרוב אוכל לסיים לשתף את הכל.



◊(define (deck iso target native_a native_a_no native_b native_b_no native_c native_c_no native_d native_d_no native_e native_e_no sentences date ankiweb)
	`(tr
		(td (span ((class "no_of_sentences")) ,sentences))
		(td (b ,target))
		(td ,(if (equal? native_a "") "" "→"))
		(td ,native_a " " (span ((class "no_of_sentences")) ,native_a_no))
		(td ,native_b " " (span ((class "no_of_sentences")) ,native_b_no))
		(td ,native_c " " (span ((class "no_of_sentences")) ,native_c_no))
		(td ,native_d " " (span ((class "no_of_sentences")) ,native_d_no))
		(td ,native_e " " (span ((class "no_of_sentences")) ,native_e_no))
		(td ,date)
		(td ,(if (equal? ankiweb "") "" `(a ((href  ,(string-append "https://ankiweb.net/shared/info/" ankiweb)) (class "barelink")) (img ((src "ankiweb.png"))))))
		(td (a ((href  ,(string-append "https://media.digitalwords.net/anki/tatoeba-audio/" iso ".apkg")) (class "barelink")) "⭳"))
	 )
 )

◊style{
	.decktable {
		font-size: 1.2rem;
		direction: ltr;
	}
	.decktable td {
		vertical-align: top;
	}
	.no_of_sentences {
		color: ◊|col-mouse|;
	}
	.decktable img {
		width: 1em;
		height: auto;
		max-width: none;
	}
}
◊table[#:class "decktable fullwidth"]{
	◊;◊deck["" "" "English" "()" "" "()" "" "()" "" "()" "" "()" "" "2019/10/12" ""]
	◊deck["deu-1" "German 1/2" "English" "(18733)" "Esperanto" "(9873)" "French" "(8322)" "Russian" "(7717)" "Spanish" "(6917)" "19582" "2019/10/12" "139315945"]
	◊deck["deu-2" "German 2/2" "" "" "" "" "" "" "" "" "" "" "" "" "1961742728"]
	◊deck["por" "Portuguese" "English" "(10163)" "Spanish" "(2282)" "Esperanto" "(1704)" "French" "(1080)" "Russian" "(653)" "11019" "2019/10/12" "1859535970"]
	◊deck["fra" "French" "English" "(7956)" "Esperanto" "(6280)" "Russian" "(5051)" "German" "(2921)" "Ukrainian" "(2204)" "8181" "2019/10/12" "1373615545"]
	◊deck["hun" "Hungarian" "English" "(6048)" "German" "(1263)" "Esperanto" "(643)" "Italian" "(529)" "French" "(350)" "6720" "2019/10/12" "1691262801"]
	◊deck["rus" "Russian" "English" "(3294)" "Japanese" "(1874)" "French" "(1662)" "German" "(1268)" "Ukrainian" "(1246)" "4690" "2019/10/12" "604511069"]
	◊deck["ber" "Berber" "English" "(4494)" "Spanish" "(320)" "French" "(315)" "Kabyle" "(60)" "Arabic" "(46)" "4598" "2019/10/12" "342441168"]
	◊deck["epo" "Esperanto" "English" "(3901)" "French" "(1340)" "German" "(1172)" "Dutch" "(748)" "Spanish" "(570)" "4601" "2019/10/12" "1832773631"]
	◊deck["fin" "Finnish" "English" "(4017)" "Russian" "(1174)" "Spanish" "(1087)" "Italian" "(766)" "Japanese" "(286)" "4057" "2019/10/12" "702922008"]
	◊deck["wuu" "Wu Chinese" "Mandarin" "(2489)" "French" "(633)" "English" "(427)" "Spanish" "(34)" "Yue Chinese" "(32)" "2491" "2019/10/12" "949774152"]
	◊deck["nld" "Dutch" "Esperanto" "(1935)" "English" "(1698)" "Ukrainian" "(1460)" "German" "(1321)" "Spanish" "(1231)" "1961" "2019/10/12" "742594829"]
	◊deck["cmn" "Mandarin Chinese" "French" "(1317)" "German" "(1280)" "English" "(1260)" "Wu Chinese" "(700)" "Spanish" "(605)" "1678" "2019/10/12" "110314713"]
	◊deck["jpn" "Japanese" "English" "(1278)" "Russian" "(1249)" "Finnish" "(1050)" "German" "(1025)" "French" "(658)" "1283" "2019/10/12" "1823020002"]
	◊deck["heb" "Hebrew" "English" "(1086)" "Esperanto" "(125)" "Polish" "(120)" "Russian" "(89)" "French" "(83)" "1086" "2019/10/12" "2086474061"]
	◊deck["lat" "Latin" "English" "(974)" "Portugeuse" "(430)" "Spanish" "(375)" "French" "(293)" "Esperanto" "(199)" "1067" "2019/10/12" "1699554124"]
	◊deck["dtp" "Central Dusun" "English" "(401)" "Japanese" "(45)" "Coastal Kadazan" "(31)" "" "" "" "" "480" "2019/10/12" "392451841"]
	◊deck["mar" "Marathi" "English" "(376)" "Hindi" "(142)" "" "" "" "" "" "" "376" "2019/10/12" "708121003"]
	◊deck["ukr" "Ukrainian" "English" "(363)" "French" "(28)" "German" "(20)" "Italian" "(17)" "Spanish" "(15)" "363" "2019/10/12" "1520102967"]
	◊deck["pol" "Polish" "English" "(222)" "Dutch" "(98)" "German" "(30)" "Ukrainian" "(27)" "Russian" "(21)" "224" "2019/10/12" "1086608121"]
	◊deck["tha" "Thai" "English" "(87)" "Esperanto" "(39)" "German" "(38)" "French" "(38)" "Russian" "(32)" "134" "2019/10/12" "1315022992"]
	◊deck["cat" "Catalan" "English" "(111)" "Spanish" "(41)" "Ukrainian" "(31)" "French" "(19)" "Esperanto" "(17)" "112" "2019/10/12" ""]
	◊deck["cbk" "Chavacano" "English" "(53)" "" "" "" "" "" "" "" "" "60" "2019/10/12" ""]
	◊deck["ron" "Romanian" "English" "(51)" "Esperanto" "(51)" "Dutch" "(50)" "German" "(34)" "Spanish" "(34)" "53" "2019/10/12" ""]
	◊deck["tur" "Turkish" "English" "(37)" "German" "(13)" "Esperanto" "(8)" "Spanish" "(5)" "Swedish" "(3)" "37" "2019/10/12" ""]
	◊deck["nst" "Naga (Tangshang)" "English" "(28)" "" "" "" "" "" "" "" "" "28" "2019/10/12" ""]
}
