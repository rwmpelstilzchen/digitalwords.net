#lang pollen

◊define-meta[title]{חפיסות של לנגוויג׳פוד}
◊define-meta[publish-date]{2020-10-25}



◊margin-figure["tuki-lo-panda.png"]{
	אם יש סמל שמתאים לשילוב של פיראטיות ולשון/דיבור זה תוכי.
	◊link["https://www.pinclipart.com/maxpin/iTxwJmx/"]{התוכי שבתמונה} הוא קרוב של ◊link["https://youtu.be/U7JF3Ik-fDM"]{תוכי פנדה}.
}
אחת התכונות שהופכות את אנקי לתוכנה שימושית היא הקהילה שהתפתחה סביבה.
נקודת כינוס אחת לקהילה הזאת היא ◊link["https://ankiweb.net/shared/decks/"]{מאגר החפיסות המוכנות של ◊L{AnkiWeb}}, שבו כל אחת/ד יכול/ה לחלוק חפיסות בקלות.
אם בוררים החוצה את החפיסות המיותרות, יש שם אוצרות נפלאים, חלקם מלוטשים יותר וחלקם פחות.

באיזשהו שלב, כשחיפשתי חפיסה ללימוד פרסית, הגעתי אל חפיסה שיצר/ה מישהי/ו◊;
◊numbered-note{
	אני לא יודע מי את/ה, אבל תודה 🌼
}
באופן אוטומטי ממאגר המילים והצירופים של לנגוויג׳פוד.
אמנם ללמוד מילים או צירופים קצרים ללא הקשר זאת לה הדרך היעילה ביותר, אבל יש שפות שבהעדר חומרים אחרים זמינים בפורמט של אנקי זה עדיף על כלום.
היה ברור לי שהחפיסה הזאת ושאר החפיסות מהסדרה לא ישארו באתר עוד הרבה זמן, ולכן הורדתי אותן למחשב.
בתור פיראט טוב, הנה הן לפניכן/ם, אותם הקבצים כמו שהורדתי אותם, ללא עיבוד:

◊(define (lp file . name) `(li (a ((href ,(string-append "https://media.digitalwords.net/anki/lp/" file))) ,@name)))
◊blockquote[#:class "textwidthpar"]{
	◊ul[#:class "comma-list"]{
		◊lp["Italian.apkg"]{איטלקית}
		◊lp["Indonesian.apkg"]{אינדונזית}
		◊lp["Afrikaans.apkg"]{אפריקאנס}
		◊lp["Bulgarian.apkg"]{בולגרית}
		◊lp["German.apkg"]{גרמנית}
		◊lp["Danish.apkg"]{דנית}
		◊lp["Dutch.apkg"]{הולדנית}
		◊lp["Hungarian.apkg"]{הונגרית}
		◊lp["Hindi.apkg"]{הינדי}
		◊lp["Vietnamese.apkg"]{ויאטנמית}
		◊lp["Greek.apkg"]{יוונית}
		◊lp["Japanese.apkg"]{יפנית}
		◊lp["Chinese.apkg"]{מנדרינית}
		◊lp["Norwegian.apkg"]{נורווגית}
		◊lp["Swahili.apkg"]{סווהילי}
		◊lp["Hebrew.apkg"]{עברית}
		◊lp["Arabic.apkg"]{ערבית}
		◊lp["Polish.apkg"]{פולנית}
		◊lp["Portuguese.apkg"]{פורטוגלית}
		◊lp["Filipino-1.apkg"]{פיליפינית א׳}
		◊lp["Filipino-2.apkg"]{פיליפינית ב׳}
		◊lp["Finnish.apkg"]{פינית}
		◊lp["Persian.apkg"]{פרסית}
		◊lp["French.apkg"]{צרפתית}
		◊lp["Czech.apkg"]{צ׳כית}
		◊lp["Korean.apkg"]{קוריאנית}
		◊lp["Cantonese.apkg"]{קנטונית}
		◊lp["Romanian.apkg"]{רומנית}
		◊lp["Thai.apkg"]{תאי}
		◊lp["Turkish.apkg"]{תורכית}
	}
}
