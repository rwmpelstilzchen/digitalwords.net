#lang pollen

◊define-meta[title]{טעימה מאספרנטו: מפגשים בזום}
◊define-meta[publish-date]{2020-04-05}
◊define-meta[last-update]{2020-06-24}
◊define-meta[multigender]{true}

◊margin-figure["esperanto.png"]{}
אספרנטו היא שפה מיוחדת במינה.
היא השפה המתוכננת היחידה שזכתה לקהל ◊gender["דוברות" "דוברים"]{} של ממש, שבאמת משתמש בשפה ככלי תקשורתי והבעתי ביום־יום, וללמעלה מאלף ◊gender["דוברות ילידיות" "דוברים ילידיים"]{}.
לפי ◊wiki["eo" "Esperanto#Parolantaro"]{הערכות}, ◊gender["דוברות" "דוברים"]{} את השפה מספר בן שש או שבע ספרות של א.נשים, שזה יותר ממספר ◊gender["דוברות" "דוברים"]{} של הרבה שפות טבעיות קטנות בעולם.
אמת, התקווה המקורית של יוצר השפה — שהיא תהווה שפה מתווכת שניה כלל־עולמית — לא התממשה (עד כה!…), אבל זה עדיין נתון מרשים שמעלה כמה שאלות:

◊bullet-list{
	למה הא.נשים האלה בחרו ללמוד אספרנטו?


	כל מי ש◊gender["למדה" "למד"]{} שפה שניה ◊gender["יודעת" "יודע"]{} שלימוד שפה זאת לא מלאכה פשוטה, אבל ◊gender["דוברות" "דוברי"]{} אספרנטו ◊gender["רבות מעידות" "רבים מעידים"]{} שאספרנטו קל ללמוד; מה בתכונות של המבנה של השפה הזאת מסייע לכך?


	ברמה האישית: איך זה מרגיש ללמוד שפה קלה אחרי שרובנו למדנו שפה „קשה” אחת או יותר?
}

בסדרה של מפגשי זום נענה על שאלות אלה ואחרות ונטעם מעט מ„החוויה האספרנטיסטית” באמצעות לימוד של השפה וקריאת טקסטים באספרנטו.
הלימוד ילווה בחוברת המינימליסטית ◊link["https://xpr.digitalwords.net/katalogo/esperanto/"]{„טעימה מאספרנטו”} שכתבתי (◊link["https://xpr.digitalwords.net/katalogo/esperanto/"]{הורדה}).

בעבר כבר לימדתי „שיעורי בָּזָק” של אספרנטו.
אני יודע מנסיון שאפשר ללמד אספרנטו במהירות, שזה כיף ושזה עובד בצורה טובה עוד יותר ממה שציפיתי מלכתחילה.



◊nosection{מתי המפגשים מתקיימים, למי הם מיועדים ואיך להצטרף?}

◊; הפגישה הראשונה תתקיים ביום שישי ה־10 באפריל 2020 משעה 12:00 עד 13:30.
◊; מספר הזום שלה הוא ◊tt{198977198}, וניתן להצטרף גם דרך הקישור הזה: ◊link["https://zoom.us/j/198977198"]{◊tt{https://zoom.us/j/198977198}}.

◊;המפגש השני יתקיים ביום שישי ה־17 באפריל 2020 משעה 12:00 עד 13:30.
◊;המפגש השלישי יתקיים ביום שישי ה־24 באפריל 2020 משעה 12:00 עד 13:30.
◊;המפגש הרביעי יתקיים ביום שישי ה־1 במאי 2020 🚩 משעה 12:00 עד 13:30.
◊;המפגש החמישי יתקיים ביום שישי ה־8 במאי 2020 משעה 12:00 עד 13:30.
◊;המפגשים מתקיימים בימי שישי משעה 12:00 עד 13:30; התאריכים הבאים לעתיד הקרוב: ה־15 במאי 2020, ה־22 במאי וה־29 במאי.
המפגשים מתקיימים בימי שישי משעה 9:00 עד 10:30.
התאריכים הבאים לעתיד הקרוב: ה־5 ביוני 2020, ה־26 ביוני, וה־10 ביולי.
קוד הפגישה בזום הוא תמיד ◊link["https://zoom.us/j/9843881536"]{◊code{9843881536}}.
אם לא התקנת זום, ◊gender["תצטרכי" "תצטרך"]{} ◊link["https://zoom.us/"]{להוריד ולהתקין} לפני המפגש◊;
◊numbered-note{
	אם אין לך נסיון בשימוש בתוכנה, צרי/צור איתי קשר ונוודא שהכל עובד כשורה מעט לפני המפגש ביום שישי.
}.

בשלב זה, אחרי שעברנו על כל עיקרי הדקדוק, אנחנו ◊gender["מתמקדות" "מתמקדים"]{} בקריאה.  
אם אתרע מזלך ולא השתתפת במפגשים עד עכשיו, לא נורא!
◊gender["תוכלי" "תוכל"]{} להשלים את החומר ולהצטרף ישר למפגש הקרוב.
◊gender["פני" "פנה"]{} אלי ואשלח הקלטות של מפגשים קודמים.
אפשר גם לעבור על ◊link["https://xpr.digitalwords.net/katalogo/esperanto"]{החוברת} או ללמוד ממקור אחר◊numbered-note{הפניות למקורות לימוד נוספים מופיעים בסוף החוברת.}, להצטרף ולהשלים תוך כדי נקודות שאינן ברורות די צָרכן.

הטקסט אותו אנחנו ◊gender["קוראות" "קוראים"]{} כעת הוא הסיפור ◊link["https://egalite.hu/baghy/migranta.htm"]{◊L{Sinjoro Melonkapo}} „אדון ראש מלון” מאת ◊link["https://egalite.hu/baghy/"]{◊L{Julio Baghy}}.
הטקסט: ◊link["https://egalite.hu/baghy/migranta/sinjoro%20melonkapo.htm"]{בפורמט ◊L{HTML}}, ◊xref-local["densa.pdf"]{בפורמט ◊L{PDF} דחוס}, ◊xref-local["maldensa.pdf"]{בפורמט ◊L{PDF} מרווח} (מתאים לכתיבת ◊wiki-en["Gloss (annotation)"]{גלוסות}) ו◊link["https://bbl.digitalwords.net/?page_id=5#archive"]{בתרגום לעברית} מאת שאול שמר.

למי המפגשים מיועדים?
קו מנחה שניסיתי מההתחלה ליצור הוא שהמפגשים יהיו נעימים, מקבלים ואינקלוסיביים; בהסתכלות לאחור אני חושב שהצלחתי במטרה הזאת.
בקבוצה שהתגבשה לה יש גם ◊gender["מתחילות" "מתחילים"]{} וגם ◊gender["אספרנטיסיות ותיקות" "אספרנטיסטים ותיקים"]{}.
בשלב זה, כשעיקר המפגשים הוא כבר קריאת טקסטים, כן דרושה היכרות בסיסית עם מבנה השפה, אבל אין שום צורך או ציפיה לשליטה שוטפת בשפה: אם עברת על הדקדוק ויש לך מושג כללי איך בנויה השפה, מקומך איתנו.
מהצד השני, גם ל◊gender["אספרנטיסטיות" "אספרנטיסטים"]{} עם רקע ונסיון רב יש מקום: תוך כדי קריאת הטקסטים אנחנו ◊gender["מדברות" "מדברים"]{} על סוגיות בלשניות רלוונטיות, בהסתכלות רחבה ובהשוואה עם שפות אחרות.
בקיצור — לא משנה מה הרקע שלך, ◊gender["את מוזמנת" "אתה מוזמן"]{} ואני מקווה שיהיה לך מעניין 🙂

בכל עניין אפשר ליצור איתי קשר באחת הדרכים שמופיעות ◊link["http://me.digitalwords.net/"]{כאן}.

בנוסף למפגשים שאני מארגן, שסובבים סביב קריאה ודיון, מתקיימים גם מפגשים בזום של ◊link["https://esperanto.org.il/"]{האגודה לאספרנטו בישראל}, בימי שני ב־18:15 (מספר הפגישה: ◊link["https://huji.zoom.us/j/7508982919"]{◊code{7508982919}}).
יכול להיות שתמצאו עניין גם ב◊link["http://www.tapuz.co.il/forums/forumpage/181/"]{פורום אספרנטו בתפוז}‏◊;
◊numbered-note{
	יש לי הרבה סימפתיה וזכרונות יפים למדיום של פורומים (עוד מימי ◊L{IOL} ו◊link["https://web.archive.org/web/20040630232744/http://forums.ort.org.il/"]{אורט}).
	הפורום הזה הוא אחד האחרונים שאני מכיר שממשיך לחיות.
	נכון, הוא ידע ימים פעילים הרבה יותר, עם הרבה יותר ◊gender["משתתפות" "משתתפים"]{} והרבה יותר פתילים ותתי־פתילים, אבל עדיין הוא הרבה יותר פעיל משאר הפורומים, שבעיקר מעלים אבק.
}.

Ĝis,◊br{}
יודה
