#lang pollen

◊(require css-tools/font-face)

◊(ffd/rp "Rac" "/fonts/rac/regular.otf" 
#:font-style "normal" #:font-weight "normal" #:base64 #f)

◊(ffd/rp "Rac" "/fonts/rac/bold.otf" 
#:font-style "normal" #:font-weight "bold" #:base64 #f)

◊; ◊(ffd/rp "Rac" "/fonts/rac/Guttman_Adii.ttf" 
◊; #:font-style "italic" #:font-weight "normal" #:base64 #f)

◊(ffd/rp "Rac" "/fonts/rac/DaysNights.ttf" 
#:font-style "italic" #:font-weight "normal" #:base64 #f)
