#lang pollen

/*
Based on:
	https://carlschwan.eu/2020/12/29/adding-comments-to-your-static-blog-with-mastodon/
	https://berglyd.net/blog/2023/03/mastodon-comments/
Contains some unused bits, but works.
*/

:root {
/*
*   Card style
*/
    --card-background: #fff;
    --card-background-selected: #eaeaea;

    --card-text-color-main: #000;
    --card-text-color-secondary: #747474;
    --card-text-color-tertiary: #bababa;
    --card-separator-color: rgba(218, 218, 218, 0.5);

    --card-border-radius: 10px;

    --card-padding: 30px;
    @media (max-width: $on-desktop-large) {
        --card-padding: 25px;
    }
    @media (max-width: $on-tablet) {
        --card-padding: 20px;
    }

    --small-card-padding: 25px;
    @media (max-width: $on-tablet) {
        --small-card-padding: 25px 20px;
    }

    @media (prefers-color-scheme: dark) {
        --card-background: #424242;
        --card-background-selected: rgba(255, 255, 255, 0.16);
        --card-text-color-main: rgba(255, 255, 255, 0.9);
        --card-text-color-secondary: rgba(255, 255, 255, 0.7);
        --card-text-color-tertiary: rgba(255, 255, 255, 0.5);
        --card-separator-color: rgba(255, 255, 255, 0.12);
    }


    --body-background: unset;

    --accent-color: #34495e;
    --accent-color-darker: #2c3e50;
    --accent-color-text: #fff;
    --body-text-color: #555;

    --tag-border-radius: 4px;

    --section-separation: 40px;

    @media (prefers-color-scheme: dark) {
        --body-background: #303030;
        --accent-color: #ecf0f1;
        --accent-color-darker: #bdc3c7;
        --accent-color-text: #000;
        --body-text-color: rgba(255, 255, 255, 0.7);
    }
}




#fediverse-comments {
    width: 55%;
}

.mastodon-wrapper {
  display: flex;
  gap: 1rem;
  flex-direction: row;
}
.comment-level {
  max-width: 3rem;
  min-width: 3rem;
}
.reply-original {
  display: none;
}
.mastodon-comment {
  background-color: var(--body-background);
  border-radius: var(--card-border-radius);
  padding: var(--card-padding);
  margin-bottom: 1rem;
  display: flex;
  gap: 1rem;
  flex-direction: column;
  flex-grow: 2;
}
.mastodon-comment .comment {
  display: flex;
  flex-direction: row;
  gap: 1rem;
  flex-wrap: true;
}
.mastodon-comment .comment-avatar img {
  width: 4rem;
}
.mastodon-comment .content {
  flex-grow: 2;
}
.mastodon-comment .comment-author {
  display: flex;
  flex-direction: column;
}
.mastodon-comment .comment-author-name {
  font-weight: bold;
}
.mastodon-comment .comment-author-name a {
  display: flex;
  align-items: center;
}
.mastodon-comment .comment-author-reply,
.mastodon-comment .comment-author-date {
    font-size: smaller;
    color: #595B61;
}
.mastodon-comment .comment-author-date {
  margin-inline-start: auto;
}
.mastodon-comment .disabled {
  color: var(--accent-color);
}
.mastodon-comment-content p:first-child {
  margin-top: 0;
}
.mastodon {
  --dlg-bg: #282c37;
  --dlg-w: 600px;
  --dlg-color: #9baec8;
  --dlg-button-p: 0.75em 2em;
  --dlg-outline-c: #00D9F5;
}
