#lang pollen


a:link, a.after:link {
	text-decoration: none !important;
	background: none;
	text-shadow: none;
}

a:after {
    position: relative;
    content: "\FEFF\200F°";◊;redefine for LTR text
    margin-left: 0.10em;
    font-size: 90%;
    top: -0.10em;
    color: #933;
	font-family: "Vesper";
}

a:hover {
	◊;background: #fbf3f3;
	background: #BBCCEE;
	transition-property: background;
	transition-duration: 0.1s;
	◊;border-radius: 8px;
}

a.wikilink:after {
    color: #555;
}



/*
a.xref {
    color: #933;
}
a.xref:after {
        content: none;
}
*/


a.xref:after {
    position: relative;
    content: "\FEFF\200F\25CA";◊;redefine for LTR text
    margin-left: 0.10em;
    font-size: 0.8rem;
    top: -0.5rem;
    color: #933;
    font-family: "Vesper";
}



.barelink:after {
	content: none;
}


.invisiblelink:hover {
    background: none;
}
.invisiblelink:after {
	content: none;
}



.notlink:after {
    position: relative;
    content: "\FEFF\200F ×";◊;redefine for LTR text
    margin-left: 0.10em;
    font-size: 90%;
    top: -0.10em;
    color: #999;
	font-family: "Vesper";
}
