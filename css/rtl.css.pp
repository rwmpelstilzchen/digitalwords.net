#lang pollen

.section_number {
    margin-left: 1em;
}

code, pre > code {
    direction: ltr !important;
}

.maintoc ul li li {
    padding-right: 2em;
}




◊; ┌───────┐
◊; │ Tufte │
◊; └───────┘

body {
	padding-right: 12.5%;
	padding-left: 0%;
    direction: rtl;
}


p {
	padding-left: 0;
}

blockquote p {
	margin-right: 0;
	margin-left: 40px;
}

blockquote footer {
	text-align: left;
}

figcaption {
	float: left;
	clear: left;
}

figure.fullwidth figcaption {
	margin-right: 0;
	margin-left: 24%;
}

.sidenote, .marginnote {
	float: left;
	clear: left;
	margin-right: 0;
	margin-left: -60%;
}

.sidenote:before {
    margin-left: 0.5em;
}

.sidenote-number:after {
	margin-left: 0;
	margin-right: 0.1rem;
}

blockquote .sidenote, blockquote .marginnote {
	margin-right: 0;
	margin-left: -82%;
	text-align: right;
}

pre.code {
	margin-left: 0;
	margin-right: 2.5%;
}

.iframe-wrapper iframe {
	left: 0;
	right: 0;
}




@media (max-width: 760px) {
    body {
        width: 84%;
        padding-left: 8%;
        padding-right: 8%; 
    }
    
    hr,
    section > p,
    section > footer,
    section > table {
        width: 100%; 
    }
    
    pre > code {
        width: 97%; 
    }

    section > ol {
        width: 90%; 
    }

    section > ul {
        width: 90%; 
    }

    figure {
        max-width: 90%; 
    }
    
    figcaption,
    figure.fullwidth figcaption {
        margin-right: 0%;
        max-width: none; 
    }
    
    blockquote {
        margin-right: 1.5em;
        margin-left: 0em; 
    }

    blockquote p,
    blockquote footer {
        width: 100%; 
    }
    
    label.margin-toggle:not(.sidenote-number) {
        display: inline; 
    }

    .sidenote,
    .marginnote {
        display: none; 
    }
    
    .margin-toggle:checked + .sidenote,
    .margin-toggle:checked + .marginnote {
        display: block;
        float: right;
        right: 1rem;
        clear: both;
        width: 95%;
        margin: 1rem 2.5%;
        vertical-align: baseline;
        position: relative; 
    }

    label {
        cursor: pointer; 
    }

    div.table-wrapper,
    table {
        width: 85%; 
    }
    
    img {
        width: 100%;
    } 
}



◊; ┌──────────────────────┐
◊; │ Tufte - non-standard │
◊; └──────────────────────┘

◊; An ugly hack
ul .sidenote, ul .marginnote {
    margin-left: -65%;
    width: 55%;
}

@media (max-width: 760px) {
    ul {
        padding-right: 1em;
    }
}

blockquote {
	border-right: 1px solid lightgray;
	padding-right: 20px;
	margin-right: 20px;
}


◊; ┌───────────┐
◊; │ Marginnav │
◊; └───────────┘

#marginnav {
    /*margin-left: -10.5%; [> Ugly hack! <]*/
    /*display: block;*/
    margin-left: 9.5%;
    width: 30%;
    color: ◊|col-mouse|;
}
@media (max-width: 760px) {
    #marginnav {
        margin-left: 0%;
        display: block;
        float: left;
        left: 1rem;
        clear: both;
        width: 95%;
        margin: 1rem 2.5%;
        vertical-align: baseline;
        position: relative; 
    }
}

.nav3 {
    margin-right: 1em;
}

.nav4 {
    margin-right: 2em;
}

.nav5 {
    margin-right: 3em;
}
