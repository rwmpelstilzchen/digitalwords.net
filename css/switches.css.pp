#lang pollen

/*
┌──────────┐
│ Switches │
└──────────┘
*/

#lightswitch, #readingmode, #uglylinks {
  /*position: absolute;
  top: 2em;
  right: 2em;*/
  margin: 0;
  padding: 0;
  height: 1em;
  z-index: 1;
  cursor: pointer;
  overflow: hidden;
  display: inline-block;
  vertical-align: middle;
}
#lightswitch li, #readingmode li, #uglylinks li {
  list-style: none;
  transition: 0.5s;
}
#lightswitch.active li, #readingmode.active li, #uglylinks.active li{
  transform: translateY(-1em);
}
#lightswitch li span, #readingmode li span, #uglylinks li span {
  display: block;
  height: 1em;
  line-height: 1em;
}



/*
┌─────────────┐
│ Lightswitch │
└─────────────┘
*/

body.night {
    color: ◊|nord6|;
    background: ◊|nord0|;
}
a.night:after {
    color: ◊|nord8|;
}
a.xref.night:after {
    color: ◊|nord8|;
}
h2.night, h3.night, h4.night {
    color: ◊|nord4|;
}
#marginnav.night {
    color: ◊|nord4|;
}
.sidenote.night:before {
    color: ◊|nord4|;
    border-color: ◊|nord4|;
}

a.night, body, img {
    transition: 0.5s;
}

/*
.mastodon-comment.night {
    background-color: #4C566A;
}
*/


/*
┌──────────────┐
│ Reading mode │
└──────────────┘
*/

.margin-figure.readingmode {
	filter: grayscale(100%) opacity(25%);
}

.margin-figure.readingmode:hover {
	filter: none;
}

.emoji.readingmode {
	filter: grayscale(100%);
}



/*
┌────────────┐
│ Ugly links │
└────────────┘
*/

a.uglylinks:after {
    display: none;
}

a.uglylinks {
    text-decoration: underline !important;
}

/*
a.uglylinks:link,
a.uglylinks:visited {
    color: inherit;
}

.no-tufte-underline:link {
    background: unset;
    text-shadow: unset;
}

a.uglylinks:link, .tufte-underline, .hover-tufte-underline:hover {
    text-decoration: none;
    background: -webkit-linear-gradient(#fffff8, #fffff8), -webkit-linear-gradient(#fffff8, #fffff8), -webkit-linear-gradient(currentColor, currentColor);
    background: linear-gradient(#fffff8, #fffff8), linear-gradient(#fffff8, #fffff8), linear-gradient(currentColor, currentColor);
    -webkit-background-size: 0.05em 1px, 0.05em 1px, 1px 1px;
    -moz-background-size: 0.05em 1px, 0.05em 1px, 1px 1px;
    background-size: 0.05em 1px, 0.05em 1px, 1px 1px;
    background-repeat: no-repeat, no-repeat, repeat-x;
    text-shadow: 0.03em 0 #fffff8, -0.03em 0 #fffff8, 0 0.03em #fffff8, 0 -0.03em #fffff8, 0.06em 0 #fffff8, -0.06em 0 #fffff8, 0.09em 0 #fffff8, -0.09em 0 #fffff8, 0.12em 0 #fffff8, -0.12em 0 #fffff8, 0.15em 0 #fffff8, -0.15em 0 #fffff8;
    background-position: 0% 93%, 100% 93%, 0% 93%;
}

@media screen and (-webkit-min-device-pixel-ratio: 0) {
    a.uglylinks:link, .tufte-underline, .hover-tufte-underline:hover {
        background-position-y: 87%, 87%, 87%;
    }
}

a.uglylinks:link::selection,
a.uglylinks:link::-moz-selection {
    text-shadow: 0.03em 0 #b4d5fe, -0.03em 0 #b4d5fe, 0 0.03em #b4d5fe, 0 -0.03em #b4d5fe, 0.06em 0 #b4d5fe, -0.06em 0 #b4d5fe, 0.09em 0 #b4d5fe, -0.09em 0 #b4d5fe, 0.12em 0 #b4d5fe, -0.12em 0 #b4d5fe, 0.15em 0 #b4d5fe, -0.15em 0 #b4d5fe;
    background: #b4d5fe;
}
*/
