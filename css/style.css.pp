#lang pollen

@import url('/fonts/main.css');
@import url('tufte.css');
@import url('butternick-links.css');
@import url('switches.css');
@import url('highlight.css');
@import url('fediverse.css');



/*
┌───────────┐
│ Languages │
└───────────┘

*/

.latin {
	font-family: "Vesper";
	direction: ltr !important;
    text-align: left;
}
.latin:before {
	content: "‎";
}
.latin:after {
	content: "‎";
}

.latinpar {
	direction: ltr;
	text-align: left;
	font-family: "Vesper";
}

.hebrewpar {
	direction: rtl;
	text-align: right;
    font-family: 'Noto Emoji', "Rac", "Open Sans Hebrew", "Alef", serif;
}

.textwidthpar {
	width: 55%;
}


blockquote ul, blockquote ol {
	width: 50%;
}

@media (max-width: 760px) {
	.textwidthpar {
        width: 100%; 
	}
}

.center {
	width: 55%;
	text-align: center;
}

@media (max-width: 760px) {
	.center {
        width: 100%; 
		text-align: center;
	}
}

.gentium {
	font-family: "Gentium Plus", "Gentium";
	direction: ltr !important;
}
.gentium:before {
	content: "‎";
}
.gentium:after {
	content: "‎";
}

.grapheme:before {
	content: "⟨";
}
.grapheme:after {
	content: "⟩";
}

.hebrew-meta {
	direction: rtl;
	font-family: "SBL Hebrew";
}

.biblical-hebrew {
	direction: rtl;
	font-family: "SBL Hebrew";
}

.mongol {
	writing-mode: tb-lr;
	writing-mode: vertical-lr;
}


/*
┌───────────────────┐
│ Table of contents │
└───────────────────┘
*/

/* Index page */

#toc {
	margin-top: 10ex;
}

#toc ul {
	list-style-type: none;
    list-style: none;
    padding-right: 0;
}

.maintoc ul li {
	font-weight: bold;
}

.maintoc ul li li {
	font-weight: normal;
}

.kinderlachhr {
    margin: 3em 0;
}


/* Local TOC for pages */

.nav2, .nav3, .nav4, .nav5 {
    font-family: 'Noto Emoji', "Vesper", "Rac";
    font-feature-settings: "tnum" 1;
}

.nav2 a:after, .nav3 a:after, .nav4 a:after, .nav4 a:after {
	content: none;
}



/*
┌───────┐
│ Tufte │
└───────┘
*/

.sidenote, .marginnote {
	margin-bottom: 2ex;
}

.marginnote img {
	width: 100%;
}


h2, h3, h4, h5 {
	font-style: normal;
	font-weight: normal;
	color: ◊|col-mouse|;
}

h2 {
	margin-top: 5rem;
}

h3 {
	margin-top: 4rem;
}

h4 {
    font-size: 1.4rem;
    margin-top: 3rem;
    margin-bottom: 1.4rem;
    line-height: 1; 
}

h5 {
    font-size: 1.2rem;
    margin-top: 3rem;
    margin-bottom: 1.4rem;
    line-height: 1; 
}

h4+h5 {
	margin-top: 2rem;
}

dl {
    font-size: 1.4rem;
    line-height: 2rem;
}

section > dl {
    width: 55%;
}

dl {
  display: grid;
  grid-template-columns: max-content auto;
}

dt {
  grid-column-start: 1;
}

dd {
  grid-column-start: 2;
}

@media (max-width: 760px) {
	/*label.margin-toggle:not(.sidenote-number) {*/
	label.margin-toggle {
		display: inline-table;
		width: 1em;
		height: 1em;
		margin-left: 0.25em;
		margin-right: 0.25em;
		/*font-size: 1em;*/
		font-family: monospace;
		/*line-height: 10px;*/
		/*line-height: normal;*/
		vertical-align: middle;
		text-align: center;
		border: solid 1px gray;
		color: gray;
		border-radius: 3px;
	}
	label.margin-toggle.sidenote-number:after {
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		vertical-align: inherit;
		font-size: initial;
	}
}

.sidenote-number:after, .sidenote:before {
    font-size: 0.8rem;
}


.sidenote:before {
    content: " " counter(sidenote-counter) " ";
    top: 0;
    border: 1px ◊|col-mouse| solid;
    color: ◊|col-mouse|;
    border-radius: 9cm;
}

.blogdiv {
    font-size: 1.4rem;
    line-height: 2rem;
    margin-top: 1.4rem;
    margin-bottom: 1.4rem;
    padding-right: 0;
    vertical-align: baseline;
    width: 55%;
}

@media (max-width: 760px) {
    .blogdiv {
        width: 100%;
    }
}

.slide {
    width: 100%;
}

section > table {
    font-size: 1.4rem;
    line-height: 2rem; 
}

ul p + p {
    margin-top: 1.4rem;
}

li {
	margin-bottom: 1.4rem;
}

.maintoc li, .child-list li {
	margin-bottom: 0px;
}

/*
┌──────┐
│ Misc │
└──────┘
*/

#sitetitle {
    font-weight: 400;
    margin-top: 4rem;
    margin-bottom: 1.5rem;
    font-size: 3.2rem;
    line-height: 1;
}

#sitelogo {
	height: 1.5ex;
	width: auto;
	vertical-align: bottom;
	float: left
}

@media (max-width: 760px) {
	#sitelogo {
		display: none;
	}
}

.todo {
	color: #E11;
}

.figuresource {
    float: left;
    ◊;margin-left: 2em;
}

.figuresource-ltr {
    float: right;
}

ul {
	list-style: circle;
}

code {
	font-family: Iosevka;
	◊;color: ◊|col-mouse|;
	direction: ltr !important;
}
code:before {
	content: "‎";
}
code:after {
	content: "‎";
}

.tldr {
	font-size: 1.4rem;
    line-height: 2rem; 
	color: ◊|col-mouse|;
	width: 55%;

    background: #fcfcfc;
    padding: 1.5rem;
    border-top: 1px solid #ebebeb;
    border-bottom: 1px solid #ebebeb;
    margin-bottom: 1.2rem;
}

@media (max-width: 760px) {
	.tldr {
		width: 100%; 
	}
}

/* This way <sup /> doesn’t make spacing between lines awkwardly big. */
sup {
    line-height: 0;
}

.frame {
    border: thin black solid;
}

th {
	text-align: initial;
}

/*
┌─────────┐
│ Glosses │
└─────────┘
*/

.example {
    ◊;background: #fcfcfc;
    padding: 1.5rem;
	padding-top: 0.5rem;
    border-top: 1px solid #ebebeb;
    border-bottom: 1px solid #ebebeb;
    margin-bottom: 1.2rem;
}

.gloss {
	font-size: 75%;
}

.glossedblock li {
	margin-bottom: 2ex;
	line-height: 4rem;
}


/*
┌──────────────┐
│ (La)TeX logo │
└──────────────┘
Source: https://stackoverflow.com/a/8160532
*/

.tex sub, .latex sub, .latex sup {
	text-transform: uppercase;
}

.tex sub, .latex sub {
	vertical-align: -0.5ex;
	margin-left: -0.1667em;
	margin-right: -0.125em;
}

.tex, .latex, .tex sub, .latex sub {
	font-size: 1em;
}

.latex sup {
	font-size: 0.85em;
	vertical-align: 0.15em;
	margin-left: -0.36em;
	margin-right: -0.15em;
}

/*
┌───────────┐
│ Pixel art │
└───────────┘
*/

.pixelart { 
    image-rendering: optimizeSpeed;             /* STOP SMOOTHING, GIVE ME SPEED  */
    image-rendering: -moz-crisp-edges;          /* Firefox                        */
    image-rendering: -o-crisp-edges;            /* Opera                          */
    image-rendering: -webkit-optimize-contrast; /* Chrome (and eventually Safari) */
    image-rendering: pixelated; /* Chrome */
    image-rendering: optimize-contrast;         /* CSS3 Proposed                  */
    -ms-interpolation-mode: nearest-neighbor;   /* IE8+                           */
}


/*
┌─────┐
│ Bit │
└─────┘
*/


@font-face {
    font-family: bob;
    font-style: normal;
    font-weight: normal;
    font-stretch: normal;
    src: url('/fonts/bobbin/bobbin-webfont.woff2') format('woff2'),
         url('/fonts/bobbin/bobbin-webfont.woff') format('woff');
}

.bitbox {
	background-color: #111;
	color: #fffff8;
	padding: 1em;
	border-radius: 1em;
	min-height: 4.8em;
	width: calc(55% - 2em);
	font-family: bob;
	display: block;
}

.bitbox a:after {
	font-family: Bobbin;
}

.bitbox a:after {
	color: ◊|nord8|;
}

.megason {
	width: 615px;
	height: 407px;
	background: url("/pici-80/megason.webp");
	display: inline-block;
	background-repeat: no-repeat;
	background-size: cover;
	transition-duration: background 1s;
}

.megason:hover {
	background: url("/pici-80/megason-easter.webp");
	background-repeat: no-repeat;
	background-size: cover;
}

.smallbit {
	width: 2em;
	height: 2em;
	background: url("/pici-80/bit-small-l.png");
	display: inline-block;
	background-repeat: no-repeat;
	background-size: cover;
	margin-left: 1em;
}

.smallbit:hover {
	background: url("/pici-80/bit-small-l-2.png");
	background-repeat: no-repeat;
	background-size: cover;
}
.smallbit a:hover {
	background: none;
}

@media (max-width: 760px) {
	.bitbox {
		width: 90%;
	}
}

/*
┌─────────┐
│ PICI-80 │
└─────────┘
*/

.picoscreenshot {
	width: 256px !important;
	height: 256px !important;
	border: solid 4px black;
}

.pico {
	font-family: 'Zepto-8' !important;
	direction: ltr !important;
    text-align: left;
	font-size: 60%;
}


/*
┌──────────────────────┐
│ booktabs-like tables │
└──────────────────────┘
*/

.booktab {
	border-top: 2px solid #111;
	border-bottom: 2px solid #111;
}

.booktab th {
    border-bottom: 1px solid #111;
    text-align: inherit;
	font weight: normal;
	vertical-align: top;
}


/*
┌───────────────────┐
│ Tables in general │
└───────────────────┘
*/

table td {
	vertical-align: top;
}


/*
┌─────────┐
│ Twemoji │
└─────────┘
*/

img.emoji {
   height: 1em;
   width: 1em;
   margin: 0 .05em 0 .1em;
   vertical-align: -0.1em;
   /*filter: grayscale(100%);*/
}


h.greyscale {
	filter: grayscale(100%);
}





/*
┌────────────────────┐
│ Letter-sized image │
└────────────────────┘
*/

.lettersized {
	height: 1em !important;
}


/*
┌───────────────┐
│ Keyboard keys │
└───────────────┘
*/

kbd {
	display: inline-block;
	padding: 3px 5px;
	font-family: Iosevka, monospace;
	direction: ltr;
	color: #444d56;
	vertical-align: middle;
	background-color: #fafbfc;
	border: solid 1px #d1d5da;
	border-bottom-color: #d1d5da;
	border-radius: 6px;
	box-shadow: inset 0 -1px 0 #d1d5da;
}


/*
┌──────────────────────┐
│ Comma separated list │
└──────────────────────┘
*/

.comma-list {
  display: inline;
  list-style: none;
  padding: 0px;
}

.comma-list li {
  display: inline;
}

.comma-list li::after {
  content: ", ";
}

.comma-list li:last-child::after {
    content: "";
}


/*
 ┌───────┐
 │ Babel │
 └───────┘
*/

.bi {
width: 82%;
display: flex;
flex-direction: row-reverse;
}

.bi > p {
  flex: 1 1 50%;
  /*border: 1px solid black;*/
}
