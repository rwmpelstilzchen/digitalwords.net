#lang pollen

◊(define-meta title "ביקור בוויילס: בנגור, קרדיף ובולך")
◊define-meta[toc]{true}

◊margin-figure["cymru-2019/IMG_20190801_174844.jpg"]{משחק שבץ־נא בוולשית שהשתתפתי בו במסגרת האולפן. שימו לב לצמדי־האותיות (@@@דיגרפים) ◊L{ch}, ◊L{dd}, ◊L{ff}, ◊L{ll}, ◊L{ng}, ◊L{rh} ו־◊L{th} בלוח, שמשמשים כאות אחת בסידור האלף־בית: גם בשבץ־נא, גם במילון וגם במקרים אחרים, כמו סידור של מושבים בקולנוע (◊L{a-b-c-ch-d-dd-e-…‎}):}
◊margin-figure["cymru-2019/IMG_20190723_202501.jpg"]{}
כל ארבע שנים מתקיים הקונגרס הבינלאומי ללימודים קלטיים, שהוא הכנס המרכזי של התחום: היסטוריה, בלשנות, ארכיאולוגיה, סוציולוגיה, ספרות, מוזיקה — כל מה שקלטי בכנס בינתחומי גדול אחד.
◊link["http://cyngresgeltaidd.bangor.ac.uk/"]{הכנס האחרון} (2019) התקיים ב◊wiki-he["%D7%91%D7%A0%D7%92%D7%95%D7%A8_(%D7%95%D7%99%D7%99%D7%9C%D7%A1"]{בנגור} שבצפון וויילס.
לרגל הכנס טסתי לארץ הקודש, ובאותה הזדמנות גם צירפתי לימודים אינטנסיביים של שבועיים באולפן ללימוד וולשית מדוברת (רמה ◊L{Uwch}, שמקבילה ל־◊L{B2} במדד האירופי) וטיול קטן.

הפוסט הזה כולל כל מני שונות, המלצות, צילומים ודברים שאני מקווה שיהיו אולי מועילים למי שמתכוונים לנסוע לוויילס.
אנשים שונים מתעניינים בדברים שונים, כך שמה שאני מוצא מעניין ומועיל לא בהכרח יהיה כזה עבורכם.
◊;◊margin-figure["cymru-2019/IMG_20190720_195042.jpg"]{נסיון}
בכל מקרה, בשבוע של הכנס ובשבועיים של האולפן הייתי בעיקר מרוכז בכנס ובאולפן התאמה, שהיו תובעניים כל אחד בדרכו מבחינה מנטלית, כך שלא היה לי הרבה פנאי לעשות דברים שיהיה רלוונטי לכתוב עליהם כאן.

כתבתי גם דף על ◊xref["osmand.html"]{ניווט בטיולים} שאולי יהיה רלוונטי עבורכם.






◊section{בנגור}

את השבוע הראשון ביליתי בעיר מקסימה שנקראת ◊wiki-he["%D7%91%D7%A0%D7%92%D7%95%D7%A8_(%D7%95%D7%99%D7%99%D7%9C%D7%A1"]{◊L{Bangor}}, על החוף הצפוני של וויילס.
אפשר לראות ממנה את האי ◊wiki-he["%D7%90%D7%A0%D7%92%D7%9C%D7%A1%D7%99"]{◊L{Môn}} ואפילו ללכת לשם ברגל דרך גשר, אבל לא יצא לי לעשות את זה (ביום הפנוי היחיד שלי בבנגור קצת לא הרגשתי טוב והיה גשום).
זאת עיר אוניברסיטה: אליבא דוויקיפדיה מתוך האוכלוסיה הכוללת, המונה 18,808 אנשים, כ־10,500 הם סטודנטים באוניברסיטה.
בזמן של הכנס, בחופשת הקיץ, העיר היתה ריקה יחסית.

בחלק הצפון־

◊TODO{מסעדה: *◊L{Voltaire}}
◊TODO{היער}
◊TODO{המדרגות הקסומות?}
◊TODO{◊L{Tafarn y Glôb} + 6 שפות קלטיות}
◊TODO{החצי־גשר}
◊TODO{השיחה בוולשית}
◊TODO{הים.

◊margin-figure["cymru-2019/IMG_20190721_163034.jpg"]{אצה מסוג }

אסקופילום נודוסום https://he.wikipedia.org/wiki/%D7%90%D7%A1%D7%A7%D7%95%D7%A4%D7%99%D7%9C%D7%95%D7%9D_%D7%A0%D7%95%D7%93%D7%95%D7%A1%D7%95%D7%9D
https://en.wikipedia.org/wiki/Pneumatocyst
}

◊TODO{המוזיאון בל׳אנבריס}
◊TODO{קאי׳ר גורס}
◊TODO{שרון}



◊section{קרדיף}

◊;◊figure["cymru-2019/IMG_20190720_195042.jpg" #:fullwidth #t]

◊TODO{הפארק בקרדיף; החלק הצפוני ממזרח ל־◊L{Taf}}
◊TODO{הספריה הישנה}
◊TODO{קנט א מיל + חוויות + ספר במתנה}
◊TODO{החנות השניה}
◊TODO{ארקייד}
◊TODO{אנטי־המלצה: קונג}



◊section{בולך}

◊TODO{מסלול טיול א׳ + ב׳}
◊TODO{הבר}
